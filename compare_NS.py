# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import datetime as dt
import get_image
import ion_footp
import numpy as np
import os
import plot_HST_UVIS
import time_conversions as tc

#sys.exit()

# save path
savepath = '{}/Plots/HST-UVIS/mapping/'.format(boxpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

# at which times do we want to compare
imgdates = np.array(['2016-181T04-34',
                     '2016-181T05-25',
                     '2016-181T06-16',
                     '2016-181T07-07'])
imget = np.array([dt.datetime.strptime(iii, '%Y-%jT%H-%M') for iii in imgdates])
imget = tc.datetime2et(imget)

# load image lists
uvislist = np.load('%s/UVIS/UVISlist_RES2.npz' % datapath)['data'][()]
hstlist = np.load('%s/HST/HSTlist.npz' % datapath)['data'][()]

# set up mapping class
cim = ion_footp.CassiniIonMap()

# bins over the 0-30 deg colat range
FIN_BINS_COLAT = 480
FIN_colatbins = np.linspace(0,30,num=FIN_BINS_COLAT+1,endpoint=True)
FIN_colatcenters = FIN_colatbins[:-1]+np.diff(FIN_colatbins)/2
# bins over the 0-360 lon range
FIN_BINS_LON = 1440
FIN_lonbins = np.linspace(0,360,num=FIN_BINS_LON+1,endpoint=True)
FIN_loncenters = FIN_lonbins[:-1]+np.diff(FIN_lonbins)/2

# map images into the other hemisphere
def map_image(red_image, orig_hem, desir_hem):
    mapped = np.full((len(FIN_loncenters), len(FIN_colatcenters)), np.nan)
    colatbins = np.linspace(0,30,num=np.shape(red_image)[1]+1,endpoint=True)
    lonbins = np.linspace(0,360,num=np.shape(red_image)[0]+1,endpoint=True)
    # map colatbins if necessary
    if orig_hem == 'North':
        if desir_hem == 'North':
            colatbins_map = colatbins
        else:
            colatbins_map = cim.map_north_to_south(colatbins)
    elif orig_hem == 'South':
        if desir_hem == 'South':
            colatbins_map = colatbins
        else:
            colatbins_map = cim.map_south_to_north(colatbins)
    lonbins_arg = np.digitize(FIN_loncenters, lonbins)-1
    colatbins_arg = np.digitize(FIN_colatcenters,colatbins_map)-1
    valid = np.where(colatbins_arg<len(colatbins)-1)[0]
    for iii in range(len(lonbins_arg)):
        mapped[iii,valid] = red_image[lonbins_arg[iii],colatbins_arg[valid]]
    return mapped


for compctr in range(len(imget)):
    # final savepath
    fullsavepath = '{}/{}'.format(savepath,imgdates[compctr])
    if not os.path.exists(fullsavepath):
        os.makedirs(fullsavepath)
    # get image numbers, filenames and reduced images of the closest images
    uvisnum = np.argmin(np.abs(imget[compctr]-uvislist['ET_START_STOP'][:,0]))
    hstnum = np.argmin(np.abs(imget[compctr]-hstlist['ET_START_STOP'][:,0]))
    uvisfile = uvislist['FILEPATH'][uvisnum]
    hstfile = hstlist['FILEPATH'][hstnum]
    uvisstr = dt.datetime.strftime(tc.et2datetime(uvislist['ET_START_STOP'][uvisnum,0]),'%Y-%jT%H-%M')
    hststr = dt.datetime.strftime(tc.et2datetime(hstlist['ET_START_STOP'][hstnum,0]),'%Y-%jT%H-%M')
    red_uvis, hem_uvis = get_image.getRedUVIS(uvisfile, minangle=15)
    red_hst, hem_hst = get_image.getRedHST(hstfile, minangle=15)
        
    # plot all mapped to south, if not both north
    if hem_uvis == hem_hst:
        map_hem = hem_uvis
    else:
        map_hem = 'South'
    # plot UVIS
    fname = '{}/UVIS_{}_{}'.format(fullsavepath,uvisstr,hem_uvis)
    title = 'UVIS, {} mapped to {}\n{} (DOY {})\n{}-{}, exp={}s'.format(hem_uvis, map_hem,
                   dt.datetime.strftime(tc.et2datetime(uvislist['ET_START_STOP'][uvisnum,0]),'%Y-%jT%H-%M'),
                   dt.datetime.strftime(tc.et2datetime(uvislist['ET_START_STOP'][uvisnum,0]),'%j'),
                   dt.datetime.strftime(tc.et2datetime(uvislist['ET_START_STOP'][uvisnum,0]),'%H:%M'),
                   dt.datetime.strftime(tc.et2datetime(uvislist['ET_START_STOP'][uvisnum,1]),'%H:%M'),
                   uvislist['EXP'][uvisnum])
    mapped_uvis = map_image(red_uvis, hem_uvis, map_hem)    
    plot_HST_UVIS.makeplotFromImage(mapped_uvis, uvislist['ET_START_STOP'][uvisnum,:],
                                    map_hem, fname, title)
    
    # plot HST
    fname = '{}/HST_{}_{}'.format(fullsavepath,hststr,hem_hst)
    title = 'UVIS, {} mapped to {}\n{} (DOY {})\n{}-{}, exp={}s'.format(hem_hst, map_hem,
                   dt.datetime.strftime(tc.et2datetime(hstlist['ET_START_STOP'][hstnum,0]),'%Y-%jT%H-%M'),
                   dt.datetime.strftime(tc.et2datetime(hstlist['ET_START_STOP'][hstnum,0]),'%j'),
                   dt.datetime.strftime(tc.et2datetime(hstlist['ET_START_STOP'][hstnum,0]),'%H:%M'),
                   dt.datetime.strftime(tc.et2datetime(hstlist['ET_START_STOP'][hstnum,1]),'%H:%M'),
                   hstlist['EXP'][hstnum])
    mapped_hst = map_image(red_hst, hem_hst, map_hem)
    plot_HST_UVIS.makeplotFromImage(mapped_hst, hstlist['ET_START_STOP'][hstnum,:],
                                    map_hem, fname, title)    
    
    # plot differenced map
    fname = '{}/diff_{}_{}'.format(fullsavepath,hststr,hem_hst)
    title = 'UVIS-HST, mapped to {}'.format(hststr, hem_hst, map_hem)
    diff = np.full(np.shape(mapped_hst), 0)
    diff[np.where(mapped_uvis>10)] = 1
    diff[np.where(mapped_hst>10)] = 2
    diff[np.where((mapped_hst>10)*(mapped_uvis>10))] = 3
    labels = ['similar', 'UVIS features', 'HST features', 'overlap']
    plot_HST_UVIS.makeplotDiff(diff, fname, title, labels)
    
    

    
