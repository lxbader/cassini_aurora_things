import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import cassinipy
from datetime import datetime, timedelta
import numpy as np
import time_conversions as tc
import matplotlib.pyplot as plt

cd = cassinipy.CassiniData(datapath)

# path to list of UVIS images
listpath = '%s/UVIS/UVISlist_new.npz' % datapath
listpath = '%s/HST/HSTlist.npz' % datapath
imagelist = np.load(listpath)['data'][()]
imgtimes = np.mean(imagelist['ET_START_STOP'], axis=1)
#imgtimes = np.linspace(imgtimes[0],imgtimes[-1],num=10000)

dt = datetime(2004,7,1)
et = tc.datetime2et(dt)

def thetaSun(et):
    yy = tc.et2datetime(et).year
    years = np.array([tc.datetime2et(datetime(iii,1,1)) for iii in [yy,yy+1]])
    yearlen = np.asscalar(np.diff(years))
    fracyy = yy+(et-years[0])/yearlen
    a = -1.371
    b = -25.69*np.cos(2.816+0.213499*fracyy)
    c = 1.389*np.cos(5.4786+0.426998*fracyy)
    return a+b+c

def getZcs(et,r):
    THS = thetaSun(et)
    RH = 29
    return (r-RH*np.tanh(r/RH))*np.tan(THS)

def getCSdist(et,r,z):
    zcs = getZcs(et,r)
    RH = 29
    ncs = np.array([-np.tan(thetaSun(et))*np.tanh(r/RH)**2,0,1])
    thn = np.arccos(np.dot(ncs,np.array([0,0,1]))/np.linalg.norm(ncs))
    return (z-zcs)*np.cos(thn)

dist = np.full_like(imgtimes, np.nan)
for iii in range(len(imgtimes)):
    et = imgtimes[iii]
    loc = cd.get_locations(et,'KSM')
    r = np.sqrt(loc[1]**2+loc[2]**2)
    z = loc[3]
    dist[iii] = getCSdist(et,r,z)
    
val = np.where(np.abs(dist)<100)[0]
print(len(val))
plt.scatter(imgtimes[val],dist[val],marker='+',color='k')



#import pandas as pd
#cspath = '%s/imglists/currentsheet_crossings.npz' % boxpath
#crossings = np.array([])
#
#def getDate():
#    date = datetime(2005,1,1)
#    while date < datetime(2017,10,1):
#        yield np.array([date, date+timedelta(days=100)])
#        date += timedelta(days=100)
#        
#genDate = getDate()
#        
#for i in genDate:
#    print(i)
#    # get data
#    cd.set_timeframe(tc.datetime2et(i))
#    cmag = cd.get_MAG(res='1M')
#    df = pd.DataFrame(np.concatenate([np.expand_dims(cmag.ettimes,axis=1), cmag.data], axis=1),columns=['TIMES','BR','BT','BP'])
#    # smoothen it
#    windowsize = 10*60 #sec
#    pre = cmag.ettimes-windowsize/2
#    post = cmag.ettimes+windowsize/2
#    df['start'] = np.searchsorted(cmag.ettimes,pre)
#    df['stop'] = np.searchsorted(cmag.ettimes,post)
#    def medianWindow(row, colname='BR'):
#        return df[colname].iloc[int(row['start']):int(row['stop']+1)].median()
#    df['BR_S'] = df.apply(medianWindow,axis=1,reduce=False,args=('BR',))
#    df['BT_S'] = df.apply(medianWindow,axis=1,reduce=False,args=('BT',))
#    df['BP_S'] = df.apply(medianWindow,axis=1,reduce=False,args=('BP',))
#    df['MAG_S'] = np.linalg.norm(df[['BR_S','BT_S','BP_S']],axis=1)
#
#    # find sign changes in BR    
#    tmp = np.diff(np.sign(df['BR_S']))
#    cond1 = np.append(np.logical_and(tmp!=0, np.isfinite(tmp)),[False])
#    cond2 = df['MAG_S']
#    arg = np.where(np.logical_and(cond1,cond2))[0]
#    alltimes = df['TIMES'][arg]
#    crossings = np.append(crossings, alltimes)
#    
#    # plot some stuff
#    plt.figure()
#    colors = ['b','g','y']
#    names = ['BR','BT','BP']
##    names = ['BR']
#    for iii in range(len(names)):
##        plt.plot(df['TIMES'], df[names[iii]], color=colors[iii],lw=0.2)
#        plt.plot(df['TIMES'], df[names[iii]+'_S'], color=colors[iii],lw=1)
##    plt.yscale('symlog', linthreshy=0.5)
#    plt.plot(df['TIMES'], df['MAG_S'], color='k',lw=1)
#    plt.scatter(alltimes,np.zeros_like(alltimes), color='r', marker='*',s=40, edgecolor='r',zorder=5)
#    plt.suptitle('%s to %s' % (datetime.strftime(i[0],'%Y-%m-%d'), datetime.strftime(i[1],'%Y-%m-%d')))
#    plt.savefig('E:/%s.png' % datetime.strftime(i[0],'%Y-%m-%d'), bbox_inches='tight', dpi=500)
#    plt.show()
#    
#np.savez(cspath, crossings=crossings)