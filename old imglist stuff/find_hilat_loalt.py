from astropy.io import fits
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.patches as ptch
import numpy as np
import glob
import os
import sys
sys.path.insert(0, 'D:/PhD/Git_Repositories/cassinipy')

import cassinipy

cd = cassinipy.CassiniData()

mydpi = 1000

fitspath = 'D:/PhD/Data/Fits/All'
savepath = 'D:/PhD/Hilat_loalt'
imglistpath = '%s/imglists' % savepath
if not os.path.exists(imglistpath):
    os.makedirs(imglistpath)

# Magnetopause model
# Input: Dp (dynamic pressure in nPa)
def mploc(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    theta = np.arange(0,170,0.1)/180*np.pi
    radius = r0*(2/(1+np.cos(theta)))**K
    [x,r] = radius*[np.cos(theta), np.sin(theta)]
    return [x,r]

imgtimes = []
imgind = []

filelist = glob.glob('%s/*.fits' % fitspath)

for iii in range(len(filelist)):
    fff = filelist[iii]
    try:
        fn = fff.split('\\')[-1]
        fn = fn.split('.')[0]
        tstart = datetime.strptime(fn, '%Y-%jT%H_%M_%S')
        hdulist = fits.open(fff)
        hdu = hdulist[0]
        exp = hdu.header['TOTALEXP']
        image = hdu.data
        tstop = tstart+timedelta(seconds=exp)
        times = [tstart, tstop]
        [out_t, out_rs, out_lat, out_loct] = cd.get_locations(cd.datetime2et(times), refframe='KRTP')
        cond1 = np.any(out_rs<30)
        cond2 = np.any(np.abs(out_lat)>60)
        cond3 = (np.sum(image==0)/360/720) < 0.9
        cond4 = exp > 400


        if np.all([cond1, cond2, cond3, cond4]):
            if not np.any(imgtimes):
                imgtimes = np.array([tstart])
                imgind = np.array([iii])
            else:
                imgtimes = np.concatenate([imgtimes, np.array([tstart])])
                imgind = np.concatenate([imgind, np.array([iii])])
    except:
        pass

[out_t, out_x, out_y, out_z] = cd.get_locations(cd.datetime2et(imgtimes), refframe='KSM')
x = out_x
r = np.sqrt(out_y**2+out_z**2)

plt.figure()
ax = plt.subplot(111)
ax.set_title('UVIS MP images')
ax.set_xlim([-40,40])
ax.set_ylim([0,60])
[xmp, rmp] = mploc(0.01)
ax.plot(xmp,rmp,'k-')
[xmp, rmp] = mploc(0.1)
ax.plot(xmp,rmp,'k-')
ax.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7'))
ax.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0'))
ax.scatter(x, r, s=4, marker='x', color='b', lw=0.2)
plt.savefig('%s/UVIS_hl.jpg' % (savepath), bbox_inches='tight', dpi=mydpi)
plt.show()
plt.close()



imgdiff = np.diff(imgtimes)
tmp = np.where(imgdiff>timedelta(hours=6))[0]
tmp = np.concatenate([np.array([0]), tmp+1, np.array([len(imgtimes)])])

for iii in range(len(tmp)-1):
    thisbulk = imgtimes[tmp[iii]:tmp[iii+1]]
    thisind = imgind[tmp[iii]:tmp[iii+1]]
    center = thisbulk[0]+(thisbulk[-1]-thisbulk[0])//2

    f = open('%s/%s.txt' % (imglistpath, datetime.strftime(center, '%Y-%jT%H_%M_%S')), 'w')
    for jjj in range(len(thisbulk)):
        f.write('%s\n' % filelist[thisind[jjj]])
    f.close()
