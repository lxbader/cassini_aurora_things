import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cassinipy
import cubehelix
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
import ppo_mag_phases
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)
ppo = ppo_mag_phases.ppoMagPhases(datapath)

makeplot = True
mydpi = 1000
cmap_UV = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)


listtype = 'low_lat'
listpath = '%s/%s' % (imglistpath,listtype)
fitspath = '%s/Fits/All' % datapath
imgsavedir = '%s/UVIS/%s' % (plotpath,listtype)
if not os.path.exists(imgsavedir):
    os.makedirs(imgsavedir)

# Import list of interesting plots
filelist = glob.glob('%s/*.txt' % listpath)

for fff in filelist:
    
    UVISimages = np.array([])
    f = open(fff, 'r')
    while True:
        tmp = f.readline()
        if tmp:
            UVISimages = np.append(UVISimages, tmp.split('\n')[0])
        else:
            f.close()
            break

    tmp = fff.split('\\')[-1]
    tmp = tmp.split('.')[0]
    fullimgsavedir = '%s/%s' % (imgsavedir,tmp)
    if not os.path.exists(fullimgsavedir):
        os.makedirs(fullimgsavedir)
    
    for ccc in range(len(UVISimages)):
        uvisfile = UVISimages[ccc]
        print(uvisfile)
        try:
            hdulist = fits.open(uvisfile)
            hdu = hdulist[0]
            exp = hdu.header['TOTALEXP']
            image = hdu.data
            filename = uvisfile.split('\\')[-1]
            filename = filename.split('.')[0]
            timestamp = filename.replace('_',':')
            pyimgtime = datetime.strptime(timestamp, '%Y-%jT%H:%M:%S')
            etimgtime = tc.datetime2et(pyimgtime)
        except:
            continue
        
        # Plot north or south view, depending on which region has been imaged
        if np.any(image[0:60,:]>0.1):
            # South
            view = 'South'
            sview = 'S'
        else:
            # North
            image = image[::-1]
            view = 'North'
            sview = 'N'
        
        # Check coverage
        cvg = np.sum(image[0:60,:]>0) / np.ma.size(image[0:60,:])
        if cvg<0.3:
            continue
    
        if makeplot:
            theta = np.array(range(720))*.5
            theta = np.radians(theta)
            r = np.array(range(360))*.5
    #        levels = np.linspace(-0.3,1.3,301)
    #        levels = np.linspace(0.25,10,40)
            levels = np.linspace(0.1, 4, 40)
        
            if np.any(np.where(image==0)):
                image[np.where(image==0)] = np.nan

            fig = plt.figure()
            ax = plt.subplot(projection='polar')
            data=image
            data[np.where(np.isnan(data))] = levels[0]
            data[np.where(data<levels[0])] = levels[0]
        
            p = ax.contourf(theta, r, data, levels, cmap=cmap_UV, extend='both')
            p.cmap.set_under('k')
            p.cmap.set_over('orangered')
            p.set_clim(0, 4)
        
            if view=='North':
                ctr=1
            else:
                ctr=2
                
            # Cassini location at the time of the MP crossing
            tmp = cd.get_ionfootp(etimgtime)
            ax.scatter(tmp[3]*np.pi/12, tmp[ctr], s=20, color='r', marker='s', zorder=5)
            
            # PPO phase angle
            tmp = ppo.getMagPhase(etimgtime+exp/2, 'N')
            ax.plot([tmp,tmp], [0,30], color='g', lw=2.5)
            ax.text(tmp, 33, 'N', color='g', fontsize=15, horizontalalignment='center', verticalalignment='center')
            tmp = ppo.getMagPhase(etimgtime+exp/2, 'S')
            ax.plot([tmp,tmp], [0,30], color='blueviolet', lw=2.5)
            ax.text(tmp, 33, 'S', color='blueviolet', fontsize=15, horizontalalignment='center', verticalalignment='center')
        
            ax.set_theta_zero_location("N")
            ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
            ax.set_xticklabels(['00','06','12','18'])
            ax.set_yticks([10,20,30])
            ax.set_yticklabels([])
            ax.grid('on', color='w', linewidth=2)
            ax.set_title('%s\n %s, %s s' % (datetime.strftime(pyimgtime, '%Y-%j, %H:%M'),view, exp), y=1.1)
            ax.set_rmax(30)
            ax.set_rmin(0)
            cbar = plt.colorbar(p, pad=0.1, ax=ax)
            cbar.set_label(r'Intensity (kR)', rotation=270)
            cbar.set_ticks([0,1,2,3,4,5,6,7,8,9,10])
        
            plt.savefig('%s/%s.jpg' % (fullimgsavedir, datetime.strftime(pyimgtime, '%Y-%j_%H%M')), bbox_inches='tight', dpi=mydpi)
            plt.show()
            plt.close()
            
#            plt.figure()
#            for iii in range(720): plt.plot(image[0:60,iii])
#            plt.show()
#            plt.close()
#    
