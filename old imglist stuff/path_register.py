# Paths
import socket

class PathRegister(object):
    
    def __init__(self):
        self.hostname = socket.gethostname()
        
        if self.hostname == 'Yggdrasil':
            self.mainpath = 'D:/PhD'
                        
        elif self.hostname == 'pyb504000032':
            self.mainpath = 'E:'
            
        self.datapath = '%s/data' % self.mainpath
        self.gitpath = '%s/GIT' % self.mainpath
        self.plotpath = '%s/plots' % self.mainpath
        self.boxpath = '%s/Box Sync' % self.mainpath
        self.imglistpath = '%s/imglists' % self.boxpath
        