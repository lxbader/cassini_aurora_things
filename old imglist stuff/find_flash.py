import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

from datetime import datetime, timedelta
import glob
import matplotlib.pyplot as plt
import matplotlib.patches as ptch
import numpy as np
import os
import cassinipy
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

mydpi=1000

flashfile = '%s/noon_flash_list.txt' % imglistpath
fitspath = '%s/Fits/All' % datapath
savepath = '%s/noon_flash' % imglistpath
if not os.path.exists(savepath):
    os.makedirs(savepath)

# Magnetopause model
# Input: Dp (dynamic pressure in nPa)
def mploc(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    theta = np.arange(0,170,0.1)/180*np.pi
    radius = r0*(2/(1+np.cos(theta)))**K
    [x,r] = radius*[np.cos(theta), np.sin(theta)]
    return [x,r]

data = np.genfromtxt(flashfile,
                    names = ['YEAR', 'DOY', 'START','STOP', 'P1', 'HEM'],
                    dtype = [np.int, np.int, 'U5', 'U5', 'U2', 'U1'],
                    usecols=[0,1,2,3,4,5],
                    skip_header=1)

# Convert to datetime
tstart = np.array([datetime(data['YEAR'][iii],1,1) +
            timedelta(days=np.asscalar(data['DOY'][iii]-1),
                      hours=int(np.asscalar(data['START'][iii])[0:2]),
                      minutes=int(np.asscalar(data['START'][iii])[3:5]))
                      for iii in range(len(data['YEAR']))])

tstop = np.array([datetime(data['YEAR'][iii],1,1) +
            timedelta(days=np.asscalar(data['DOY'][iii]-1),
                      hours=int(np.asscalar(data['STOP'][iii])[0:2]),
                      minutes=int(np.asscalar(data['STOP'][iii])[3:5]))
                      for iii in range(len(data['YEAR']))])

for iii in range(len(tstop)):
    if data['P1'][iii] == '+1':
        tstop[iii] += timedelta(days=1)

imgtimes = []
imgind = []

filelist = glob.glob('%s/*.fits' % fitspath)

for iii in range(len(filelist)):
    try:
        fff = filelist[iii]
        fn = fff.split('\\')[-1]
        fn = fn.split('.')[0]
        thist = datetime.strptime(fn, '%Y-%jT%H_%M_%S')

        tmp = np.where((thist>=tstart)*(thist<=tstop))[0]

        if len(tmp):
            if not np.any(imgtimes):
                imgtimes = np.array([thist])
                imgind = np.array([iii])
            else:
                imgtimes = np.concatenate([imgtimes, np.array([thist])])
                imgind = np.concatenate([imgind, np.array([iii])])
    except:
        pass

[out_t, out_x, out_y, out_z] = cd.get_locations(tc.datetime2et(imgtimes), refframe='KSM')
x = out_x
r = np.sqrt(out_y**2+out_z**2)

plt.figure()
ax = plt.subplot(111)
ax.set_title('UVIS MP images')
ax.set_xlim([-40,40])
ax.set_ylim([0,60])
[xmp, rmp] = mploc(0.01)
ax.plot(xmp,rmp,'k-')
[xmp, rmp] = mploc(0.1)
ax.plot(xmp,rmp,'k-')
ax.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7'))
ax.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0'))
ax.scatter(x, r, s=4, marker='x', color='b', lw=0.2)
plt.savefig('%s/UVIS_noon.jpg' % (savepath), bbox_inches='tight', dpi=mydpi)
plt.show()
plt.close()

imgdiff = np.diff(imgtimes)
tmp = np.where(imgdiff>timedelta(hours=6))[0]
tmp = np.concatenate([np.array([0]), tmp+1, np.array([len(imgtimes)])])

for iii in range(len(tmp)-1):
    thisbulk = imgtimes[tmp[iii]:tmp[iii+1]]
    thisind = imgind[tmp[iii]:tmp[iii+1]]
    argcenter = len(thisbulk)//2
    center = thisbulk[argcenter]

    f = open('%s/%s.txt' % (savepath, datetime.strftime(center, '%Y-%jT%H_%M_%S')), 'w')
    for jjj in range(len(thisbulk)):
        f.write('%s\n' % filelist[thisind[jjj]])
    f.close()
