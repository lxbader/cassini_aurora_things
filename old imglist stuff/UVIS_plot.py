import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cubehelix
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import sys

cmap_UV = cubehelix.cmap(reverse=False, start=0.3, rot=-0.5)

mydpi = 600
datapath = 'E:/data/Fits/All'
imgsavedir = '%s/plots' % datapath
if not os.path.exists(imgsavedir):
    os.makedirs(imgsavedir)

filectr = 0
for fff in glob.glob('%s/*.fits' % datapath):
    filectr += 1
    print('Plotting file %s' % filectr)
    try:
        hdulist = fits.open(fff)
        hdu = hdulist[0]
        image = hdu.data
        filename = fff.split('\\')[1]
        filename = filename.split('.')[0]
        timestamp = filename.replace('_',':')
        exp = hdu.header['TOTALEXP']

    #    # Plot longitude-latitude grid
    #    ax = plt.axes()
    #    ax.imshow(image, origin = 'lower')
    #    ax.set_aspect('equal')
    #    ax.set_xlabel('Longitude (deg)')
    #    ax.set_ylabel('Latitude (deg)')
    #    ax.set_title('%s\n %s s' % (timestamp, exp), y=1.05)
    #    plot1 = ax.pcolor(range(720), range(360), image, cmap=mycmap, vmin=0, vmax=12)
    #    plt.colorbar(plot1, ax=ax)
    #    plt.savefig('%s/%s.jpg' % (imgsavedir, filename), bbox_inches='tight', dpi=mydpi)
    #    plt.show()

        # Plot north and south view
        theta = np.array(range(720))*.5
        theta = np.radians(theta)
        r = np.array(range(360))*.5
        levels = np.linspace(-0.5,1.5,301)

        view = ['North', 'South']
        viewdata = [image[::-1], image]
        for dirctr in range(2):
            ax = plt.axes(polar = "True")
            data = np.log10(viewdata[dirctr])
            data[np.where(np.isnan(data))] = levels[0]
            data[np.where(data<levels[0])] = levels[0]
#            p = ax.contourf(theta, r, data, levels, cmap=cmap_UV)
            p = ax.pcolormesh(theta, r, data, cmap=cmap_UV)
            ax.set_theta_zero_location("N")
            ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
            ax.set_xticklabels(['00','06','12','18'])
            ax.set_yticks([10,20,30])
            ax.set_yticklabels([])
            ax.grid('on', color='w', linewidth=2)
            ax.set_title('%s %s, %s s' % (timestamp, view[dirctr], exp), y=1.1)
            ax.set_rmax(30)
            ax.set_rmin(0)
            cbar = plt.colorbar(p, pad=0.1, ax=ax)
            cbar.set_label(r'log10 Intensity (kR)', rotation=270)
#            plt.savefig('%s/%s_%s.jpg' % (imgsavedir, view[dirctr], filename), bbox_inches='tight', dpi=mydpi)
            plt.show()
        

    except:
        print('Plot failed')
        pass
    sys.exit()