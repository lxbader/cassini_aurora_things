import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

from astropy.io import fits
import cassinipy
from datetime import datetime, timedelta
import glob
import numpy as np
import os
import plot_loc
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

mydpi=1000

fitspath = '%s/Fits/All' % datapath
savepath = '%s/full_set' % imglistpath
if not os.path.exists(savepath):
    os.makedirs(savepath)

filelist = glob.glob('%s/*.fits' % fitspath)

imgtimes = np.full((len(filelist)), np.nan)
imgexp = np.full((len(filelist)), np.nan)
imgcvg = np.full((len(filelist)), np.nan)

for iii in range(len(filelist)):
    fff = filelist[iii]
    try:
        fn = fff.split('\\')[-1]
        fn = fn.split('.')[0]
        tstart = datetime.strptime(fn, '%Y-%jT%H_%M_%S')
        imgtimes[iii] = tc.datetime2et(tstart)
        hdulist = fits.open(fff)
        hdu = hdulist[0]
        image = hdu.data
        exp = hdu.header['TOTALEXP']
        imgexp[iii] = exp
        cvg = np.sum(image!=0)/360/720
        imgcvg[iii] = cvg
    except:
        pass
    
minyear = tc.et2datetime(imgtimes[0]).year
maxyear = tc.et2datetime(imgtimes[-1]).year
maxyear = 2016

tmp = tc.et2datetime(imgtimes)
imgyears = np.array([iii.year for iii in tmp])

cond1 = imgexp>400
cond2 = imgcvg>0.1
for yyy in range(minyear, maxyear+1):
    cond3 = imgyears==yyy
    selec = np.where(cond1*cond2*cond3)[0]
    startstop = [tc.datetime2et(datetime(yyy,1,1,0,0,0)),tc.datetime2et(datetime(yyy+1,1,1,0,0,0))]
#    plot_loc.plotCassiniLoc(startstop, imgtimes[selec], cd, 'UVIS images %d' % yyy, '%s/UVIS_all_%d.jpg' % (savepath, yyy))

argselec = np.where(cond1*cond2)[0]
selecimgtimes = tc.et2datetime(imgtimes[argselec])

imgdiff = np.diff(selecimgtimes)
tmp = np.where(imgdiff>timedelta(hours=6))[0]
tmp = np.concatenate([np.array([0]), tmp+1, np.array([len(selecimgtimes)])])

for iii in range(len(tmp)-1):
    thisbulk = selecimgtimes[tmp[iii]:tmp[iii+1]]
    thisind = argselec[tmp[iii]:tmp[iii+1]]
    center = thisbulk[0]+(thisbulk[-1]-thisbulk[0])//2

    f = open('%s/%s.txt' % (savepath, datetime.strftime(center, '%Y-%jT%H_%M_%S')), 'w')
    for jjj in range(len(thisbulk)):
        name = filelist[thisind[jjj]].split('\\')[-1]
        f.write('%s\n' % name)
    f.close()
