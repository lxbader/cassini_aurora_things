import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cassinipy
import cubehelix
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
import ppo_mag_phases
import shutil
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)
ppo = ppo_mag_phases.ppoMagPhases(datapath)

makeplot = False
mydpi = 1000
cmap_UV = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

fitspath = '%s/Fits/All' % datapath
imgpath = '%s/Fits/02_img' % datapath
savepath = '%s/NS' % imglistpath
if not os.path.exists(imgpath):
    os.makedirs(imgpath)
if os.path.exists(savepath):
    shutil.rmtree(savepath)
os.makedirs(savepath)

filelist = glob.glob('%s/*.fits' % fitspath)
filenames = np.array([fff.split('\\')[-1].split('.')[0] for fff in filelist])
filetimes = np.array([datetime.strptime(fff, '%Y-%jT%H_%M_%S') for fff in filenames])
#filelist = [filelist[iii].replace('\\', '/') for iii in range(len(filelist))]

fileexp = np.full(np.shape(filelist), np.nan)
filehem = np.full(np.shape(filelist), 'X')
filemaxkr = np.full(np.shape(filelist), np.nan)

for iii in range(len(filelist)):
#for iii in range(1,2):
    fff = filelist[iii]
    try:
        hdulist = fits.open(fff)
        hdu = hdulist[0]
        exp = hdu.header['TOTALEXP']
        image = hdu.data
    except:
        continue
    
    print(fff)
    fileexp[iii] = exp
    

    # Plot north or south view, depending on which region has been imaged
    if np.any(image[0:60,:]>0.1):
        # South
        view = 'South'
        sview = 'S'
    else:
        # North
        image = image[::-1]
        view = 'North'
        sview = 'N'
    
    filehem[iii] = sview

    # Check coverage
    cvg = np.sum(image[0:60,:]>0) / np.ma.size(image[0:60,:])
    if cvg<0.3:
        continue

    if makeplot:
        theta = np.array(range(720))*.5
        theta = np.radians(theta)
        r = np.array(range(360))*.5
#        levels = np.linspace(-0.3,1.3,301)
#        levels = np.linspace(0.25,10,40)
        levels = np.linspace(0.1, 4, 40)
    
        if np.any(np.where(image==0)):
            image[np.where(image==0)] = np.nan
    
        thistime = filetimes[iii]
    
        fig = plt.figure()
        ax = plt.subplot(projection='polar')
        data=image
        data[np.where(np.isnan(data))] = levels[0]
        data[np.where(data<levels[0])] = levels[0]
    
        p = ax.contourf(theta, r, data, levels, cmap=cmap_UV, extend='both')
        p.cmap.set_under('k')
        p.cmap.set_over('orangered')
        p.set_clim(0, 4)
    
        # Plot lines of ion footprint
        tmin = thistime - timedelta(days=1)
        tmax = tmin + timedelta(days=2)
        [tmin, tmax] = tc.datetime2et([tmin, tmax])
        step = 1000
        times = [xxx*(tmax-tmin)/step + tmin for xxx in range(step)]
        tmp = cd.get_ionfootp(times)
    
        if view=='North':
            ctr=1
        else:
            ctr=2
#        ax.plot(tmp[3]*np.pi/12, tmp[ctr], color='1', linestyle='-', lw=2)
    
    #    # Plot dots and day markers
    #    tmp = cd.get_ionfootp(ettimemarkers)
    #    ax.scatter(tmp[3]*np.pi/12, tmp[dirctr+1], s=20, color=fpcolorbright)
    #    tmp = cd.get_ionfootp(etdaymarkers)
    #    for jjj in range(len(etdaymarkers)):
    #        ax.text(tmp[3][jjj]*np.pi/12, tmp[1][jjj], daylabels[jjj], color=fpcolorbright)
    
        # Cassini location at the time of the MP crossing
        tmp = cd.get_ionfootp(tc.datetime2et([thistime]))
        ax.scatter(tmp[3]*np.pi/12, tmp[ctr], s=20, color='r', marker='s', zorder=5)
        
        # PPO phase angle
        tmp = ppo.getMagPhase(tc.datetime2et(thistime)+exp/2, 'N')
        ax.plot([tmp,tmp], [0,30], color='g', lw=2.5)
        ax.text(tmp, 33, 'N', color='g', fontsize=15, horizontalalignment='center', verticalalignment='center')
        tmp = ppo.getMagPhase(tc.datetime2et(thistime)+exp/2, 'S')
        ax.plot([tmp,tmp], [0,30], color='blueviolet', lw=2.5)
        ax.text(tmp, 33, 'S', color='blueviolet', fontsize=15, horizontalalignment='center', verticalalignment='center')
    
        ax.set_theta_zero_location("N")
        ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
        ax.set_xticklabels(['00','06','12','18'])
        ax.set_yticks([10,20,30])
        ax.set_yticklabels([])
        ax.grid('on', color='w', linewidth=2)
        ax.set_title('%s\n %s, %s s' % (datetime.strftime(thistime, '%Y-%j, %H:%M'),view, exp), y=1.1)
        ax.set_rmax(30)
        ax.set_rmin(0)
        cbar = plt.colorbar(p, pad=0.1, ax=ax)
        cbar.set_label(r'Intensity (kR)', rotation=270)
        cbar.set_ticks([0,1,2,3,4,5,6,7,8,9,10])
    
        plt.savefig('%s/%s.jpg' % (imgpath, datetime.strftime(filetimes[iii], '%Y-%j_%H%M')), bbox_inches='tight', dpi=mydpi)
        plt.show()
        plt.close()
        
#        plt.figure()
#        for iii in range(720): plt.plot(image[0:60,iii])
#        plt.show()
#        plt.close()

sys.exit()
        
# Find switches between N and S hemispheres
argswitch = np.full((len(filehem)-1), False)
for iii in range(len(filehem)-1):
    cond1 = filehem[iii] != filehem[iii+1]
    cond2 = np.all(filehem[iii:iii+2] != 'X')
    cond3 = fileexp[iii] > 100
    if cond1 and cond2 and cond3:
        argswitch[iii] = True
#argswitch = np.array([filehem[iii]!=filehem[iii+1] for iii in range(len(filehem)-1)])
argswitch = np.where(argswitch)[0]

# Indices of 2 plots before and after each hemisphere switch
argind = np.full((4*np.size(argswitch)), np.nan)
it = np.nditer(np.arange(len(argind)))
for iii in range(len(argswitch)):
    for jjj in range(-1,3):
        argind[np.asscalar(it[0])] = argswitch[iii]+jjj
        it.iternext()

argind = np.unique(argind)

imgdiff = np.diff(argind)
tmp = np.where(imgdiff>1)[0]
tmp = np.concatenate([np.array([0]), tmp+1, np.array([len(argind)])])

for iii in range(len(tmp)-1):
    thisind = argind[tmp[iii]:tmp[iii+1]]
    argcenter = np.int(np.floor(np.mean(thisind)))
    thistime = filetimes[argcenter]

    f = open('%s/%s.txt' % (savepath, datetime.strftime(thistime, '%Y-%jT%H_%M_%S')), 'w')
    for jjj in range(len(thisind)):
        f.write('%s\n' % filelist[int(thisind[jjj])])
    f.close()
