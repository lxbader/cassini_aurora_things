import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

from astropy.io import fits
from datetime import datetime, timedelta
import glob
import matplotlib.pyplot as plt
import matplotlib.patches as ptch
import numpy as np
import os
import cassinipy
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

deltat = 36 # hours

mydpi=1000

mpfile = '%s/mp_cross_list.txt' % imglistpath
fitspath = '%s/Fits/All' % datapath
savepath = '%s/mp_cross' % imglistpath
if not os.path.exists(savepath):
    os.makedirs(savepath)

# Magnetopause model
# Input: Dp (dynamic pressure in nPa)
def mploc(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    theta = np.arange(0,170,0.1)/180*np.pi
    radius = r0*(2/(1+np.cos(theta)))**K
    [x,r] = radius*[np.cos(theta), np.sin(theta)]
    return [x,r]

# Import list of MP crossing dates
mp_data = np.genfromtxt(mpfile,
                    names = ['YEAR','DOY','HOUR','MINUTE','IN_OUT'],
                    dtype = [np.int, np.int, np.int, np.int, 'U1'],
                    usecols=[0,1,2,3,4],
                    skip_header=1)

# Convert to datetime
mp_times = np.array([datetime(mp_data['YEAR'][iii],1,1) +
            timedelta(days=np.asscalar(mp_data['DOY'][iii]-1),
                      hours=np.asscalar(mp_data['HOUR'][iii]),
                      minutes=np.asscalar(mp_data['MINUTE'][iii]))
                      for iii in range(len(mp_data['YEAR']))])

imgtimes = []
imgind = []

filelist = glob.glob('%s/*.fits' % fitspath)

for iii in range(len(filelist)):
    fff = filelist[iii]
    try:
        fn = fff.split('\\')[-1]
        fn = fn.split('.')[0]
        tstart = datetime.strptime(fn, '%Y-%jT%H_%M_%S')
        hdulist = fits.open(fff)
        hdu = hdulist[0]
        image = hdu.data
        exp = hdu.header['TOTALEXP']
        tstop = tstart+timedelta(seconds=exp)
        times = [tstart, tstop]

        cond1 = np.any(np.abs(tstart-mp_times) < timedelta(hours=deltat))
        # Check coverage in image
        cond2 = (np.sum(image!=0)/360/720) > 0.1
        # Check exposition time
        cond3 = exp > 400

        if np.all([cond1, cond2, cond3]):
            if not np.any(imgtimes):
                imgtimes = np.array([tstart])
                imgind = np.array([iii])
            else:
                imgtimes = np.concatenate([imgtimes, np.array([tstart])])
                imgind = np.concatenate([imgind, np.array([iii])])
    except:
        pass

[out_t, out_x, out_y, out_z] = cd.get_locations(tc.datetime2et(imgtimes), refframe='KSM')
x = out_x
r = np.sqrt(out_y**2+out_z**2)

plt.figure()
ax = plt.subplot(111)
ax.set_title('UVIS MP images')
ax.set_xlim([-40,40])
ax.set_ylim([0,60])
[xmp, rmp] = mploc(0.01)
ax.plot(xmp,rmp,'k-')
[xmp, rmp] = mploc(0.1)
ax.plot(xmp,rmp,'k-')
ax.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7'))
ax.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0'))
ax.scatter(x, r, s=4, marker='x', color='b', lw=0.2)
plt.savefig('%s/UVIS_mp.jpg' % (savepath), bbox_inches='tight', dpi=mydpi)
plt.show()
plt.close()

imgdiff = np.diff(imgtimes)
tmp = np.where(imgdiff>timedelta(hours=6))[0]
tmp = np.concatenate([np.array([0]), tmp+1, np.array([len(imgtimes)])])

for iii in range(len(tmp)-1):
    thisbulk = imgtimes[tmp[iii]:tmp[iii+1]]
    thisind = imgind[tmp[iii]:tmp[iii+1]]
    center = thisbulk[0]+(thisbulk[-1]-thisbulk[0])//2

    f = open('%s/%s.txt' % (savepath, datetime.strftime(center, '%Y-%jT%H_%M_%S')), 'w')
    for jjj in range(len(thisbulk)):
        f.write('%s\n' % filelist[thisind[jjj]])
    f.close()