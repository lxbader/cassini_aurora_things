# Projecting UVIS images into the equatorial plane
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cubehelix
from datetime import datetime
import glob
import ion_footp
import matplotlib.pyplot as plt
import numpy as np
import os

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.2, gamma=1.5)
KR_MAX = 10
uvispath = '%s/UVIS/all' % datapath
savepath = '%s/UVIS/eq_proj_10kr' % datapath
if not os.path.exists(savepath):
    os.makedirs(savepath)

cim = ion_footp.CassiniIonMap()

# input image size
UVIScolatbins = np.linspace(0,180,num=361)
UVISlonbins = np.linspace(0,360,num=721)

# output image size
rbins = np.linspace(5,100,num=1000)
lonbins = np.linspace(0,360,num=721)

# map radial distances to colatitudes of input image
colatbins_s = cim.map_ion(np.array([rbins, np.zeros_like(rbins), np.zeros_like(rbins)]))[1]
colatbins_n = cim.map_ion(np.array([rbins, np.zeros_like(rbins), np.zeros_like(rbins)]))[0]

# center of each bin in output image
rcenters = rbins[:-1]+np.diff(rbins)/2
colatcenters_s = colatbins_s[:-1]+np.diff(colatbins_s)/2
colatcenters_n = colatbins_n[:-1]+np.diff(colatbins_n)/2
loncenters = lonbins[:-1]+np.diff(lonbins)/2

fitsfiles = glob.glob('%s/*.fits' % uvispath)

for filectr in range(len(fitsfiles)):
    try:
        print('File %s of %s' % (filectr+1, len(fitsfiles)))    
        # import image, assign input image pixels to output image pixels
    #    uvisimage = '%s/UVIS/all/2008-201T12_11_03.fits' % datapath
        filename = fitsfiles[filectr].split('.')[0]
        filename = filename.split('/')[-1]
        filename = filename.split('\\')[-1]
        filename = filename.replace('-','_')
        pyimgtime = datetime.strptime(filename, '%Y_%jT%H_%M_%S')
        
        hdulist = fits.open(fitsfiles[filectr])
        hdu = hdulist[0]
        exp = hdu.header['TOTALEXP']
        image = hdu.data
        if np.any(image[0:60,:]>0.1):
            hemisphere = 'South'
            UVIScolatind = np.digitize(colatbins_s[:-1],UVIScolatbins)-1
        else:
            hemisphere = 'North'
            image = image[::-1,:]
            UVIScolatind = np.digitize(colatbins_n[:-1],UVIScolatbins)-1
        UVISlonind = np.digitize(lonbins[:-1],UVISlonbins)-1
        
        # copy data from input to output image
        eq_proj = np.zeros((len(rcenters), len(loncenters)))
        for rctr in range(len(UVIScolatind)):
            eq_proj[rctr,:] = image[UVIScolatind[rctr],UVISlonind]
        
        # make figure
        fig = plt.figure()
        fig.set_size_inches(8,8)
        ax = fig.add_subplot(111, projection='polar')
        
        contours = np.linspace(0,KR_MAX,num=21)
        #quad = ax.pcolormesh(lonbins*np.pi/180, rbins, eq_proj, cmap=cmap_UV)
        quad = ax.contourf(loncenters*np.pi/180, rcenters, eq_proj, contours, cmap=cmap_UV, extend='both')
        quad.cmap.set_under('k')
        quad.cmap.set_over('orangered') 
        quad.set_clim(0, KR_MAX)   
        ax.set_axis_bgcolor('gray')
        cbar = plt.colorbar(quad, pad=0.1, ax=ax, extend='both')
        cbar.set_label('Intensity (kR)', rotation=270, labelpad=20)
        cbar.set_ticks([0,np.floor(KR_MAX/2), KR_MAX])
        
        ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
        ax.set_xticklabels(['00','06','12','18'])
        ax.set_yticks([20,40,60,80])
        ax.set_yticklabels([20,40,60,80], color='w')
        ax.grid('on', color='w', linewidth=1, linestyle='--')
        ax.set_theta_zero_location("N")
        ax.set_rorigin(0)
        ax.set_rmax(100)
        ax.set_rmin(5)
        ax.set_title('%s\n%s\n%s, %s s' % (datetime.strftime(pyimgtime, '%Y-%j %H:%M:%S'),
                                       datetime.strftime(pyimgtime, '%Y-%m-%d %H:%M:%S'),
                                       hemisphere, exp), y=1.1)    
        
        # save and close
        plt.savefig('%s/eq_proj_%s.png' % (savepath,filename), bbox_inches='tight', dpi=500)
        plt.close()
    except:
        pass
