import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import cassinipy
from datetime import datetime, timedelta
import numpy as np
import os
import plot_loc
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)
savepath = '%s/HST' % plotpath
if not os.path.exists(savepath):
    os.makedirs(savepath)

hstfile = '%s/HST_campaigns.txt' % boxpath
data = np.genfromtxt(hstfile,
                    names = ['FYEAR', 'FMONTH', 'FDAY','TYEAR', 'TMONTH', 'TDAY'],
                    dtype = [np.int, np.int, np.int, np.int, np.int, np.int],
                    usecols=[0,1,2,3,4,5],
                    skip_header=2)

hsttimes = np.array([[datetime(data['FYEAR'][iii],data['FMONTH'][iii],data['FDAY'][iii],0,0,0),
                      datetime(data['TYEAR'][iii],data['TMONTH'][iii],data['TDAY'][iii],23,59,59)] for iii in range(len(data['FYEAR']))])
hsttimes = np.delete(hsttimes, 2, 0)
tailtimes = np.array([])

for iii in range(len(hsttimes[:,0])):
    tmptimes = hsttimes[iii,:]
    title = '%s to %s' % (datetime.strftime(tmptimes[0], '%Y-%m-%d'), datetime.strftime(tmptimes[1], '%Y-%m-%d'))
    startstop = tc.datetime2et(tmptimes)
    savefile = '%s/%d' % (savepath, iii)
    try:
    #    plot_loc.plotCassiniLoc(startstop, [], cd, title, savefile)
        timelist = np.linspace(startstop[0], startstop[1], num=10000)
        [t,x,y,z] = cd.get_locations(timelist, 'KSM')
        [t,r,lat,lt] = cd.get_locations(timelist, 'KRTP')

        cond1 = r>10
        cond2 = x<0
        cond3 = np.abs(z) < 10
        tmp = np.where(cond1*cond2*cond3)
        tailtimes = np.append(tailtimes, timelist[tmp])
    except:
        pass

taildates = tc.et2datetime(tailtimes)
taildates = np.array([datetime(taildates[jjj].year, taildates[jjj].month, taildates[jjj].day) for jjj in range(len(taildates))])
taildates = np.unique(taildates)

plotmagdates = []
for iii in range(len(taildates)):
    for jjj in range(6):
        plotmagdates = np.append(plotmagdates, taildates[iii]+timedelta(hours=4*jjj))

for date in plotmagdates:
#for iii in range(0, len(plotmagdates)):
#    date = plotmagdates[iii]
    thistime = tc.datetime2et(date)
    tlimet = [thistime, thistime+4*3600]
    cd.set_timeframe(tlimet)
    cd.set_refframe('KRTP')
    magdata = cd.get_MAG('1S')
    datestr = datetime.strftime(date, '%Y-%m-%d_%H-%M')
    imgsavefile = '%s/%s' % (savepath, datestr)

    cd.plot_MAG(imgsavefile)
