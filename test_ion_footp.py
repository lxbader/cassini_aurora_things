# check ionospheric footprints

# plot UVIS files
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as spint
import scipy.optimize as opt

import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

timefmt = '%Y-%m-%d %H-%M-%S'
times = ['2008-01-01 12-00-00', '2008-02-01 00-00-00']

pytimes = [datetime.strptime(iii, timefmt) for iii in times]
ettimes = tc.datetime2et(pytimes)
ettimes = np.linspace(ettimes[0], ettimes[1], num=1000)

#ettimes = np.array([  2.55007047e+08,   2.55008792e+08,   2.55010538e+08,
#         2.55012283e+08,   2.55014029e+08,   2.55015774e+08,
#         2.55017520e+08])

# Cassini location
tmp = cd.get_ionfootp(ettimes)

## Step by step calculation
#[t, x, y, z] = cd.get_locations(ettimes, 'KSM')
##times, colat_n, colat_s, loct = cd.cim.map_ion(np.array([x,y,z]))
#
#xyz = np.array([x,y,z])
#
#if xyz.ndim<2:
#    xyz = np.expand_dims(xyz, axis=1)
#[ksm_x, ksm_y, ksm_z] = xyz
#
## Cassini local time [0-24 hours]
#loct = ((np.arctan2(ksm_y,ksm_x) + np.pi) * 12/np.pi) % 24
#
## to cylindrical
#cyl_z = ksm_z
#cyl_r = np.sqrt(ksm_x**2+ksm_y**2)
#
## Collect indices of locations above, below and within RC region
#above_ind = np.where(cyl_z >= cd.cim.RC_T)[0]
#below_ind = np.where(cyl_z <= -cd.cim.RC_T)[0]
#within_ind = np.where((cyl_z < cd.cim.RC_T)*(cyl_z > -cd.cim.RC_T))[0]
#
## Predefine empty arrays for flux function results
#tmp = len(ksm_x)
#vec_potl = np.full((tmp), np.nan)
#
#for iii in above_ind:
#    vec_potl[iii] = cd.cim.RC_PARAM*spint.romberg(cd.cim.a_above,
#                                                cd.cim.INTMIN,
#                                                cd.cim.INTMAX,
#                                                args=(cyl_r[iii], cyl_z[iii]),
#                                                divmax=cd.cim.DIVMAX)
#
#for iii in below_ind:
#    vec_potl[iii] = cd.cim.RC_PARAM*spint.romberg(cd.cim.a_below,
#                                                cd.cim.INTMIN,
#                                                cd.cim.INTMAX,
#                                                args=(cyl_r[iii], cyl_z[iii]),
#                                                divmax=cd.cim.DIVMAX)
#
#for iii in within_ind:
#    vec_potl[iii] = cd.cim.RC_PARAM*spint.romberg(cd.cim.a_within,
#                                                cd.cim.INTMIN,
#                                                cd.cim.INTMAX,
#                                                args=(cyl_r[iii], cyl_z[iii]),
#                                                divmax=cd.cim.DIVMAX)
#
## Flux function for external field is magnetic vector potential x cyl_r for axisymmetric system
#ext_ff = vec_potl*cyl_r
## Planetary contribution to flux function
#planet_ff = cd.cim.f_planetary(cyl_r, cyl_z)
## Add them both up
#flux_function = ext_ff+planet_ff
#
#colat_south = np.full((len(flux_function)), np.nan)
#colat_north = np.full((len(flux_function)), np.nan)
#for iii in range(len(flux_function)):
#    colat_south[iii] = opt.brentq(cd.cim.map_ionos_squash_south, 0, np.pi/2, args=(flux_function[iii]))
#    colat_north[iii] = opt.brentq(cd.cim.map_ionos_squash_north, 0, np.pi/2, args=(flux_function[iii]))
#
#above = np.zeros((4,100))
#ctr = 0
#for jjj in range(357,361):
#    above[ctr,:] = [cd.cim.a_above(iii, cyl_r[jjj], cyl_z[jjj]) for iii in np.linspace(cd.cim.INTMIN,1,num=100)]
#    plt.plot(above[ctr,:])
#    test = cd.cim.RC_PARAM*spint.romberg(cd.cim.a_above,
#                                                        cd.cim.INTMIN,
#                                                        cd.cim.INTMAX,
#                                                        args=(cyl_r[jjj], cyl_z[jjj]),
#                                                        divmax=cd.cim.DIVMAX)
#    print(test)
#    
#    
#    test2 = cd.cim.RC_PARAM*spint.quad(cd.cim.a_above,0,10,args=(cyl_r[jjj], cyl_z[jjj]))[0]
#    print(test2)
#    
#    ctr += 1
#















# make figure
fig = plt.figure()
fig.set_size_inches(8,8)
ax = fig.add_subplot(111, projection='polar')

# north
ax.plot(tmp[3]*np.pi/12, tmp[1], 'r-')
# south
#ax.plot(tmp[3]*np.pi/12, tmp[2], 'r-')

# general figure setup 
ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
ax.set_xticklabels(['00','06','12','18'])
ax.set_yticks([10,20,30])
ax.set_yticklabels([])
ax.grid('on', color='w', linewidth=2)
ax.set_theta_zero_location("N")
ax.set_rmax(30)
ax.set_rmin(0) 

plt.show()

