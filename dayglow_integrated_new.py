import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cassinipy
import cubehelix
from datetime import datetime, timedelta
import get_image
import glob
from matplotlib.collections import PatchCollection
import matplotlib.cm as mcm
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
from matplotlib.patches import Polygon as mPolygon
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import numpy as np
import os
import remove_solar_reflection
import scipy.interpolate as sint
import spiceypy as spice
import time_conversions as tc
import uvisdb

plotsavepath = '{}/UVIS_dayglow'.format(plotpath)
if not os.path.exists(plotsavepath):
    os.makedirs(plotsavepath)

metakernelpath = '{}/SPICE/metakernels'.format(datapath)
spice.kclear()
spice.furnsh('{}/cassini_generic.mk'.format(metakernelpath))

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)

cd = cassinipy.CassiniData(datapath)

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF
imglist['ET_CENT'] = imglist[['ET_START','ET_STOP']].mean(axis=1)

minangle = 10

#%%
# =============================================================================
# Get dayglow intensity / power
# =============================================================================

szabins = np.linspace(0, 120, num=4*120+1)
#szabins = np.linspace(30, 120, num=4*90+1)
szacents = szabins[:-1] + np.mean(np.diff(szabins))/2

dts = np.arange(datetime(2007,1,1), datetime(2018,1,1), timedelta(hours=24)).astype(datetime)
ets = tc.datetime2et(dts)
etcent = ets[:-1] + np.mean(np.diff(ets))/2
dtcent = tc.et2datetime(etcent)

tags = ['', '_1250_1550', '_1650_1800']
folders = ['PROJECTIONS_RES2_ISS', 'PROJECTIONS_RES1_ISS_1250_1550', 'PROJECTIONS_RES1_ISS_1650_1800']

for iii in range(1, len(tags)):
    tag = tags[iii]
    folder = folders[iii]
    
    rsr = remove_solar_reflection.RemoveSolarReflection(imagepath='{}/UVIS/PDS_UVIS/{}'.format(datapath, folder))
    rsr.MIN_COLAT = 27

    npzfile = '{}/UVIS_SZA_wm2_kr_min{}{}'.format(gitpath, minangle, tag)
    try:
#        raise Exception
        tmp = np.load('{}.npz'.format(npzfile))
        dg_kr = tmp['dg_kr']
        dg_krcorr = tmp['dg_krcorr']
        dg_wm2 = tmp['dg_wm2']
        n_px = tmp['n_px']
        print('Daily averaged profiles loaded')
    except:
        dg_kr = np.full((len(etcent), len(szacents), 2), np.nan)
        dg_krcorr = np.full((len(etcent), len(szacents), 2), np.nan)
        dg_wm2 = np.full((len(etcent), len(szacents), 2), np.nan)
        n_px = np.full((len(etcent), len(szacents)), np.nan)
        
        diff_sec = 3600*12
        for ttt in range(len(etcent)):
            val = imglist[np.abs(imglist['ET_CENT']-etcent[ttt])<diff_sec]
            
            if not len(val):
                continue
        
            # dayglow INTENSITY + POWER
            file = val.iloc[0]['FILEPATH'].replace('PROJECTIONS_RES2_ISS', folder)
            bgfiles = val['FILEPATH'].as_matrix()
            try:
                rsr.calculate('{}{}'.format(uvispath.replace('PROJECTIONS_RES2_ISS',
                                                             folder),
                                            file),
                              bgfiles=bgfiles,
                              minangle=minangle,
                              bgpath=uvispath.replace('PROJECTIONS_RES2_ISS',
                                                      folder))
            except:
                # triggered if no bg pixels
                krvals = np.full((len(szacents)), np.nan)
                krcorrvals = np.full((len(szacents)), np.nan)
                wm2vals = np.full((len(szacents)), np.nan)
            else:
                sza = rsr.sza_flat
                kr = rsr.kr_flat
                krcorr = rsr.krcorr_flat
                wm2 = rsr.wm2_flat                
                
                arg = np.digitize(sza, szabins)-1
                krvals = np.zeros((len(szacents)))
                krcorrvals = np.zeros((len(szacents)))
                wm2vals = np.zeros((len(szacents)))
                krmads = np.zeros((len(szacents)))
                krcorrmads = np.zeros((len(szacents)))
                wm2mads = np.zeros((len(szacents)))
                num = np.zeros((len(szacents)))
                for iii in range(len(szacents)):
                    # simple binning
                    krvals[iii] = np.nanmedian(kr[arg==iii])
                    krcorrvals[iii] = np.nanmedian(krcorr[arg==iii])
                    wm2vals[iii] = np.nanmedian(wm2[arg==iii])
                    
                    krmads[iii] = np.nanmedian(np.abs(kr[arg==iii]-krvals[iii]))
                    krcorrmads[iii] = np.nanmedian(np.abs(krcorr[arg==iii]-krcorrvals[iii]))
                    wm2mads[iii] = np.nanmedian(np.abs(wm2[arg==iii]-wm2vals[iii]))
                    
                    num[iii] = len(kr[arg==iii])
                                        
            dg_kr[ttt, :, 0] = krvals
            dg_krcorr[ttt, :, 0] = krcorrvals
            dg_wm2[ttt, :, 0] = wm2vals
            
            dg_kr[ttt, :, 1] = krmads
            dg_krcorr[ttt, :, 1] = krcorrmads
            dg_wm2[ttt, :, 1] = wm2mads
            
            n_px[ttt, :] = num
                
                
        np.savez(npzfile,
                 dg_kr=dg_kr, dg_krcorr=dg_krcorr, dg_wm2=dg_wm2, n_px=n_px)
        
        # =============================================================================
        # Integrate dayglow power in reasonable range
        # SZA = polar angle of ideal sphere
        # =============================================================================
        RS = (58232 + 1100)*1e3#m
        SZAMIN=65
        SZAMAX=85
        inv = (szacents<SZAMIN) | (szacents>SZAMAX)
        sza = np.radians(szacents)
        sza[inv] = np.nan
        dsza = np.radians(np.diff(szabins))
        dsza[inv] = np.nan
        mult = np.sin(sza)*dsza*2*np.pi*RS**2
        
        mult_full = np.tile(mult, (dg_wm2.shape[0], 1))
        dg_int = np.nansum(dg_wm2[:,:,0]*mult_full, axis=-1)
        dg_int_err = np.sqrt(np.nansum((dg_wm2[:,:,1]*mult_full)**2, axis=-1))
        inv = np.where(np.sum(np.isnan(dg_wm2[:,:,0]*mult_full), axis=-1) != np.sum(inv))
        dg_int[inv] = np.nan 
        dg_int_err[inv] = np.nan 
        
        npzfile = '{}/UVIS_dg_ing_min{}{}'.format(gitpath, minangle, tag)
        np.savez(npzfile, dg_int=dg_int, dg_int_err=dg_int_err)
     
#%%
# =============================================================================
# Get sunsport numbers
# =============================================================================
def getShifted(et, dt, val):
    az_s = np.zeros_like(et)
    az_e = np.zeros_like(et)
    for iii in range(len(et)):
        if dt[iii]<datetime(2007,1,1) or dt[iii]>datetime(2017,9,1):
            continue
        vec_s, _ = spice.spkpos('SATURN', et[iii], 'IAU_SUN', 'XLT', 'SUN')
        az_s[iii] = np.arctan2(vec_s[1], vec_s[0])
        vec_e, _ = spice.spkpos('EARTH', et[iii], 'IAU_SUN', 'XLT', 'SUN')
        az_e[iii] = np.arctan2(vec_e[1], vec_e[0])
        
    opp_angle = (az_s-az_e)%(2*np.pi)
    opp_angle[opp_angle>(np.pi)] -= 2*np.pi
    # slightly positive opp angle = peak arrives at Saturn slightly delayed
    solar_rot_period = 24.47*24*3600 #s
    time_offs = opp_angle/2/np.pi * solar_rot_period
    et_sat = et+time_offs
    
    tmp = np.argsort(et_sat)
    et_sat = et_sat[tmp]
    dt_sat = tc.et2datetime(et_sat)
    val_sat = val[tmp]
    return et_sat, dt_sat, val_sat

# daily sunspots at Earth      
sunspfile = '{}/Solar data/Sunspot numbers/SN_d_tot_V2.0.txt'.format(boxpath)
tmp = np.genfromtxt(sunspfile, usecols=(0,1,2,4), dtype=np.int)
sunspdt = np.array([datetime(tmp[i,0], tmp[i,1], tmp[i,2]) for i in range(tmp.shape[0])])
sunspet = tc.datetime2et(sunspdt)
sunspn = tmp[:,-1].astype(np.float)
sunspet_sat, sunspdt_sat, sunspn_sat = getShifted(sunspet, sunspdt, sunspn)

# daily Mg II index at Earth
mgfile = '{}/Solar data/Mg_II/MgII_composite.dat'.format(boxpath)
tmp = np.genfromtxt(mgfile, usecols=(0,1,2,3), dtype=np.float, comments=';')
mgdt = np.array([datetime(int(tmp[iii,0]), int(tmp[iii,1]), int(tmp[iii,2]))
                    for iii in range(tmp.shape[0])])
mget = tc.datetime2et(mgdt)
mgind = tmp[:,-1]
mget_sat, mgdt_sat, mgind_sat = getShifted(mget, mgdt, mgind)


## monthly
#sunspfile_m = '{}/Solar data/Sunspot numbers/SN_ms_tot_V2.0.txt'.format(boxpath)
#tmp = np.genfromtxt(sunspfile_m, usecols=(0,1,3), dtype=np.int)
#sunspdt_m = np.array([datetime(tmp[i,0], tmp[i,1], 15) for i in range(tmp.shape[0])])
#sunspn_m = tmp[:,-1].astype(np.float)

#%%
# =============================================================================
# Plot 2014 section timeline
# =============================================================================
panels = 'abcdefg'
paneliter = iter(panels)
def addLabel(ax, x=0.07, xplus=0.02, y=0.9, specialtext=''):
    txt = ax.text(x, y, '({})'.format(next(paneliter)),
                  transform=ax.transAxes, ha='right', va='center',
                  color='k', fontweight='bold', fontsize=15)
    txt.set_path_effects([PathEffects.withStroke(linewidth=3.5,
                                                 foreground='w')])
    
    txt = ax.text(x+xplus, y, specialtext,
                  transform=ax.transAxes, ha='left', va='center',
                  color='k', fontsize=11)
    txt.set_path_effects([PathEffects.withStroke(linewidth=3.5,
                                                 foreground='w')])
    
cm = mcm.get_cmap('viridis')
colorlist = [cm(iii) for iii in np.linspace(0.2, 0.7, num=2)]

fig = plt.figure()
fig.set_size_inches(8, 12)
gs = gridspec.GridSpec(5,2, hspace=0.1, wspace=0.4, height_ratios=(1,1,1,0.3,0.85))
ax_w = plt.subplot(gs[0,:])
ax_w2 = plt.subplot(gs[1,:], sharex=ax_w)
ax_s = plt.subplot(gs[2,:], sharex=ax_w)


# plot dayglow power
dg_vals = list()
tags = ['_1250_1550', '_1650_1800']
axs = [ax_w, ax_w2]
for ttt in range(len(tags)):
    ax = axs[ttt]
    npzfile = '{}/UVIS_dg_ing_min{}{}.npz'.format(gitpath, minangle, tags[ttt])
    data = np.load(npzfile)
    dg_int = data['dg_int']
    dg_int_err = data['dg_int_err']
    x = dtcent
    y = dg_int/1e9
    ax.errorbar(x, y, yerr=dg_int_err/1e9, color='k', zorder=3, marker='o',
                label='{}-{} $\mathrm{{\AA}}$'.format(
                        tags[ttt].split('_')[1],
                        tags[ttt].split('_')[2]))
    val = np.where(np.isfinite(y) &
                   (x>datetime(2014,5,17)) &
                   (x<datetime(2014,6,17)))
    if not ttt:
        ax.set_ylim([10, 15])
    else:
        ax.set_ylim([50, 95])
    ax.set_xlim([datetime(2014,5,17,1), datetime(2014,6,15)])
    ax.set_ylabel('Airglow power (GW)')
    addLabel(ax, xplus=0.01, specialtext='{}-{} $\mathrm{{\AA}}$'.format(
                        tags[ttt].split('_')[1],
                        tags[ttt].split('_')[2]))
    
    dg_vals.append(dg_int)

# plot small globe
for ax_parent in [ax_w]:
    w = 0.3
    bbox = ax_w.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    width, height = bbox.width, bbox.height
    size = w*width
    h = size/height
    ax = inset_axes(ax_parent,
                    width="100%", height="100%", loc=9,
                    bbox_to_anchor=(1-w/3, 0-h/2, w, h),
                    bbox_transform=ax_parent.transAxes,
                    axes_class=Axes3D,
                    )
    ax.set_zorder(0)
    ax.patch.set_alpha(0)
    
    
    rotatedeg = -26
    r = 100
    ELEV=15
    AZIM=75
    viewdir = np.array([np.cos(np.radians(AZIM))*np.cos(np.radians(ELEV)),
                        np.sin(np.radians(AZIM))*np.cos(np.radians(ELEV)),
                        np.sin(np.radians(ELEV))])
    
    az_steps = np.radians(np.arange(-180,181,15))
    el_steps = np.radians(np.arange(-90,91,10))
    
    az_range = np.radians(np.linspace(-180,180,num=181))
    el_range = np.radians(np.linspace(-90,90,num=181))
    
    kwargs_thin = dict(color='k', lw=0.5, alpha=0.3)
    kwargs_mid = dict(color='k', lw=0.8, alpha=0.5, zorder=3)
    kwargs_bold = dict(color='k', lw=1.5, zorder=12)
    
    def rotateDipole(x, y, z, deg):
        angle = np.radians(deg)
        x_new = x*np.cos(angle)-z*np.sin(angle)
        z_new = x*np.sin(angle)+z*np.cos(angle)
        return x_new, y, z_new
    
    def nanInvis(x, y, z):
        thissum = x*viewdir[0]+y*viewdir[1]+z*viewdir[2]
        angle = np.degrees(np.arccos(thissum/np.linalg.norm(viewdir)/np.sqrt(x**2+y**2+z**2)))
        inv = np.where(angle>90)
        x[inv] = np.nan
        y[inv] = np.nan
        z[inv] = np.nan
        return x, y, z
    
    # plot parallels
    for el in el_steps[:-1]:
        x = r*np.cos(el)*np.cos(az_range)
        y = r*np.cos(el)*np.sin(az_range)
        z = r*np.sin(el)
        x, y, z = rotateDipole(x, y, z, rotatedeg)
        x, y, z = nanInvis(x, y, z)
        ax.plot(x, y, z, **kwargs_mid)
        
    # plot meridians
    for az in az_steps[:-1]:
        x = r*np.cos(el_range)*np.cos(az)
        y = r*np.cos(el_range)*np.sin(az)
        z = r*np.sin(el_range)
        x, y, z = rotateDipole(x, y, z, rotatedeg)
        x, y, z = nanInvis(x, y, z)
        ax.plot(x, y, z, **kwargs_mid)
        
    # plot rotation axis
    for lim in [[-1.2, -1.1],[1.01, 1.2]]:
        z = np.linspace(lim[0]*r, lim[1]*r, num=100)
        x = np.zeros_like(z)
        y = np.zeros_like(z)
        x, y, z = rotateDipole(x, y, z, rotatedeg)
    #    x, y, z = nanInvis(x, y, z)
        ax.plot(x, y, z, **kwargs_bold)
    
    # plot illumination
    u = np.radians(np.linspace(0, 360, 361))
    v = np.radians(np.linspace(180, 0, 361))
    r_surf = r-1
    x = r_surf * np.outer(np.ones(np.size(u)), np.cos(v))
    y = r_surf * np.outer(np.cos(u), np.sin(v))
    z = r_surf * np.outer(np.sin(u), np.sin(v))
    cval = np.tile(np.flip(v, axis=0), (len(u), 1))
    c = np.zeros(cval.shape+(4,))
    for iii in range(cval.shape[0]):
        for jjj in range(cval.shape[1]):
            val = 2*(cval[iii,jjj]-np.pi/2)/np.pi+0.2
            if val<0: val=0
            if val>1: val=0.999
            c[iii,jjj,:] = cmap_UV(val)
            c[iii,jjj,:] = plt.cm.gist_gray(val)
    ax.plot_surface(x, y, z, facecolors=c, zorder=5, alpha=1,
                    rcount=len(u), ccount=len(v))
    
    # plot integrated band  
    u = np.radians(np.linspace(-80, 105, 361))
    v = np.radians(np.linspace(SZAMIN, SZAMAX, 50))
    r_surf = r+1
    x = r_surf * np.outer(np.ones(np.size(u)), np.cos(v))
    y = r_surf * np.outer(np.cos(u), np.sin(v))
    z = r_surf * np.outer(np.sin(u), np.sin(v))
    ax.plot_surface(x, y, z, color='crimson', zorder=12, alpha=1)
    
    def set_axes_radius(ax, origin, radius):
        ax.set_xlim3d([origin[0] - radius, origin[0] + radius])
        ax.set_ylim3d([origin[1] - radius, origin[1] + radius])
        ax.set_zlim3d([origin[2] - radius, origin[2] + radius])
    
    def set_axes_equal(ax):
        limits = np.array([
            ax.get_xlim3d(),
            ax.get_ylim3d(),
            ax.get_zlim3d(),
        ])
        origin = np.mean(limits, axis=1)
        radius = 0.5 * np.max(np.abs(limits[:, 1] - limits[:, 0]))
        set_axes_radius(ax, origin, radius)
        
    
    ax.axis('off')
    ax.view_init(elev=ELEV, azim=AZIM)
    ax.dist=6
    set_axes_equal(ax)  
    
def get_offset(data, somespline):
    MINDT = datetime(2014,5,17)
    MAXDT = datetime(2014,6,16)
    val = np.where((dtcent>MINDT) & (dtcent<MAXDT))[0]
    dgdt_sel = dtcent[val]
    dgpower_sel = data[val]/1e9
    etshifts = np.linspace(-5*24*3600,
                           5*24*3600, num=500)
    corrcoeffs = np.zeros_like(etshifts)
    for iii in range(len(etshifts)):
        ydata = somespline(tc.datetime2et(dgdt_sel+timedelta(seconds=etshifts[iii])))
        argval = np.where(np.isfinite(dgpower_sel))[0]
        covxy = np.cov(dgpower_sel[argval], ydata[argval])[0,1]
        stdx = np.std(dgpower_sel[argval])
        stdy = np.std(ydata[argval])
        corrcoeffs[iii] = covxy/stdx/stdy
    return etshifts[np.argmax(corrcoeffs)]

# plot sunspot number
sunsp_sat_spline = sint.CubicSpline(sunspet_sat, sunspn_sat)
sunsp_offs_sec = get_offset(np.sum(dg_vals, axis=0), sunsp_sat_spline)
dtrange = np.arange(datetime(2014,5,16), datetime(2014,6,17),
                    timedelta(hours=1)).astype(datetime)
ax_s.plot(dtrange, sunsp_sat_spline(tc.datetime2et(dtrange)),
          color=colorlist[0], label='seen from Saturn')
ax_s.set_ylim([40,210])
ax_s.set_ylabel('Daily sunspot number')
ax_s.yaxis.label.set_color(colorlist[0])
ax_s.tick_params(axis='y', colors=colorlist[0])

# plot Mg II index
mg_sat_spline = sint.CubicSpline(mget_sat, mgind_sat)
mg_offs_sec = get_offset(np.sum(dg_vals, axis=0), mg_sat_spline)
ax_m = ax_s.twinx()
ax_m.plot(dtrange, mg_sat_spline(tc.datetime2et(dtrange)),
          color=colorlist[1], label='Mg II index')
ax_m.plot(dtrange[0], [np.nan], color=colorlist[0],
          label='Sunspot number', zorder=5)
ax_m.set_ylabel('Daily Mg II index', rotation=270, labelpad=12)
ax_m.legend(loc=9, bbox_to_anchor=(0.3,1),
            shadow=True, facecolor='w', edgecolor='k', fancybox=True)
ax_m.yaxis.label.set_color(colorlist[1])
ax_m.tick_params(axis='y', colors=colorlist[1])

ax_s.xaxis.set_major_locator(mdates.DayLocator(interval=3))
ax_s.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%j'))
ax_s.xaxis.set_minor_locator(mdates.DayLocator())

for ax in [ax_w, ax_w2, ax_s, ax_m]:
    ax.tick_params(axis='both', which='both', direction='in',
                   left=False if ax==ax_m else True,
                   labelleft=False if ax==ax_m else True,
                   right=True if not ax==ax_s else False,
                   labelright=True if ax==ax_m else False,
                   top=True, labeltop=False,
                   bottom=True, labelbottom=True if ax==ax_s else False)
plt.setp(ax_s.get_xticklabels(), rotation=30, ha='right')
addLabel(ax_s)

# select subset we're interested in
MINDT = datetime(2014,5,17)
MAXDT = datetime(2014,6,16)
val = np.where((dtcent>MINDT) & (dtcent<MAXDT))[0]
dgdt_sel = dtcent[val]
for ttt in range(len(tags)):
    dgpower_sel = dg_vals[ttt][val]/1e9
    sunsp_offs_sec = get_offset(dg_vals[ttt], sunsp_sat_spline)
    mg_offs_sec = get_offset(dg_vals[ttt], mg_sat_spline)
    ax = plt.subplot(gs[-1,ttt])
    bx = ax.twinx()
    for iii in range(2):
        thisax = [ax,bx][iii]
        if iii:
            ydata = mg_sat_spline(tc.datetime2et(dgdt_sel+timedelta(seconds=mg_offs_sec)))
            if ttt:
                thisax.set_ylabel('Mg II index', rotation=270, labelpad=15)
            thisax.set_ylim([0.154, 0.166])
            thisax.set_yticks(np.arange(0.154, 0.166, 0.002))
        else:
            ydata = sunsp_sat_spline(tc.datetime2et(dgdt_sel+timedelta(seconds=sunsp_offs_sec)))
            if not ttt:
                thisax.set_ylabel('Sunspot number')
            thisax.set_ylim([0, 250])
        thisax.scatter(dgpower_sel, ydata, s=10, color=colorlist[iii])
        argval = np.where(np.isfinite(dgpower_sel))[0]
        covxy = np.cov(dgpower_sel[argval], ydata[argval])[0,1]
        stdx = np.std(dgpower_sel[argval])
        stdy = np.std(ydata[argval])
        p = covxy/stdx/stdy
        
        thisax.tick_params(axis='both', which='both', direction='in', top=True)

        tdiff = sunsp_offs_sec if not iii else mg_offs_sec
        ax.text(0.05, 0.95 if not iii else 0.85, '$p={:0.2f}\,\,(-{:0.1f}\,\mathrm{{d}})$'.format(p, tdiff/3600/24),
                transform=ax.transAxes,
                ha='left', va='top',
                color=colorlist[iii],
                fontweight='bold')
        
    ax.set_xlabel('Airglow power (GW)')
    ax.set_xlim([10, 14.5] if not ttt else [47, 87])
    
    addLabel(ax, x=0.17, y=1.1, specialtext='{}-{} $\mathrm{{\AA}}$'.format(
                        tags[ttt].split('_')[1],
                        tags[ttt].split('_')[2]))
    ax.yaxis.label.set_color(colorlist[0])
    ax.tick_params(axis='y', colors=colorlist[0])
    bx.yaxis.label.set_color(colorlist[1])
    bx.tick_params(axis='y', colors=colorlist[1])

plt.savefig('{}/2014_section_min{}.png'.format(plotsavepath, minangle),
            bbox_inches='tight', dpi=300)
plt.close()

#%%
# =============================================================================
# Plot statistical dayglow power / brightness vs SZA
# =============================================================================

panels = 'abcdefg'
paneliter = iter(panels)

def addLabel(ax, specialtext=''):
    txt = ax.text(0.12, 0.95, '({})'.format(next(paneliter)),
                  transform=ax.transAxes, ha='right', va='center',
                  color='k', fontweight='bold', fontsize=15)
    txt.set_path_effects([PathEffects.withStroke(linewidth=3.5, foreground='w')])
    
    txt = ax.text(0.13, 0.95, specialtext,
                  transform=ax.transAxes, ha='left', va='center',
                  color='k', fontsize=11)
    txt.set_path_effects([PathEffects.withStroke(linewidth=3.5, foreground='w')])

fig = plt.figure()
fig.set_size_inches(9,4.5)
gs = gridspec.GridSpec(1, 2, hspace=0.1)
ax_1250 = plt.subplot(gs[0,0])
ax_1650 = plt.subplot(gs[0,1], sharex=ax_1250)

# good limits:
# min20: 300px, 25days

cm = mcm.get_cmap('viridis')
clist = [cm(iii) for iii in np.linspace(0, 1, num=11)]
colorlist = [clist[1], clist[7]]
#colorlist = ['royalblue','crimson']
colorlist = ['k','royalblue']

for aaa in range(2):
    ax = [ax_1250, ax_1650][aaa]
    tag = ['_1250_1550', '_1650_1800'][aaa]
    
    npzfile = '{}/UVIS_SZA_wm2_kr_min{}{}.npz'.format(gitpath, minangle, tag)
    data = np.load(npzfile)
    dg_wm2 = data['dg_wm2']
    n_px = data['n_px']
    
    
    data = dg_wm2[:,:,0]*1e6
    data[n_px[:,:]<300] = np.nan
    count = np.sum(np.isfinite(data), axis=0)
    med = np.nanmedian(data, axis=0)
    err = np.nanmedian(np.abs(data-med), axis=0)
    
    mean = np.nanmean(data, axis=0)
    std = np.nanstd(data, axis=0)
    
    # reverse engineer corrected brightness from power
    for sctr in range(2):
        sss = [mean, std][sctr]
        sss = sss/4/np.pi
        sss /= 1.6e-18
        sss = sss/1e3*(4*np.pi*1e-10)
        sss = sss/1e6 if aaa else sss/1e3
        if not sctr:
            mean = sss
        else:
            std = sss

    val=np.where(count>30)[0]
#        val = np.where(np.isfinite(count))[0]
    
#        ax.plot(szacents[val], med[val], color = color,
#                label = 'Northern hemisphere' if not hhh else 'Southern hemisphere')
#        ax.fill_between(szacents[val], med[val]-err[val],
#                        med[val]+err[val], color=color, alpha=0.3)
    
    ax.plot(szacents[val], mean[val], color = colorlist[0],
            label='auroral slews')
    ax.fill_between(szacents[val], mean[val]-std[val],
                    mean[val]+std[val], color=colorlist[0], alpha=0.2)
    
    # get values from stares
    specsavefile = '{}/UVIS/sorted_spectra/bandbin_1_corr/sza_brightness_24_{}.npz'.format(datapath,
                    'DAYGLOW' if not aaa else 'REFL_SUNLIGHT')
    specdata = np.load(specsavefile)
    szabins_spec = specdata['szabins']
    szacents_spec = specdata['szacents']
    szabrightness_spec = specdata['szabrightness']
    szabrightness_err_spec = specdata['szabrightness_err']
    specnum = specdata['specnum']
    ydata = szabrightness_spec*(1 if aaa else 1e3)
    yerr = szabrightness_err_spec*(1 if aaa else 1e3)
#    if not aaa:
#        ydata[yerr>200] = np.nan
    ax.plot(szacents_spec, ydata, color = colorlist[1],
            label = 'avg spectra', ls='--')
    ax.fill_between(szacents_spec, ydata-yerr,
                    ydata+yerr, color=colorlist[1], alpha=0.2)
#    ax.plot([-1], [0], color = colorlist[1],
#            label = 'average spectra')
#    ax.errorbar(szacents_spec, ydata, yerr=yerr, color=colorlist[1], ls='none')

    ax.legend(loc=1, shadow=True, facecolor='w', edgecolor='k', fancybox=True)
    ax.set_xlabel('Solar zenith angle (deg)')
    ax.set_ylabel('Brightness ({})'.format('kR' if aaa else 'R'))
    ax.set_xlim([20,110])
    ax.set_ylim([0, 550 if not aaa else 6])
    ax.grid()  
    addLabel(ax, specialtext='{}-{} $\mathrm{{\AA}}$'.format(
                        tag.split('_')[1],
                        tag.split('_')[2]))
    ax.tick_params(axis='both', which='both', direction='in',
                   left=True, labelleft=True,
                   right=True, labelright=False,
                   top=True, labeltop=False,
                   bottom=True, labelbottom=True)
            
    
plt.savefig('{}/sza_vs_dg_min{}{}.png'.format(plotsavepath, minangle, 'speccomp'), bbox_inches='tight', dpi=300)
plt.savefig('{}/sza_vs_dg_min{}{}.pdf'.format(plotsavepath, minangle, 'speccomp'), bbox_inches='tight')
plt.close()
#%%
# =============================================================================
# Plot example
# =============================================================================
samplefiles = ['2017_212T14_48_30.fits',
               '2017_212T15_29_26.fits',
               '2017_212T16_51_34.fits',
               '2017_212T17_30_35.fits',
               '2017_212T18_11_31.fits',
               '2017_212T18_51_38.fits',
               '2017_212T19_45_46.fits',
               '2017_212T20_38_10.fits',#7
               '2017_212T21_32_02.fits',#8
               '2017_212T22_24_44.fits',
               ]

samplefiles = glob.glob('{}/UVIS_ALL/20190402043107/2014/1xx/2014_145*'.format(plotpath)) #COUVIS_0047
samplefiles = glob.glob('{}/UVIS_ALL/20190402043107/2016/3xx/2016_303*'.format(plotpath)) #COUVIS_0057
#samplefiles = glob.glob('{}/UVIS_ALL/20190402043107/2017/0xx/2017_072*'.format(plotpath)) #COUVIS_0058
#samplefiles = glob.glob('{}/UVIS_ALL/20190402043107/2017/0xx/2017_079*'.format(plotpath))
#samplefiles = glob.glob('{}/UVIS_ALL/20190402043107/2017/0xx/2017_087*'.format(plotpath))
samplefiles = np.array(['{}.fits'.format(iii.split('.')[0].split('/')[-1].split('\\')[-1])
                        for iii in samplefiles])

#%%

panels = 'abcdefg'
paneliter = iter(panels)

fig = plt.figure()
gs = gridspec.GridSpec(2, 4, width_ratios=(1, 0.1, 1, 0.07), height_ratios=(1.5,1),
                       wspace=0.1, hspace=0.2)
fig.set_size_inches(8, 9.6)

KR_MIN = 0.5
KR_MAX = 30
rmin = 0
rmax = 50
    
# plot image
ax = plt.subplot(gs[0,:3], projection='polar')
thisfile = samplefiles[0]
sampledir = 'E:/data/UVIS/PDS_UVIS/PROJECTIONS_RES2_ISS/COUVIS_0057'
file = '{}/{}'.format(sampledir, thisfile)
fmt = '%Y_%jT%H_%M_%S'
thisdt = datetime.strptime(thisfile.split('\\')[-1].split('/')[-1].split('.')[0], fmt)
thiset = tc.datetime2et(thisdt)

fimg, fangles = get_image.getFullUVIS(file)
rimg, rangles, hem = get_image.reduceImage(fimg, fangles)
if hem=='North':    
    fimg = np.flip(fimg, axis=1)
    fangles = np.flip(fangles, axis=1)
data = fimg
data[data<0.1] = 0.1

hdu = fits.open(file)[0]
exp = hdu.header['TOTALEXP']
_, r, lat, loct = cd.get_locations([thiset+exp/2], refframe='KRTP')
fmt = '%Y-%j, %H:%M:%S'
title = '{}\n{}$\,$s, {} ({:.2f} R$_S$)'.format(datetime.strftime(thisdt, fmt),
         exp, hem, r-1)
ax.set_title(title, y=1.02, fontsize=10)

# plot
lonbins = np.linspace(0, 2*np.pi, num=np.shape(data)[0]+1)
colatbins = np.linspace(0, 180, num=np.shape(data)[1]+1)
loncent = lonbins[:-1] + np.diff(lonbins)/2
colatcent = colatbins[:-1] + np.diff(colatbins)/2
quad = ax.pcolormesh(lonbins, colatbins, data.T, cmap=cmap_UV)
quad.set_clim(0, KR_MAX)
quad.set_norm(mcolors.LogNorm(KR_MIN, KR_MAX))
ax.set_facecolor('gray')

# plot colorbar
cbar = plt.colorbar(quad, cax=plt.subplot(gs[0,-1]))
cbar.set_label('Intensity (kR)', labelpad=10, rotation=270, fontsize=11)
cbar.set_ticks(np.append(np.append(np.arange(KR_MIN,1,0.1),
                                   np.arange(1,10,1)),
                         np.arange(10,KR_MAX+1,10)))

# set ticks and grids
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    tmp = np.degrees(ticks[iii])
    txt = ax.text(ticks[iii], rmax-5, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                  ha='center',va='center', zorder=5)
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax.set_xticks(ticks)
ax.set_xticks(np.linspace(0, 2*np.pi, 25), minor=True)
ax.set_xticklabels([])
ax.set_yticks(np.arange(10, rmax+1, 10))
ax.set_yticks(np.arange(5, rmax+1, 10), minor=True)
ax.set_yticklabels([])
ax.grid('on', color='0.8', which='major',
        linewidth=1)
ax.grid('on', color='0.8', linestyle='--', which='minor',
        linewidth=0.5)

# highlight sections where elevation angles too low and aurora is excluded
exclude = fangles<minangle+1
p = []
if np.any(exclude):
    lowerlon = []
    lowerlat = []
    upperlon = []
    upperlat = []
    for lll in range(exclude.shape[0]):
        if np.any(exclude[lll,:]):
            latcut = np.where(exclude[lll,:])[0]
            lowerlon = np.append(lowerlon, [lonbins[lll]])
            upperlon = np.append(upperlon, [lonbins[lll]])
            lowerlat = np.append(lowerlat, [colatbins[np.min(latcut)]])
            upperlat = np.append(upperlat, [colatbins[np.max(latcut)+1]])
    lowerlon = np.append(lowerlon, [lowerlon[-1]+np.mean(np.diff(lonbins))])
    upperlon = np.append(upperlon, [upperlon[-1]+np.mean(np.diff(lonbins))])
    lowerlat = np.append(lowerlat, [lowerlat[-1]])
    upperlat = np.append(upperlat, [upperlat[-1]])
    
    lons = np.append(lowerlon, np.flip(upperlon, axis=0))
    lats = np.append(lowerlat, np.flip(upperlat, axis=0))

    tmp = mPolygon(np.array([lons, lats]).T)
    p.append(tmp)
    
thisc = 'silver'
if len(p):
    
    ax.add_collection(PatchCollection(p, facecolor=thisc,
                                      edgecolor='none',
                                      alpha=0.5, zorder=5))
    ax.add_collection(PatchCollection(p, facecolor='none',
                                      edgecolor=thisc, linewidth=1,
                                      alpha=1, zorder=5))

# highlight excluded aurora section
ax.fill_between(lonbins, np.zeros_like(lonbins),
                np.full_like(lonbins, rsr.MIN_COLAT),
                facecolor=thisc, edgecolor='none',
                alpha=0.5, zorder=5)
ax.plot(lonbins, np.full_like(lonbins, rsr.MIN_COLAT),
        c=thisc, lw=1, zorder=5)

ax.set_rorigin(0)
ax.set_rmax(rmax)
ax.set_rmin(rmin)
ax.set_theta_zero_location('N')
txt = ax.text(0, 1.12, '({})'.format(next(paneliter)),
              transform=ax.transAxes, ha='left', va='top',
              color='k', fontweight='bold', fontsize=15)
    

# plot histogram
for tctr in range(2):
    ax = plt.subplot(gs[1, 2 if tctr else 0])
    
    sampledir = 'E:/data/UVIS/PDS_UVIS/PROJECTIONS_RES1_ISS_{}/COUVIS_0057'.format(
            '1650_1800' if tctr else '1250_1550')
    #rsrsamplefiles =
    rsr = remove_solar_reflection.RemoveSolarReflection(
            imagepath='E:/data/UVIS/PDS_UVIS/PROJECTIONS_RES1_ISS_{}'.format(
                    '1650_1800' if tctr else '1250_1550')) 
    rsr.calculate('{}/{}'.format(sampledir, samplefiles[0]),
                  bgfiles=samplefiles, bgpath=sampledir, minangle=minangle+15)
    
    histszabins = np.linspace(50, 105, 150)
    if tctr:
        histkrbins = np.linspace(0, 3, 200)
    else:
        histkrbins = np.linspace(0, 0.4, 200)
    # reverse engineer corrected brightness from power
    tmp = np.copy(rsr.wm2_flat)
    tmp = tmp/4/np.pi
    tmp /= 1.6e-18
    tmp = tmp/1e3*(4*np.pi*1e-10)
    rsrkr = tmp
    
    hist2d, _, _ = np.histogram2d(rsr.sza_flat, rsrkr,
                                  bins=[histszabins, histkrbins])
            
    szabinsum = np.sum(hist2d, axis=1)
    szabincount = np.tile(szabinsum, (len(histkrbins)-1,1)).T
    
    quad = ax.pcolormesh(histszabins, histkrbins, (hist2d/szabincount).T*100, cmap=cmap_UV)
    quad.set_norm(mcolors.LogNorm(0.1,30))
#    quad.set_norm(mcolors.Normalize(vmin=0,vmax=30))
    cbar = plt.colorbar(quad, cax=plt.subplot(gs[1,-1]))
    cbar.set_label('% of pixels per solar zenith angle bin', labelpad=12, fontsize=11, rotation=270)
    ax.set_facecolor('gray')
    
    sza = rsr.sza_flat
    kr = rsrkr
    
    arg = np.digitize(sza, szabins)-1
    krvals = np.zeros((len(szacents)))
    krmads = np.zeros((len(szacents)))
    for iii in range(len(szacents)):
        krvals[iii] = np.nanmedian(kr[arg==iii])    
        krmads[iii] = np.nanmedian(np.abs(kr[arg==iii]-krvals[iii]))
        
    thisc='silver'
    ax.plot(szacents, krvals, ls='-', c=thisc, zorder=6)
    
    ax.fill_between(szacents,
                    krvals-krmads,
                    krvals+krmads,
                    color=thisc, alpha=0.3,
                    zorder=20)
    
    ax.set_xlabel('Solar zenith angle (deg)', fontsize=10)
    ax.set_ylabel('Brightness (kR)', fontsize=10)
    
    ax.set_xlim([histszabins[0], histszabins[-1]])
    ax.set_ylim([histkrbins[0], histkrbins[-1]])
    ax.grid()
    ax.tick_params(axis='both', which='both', direction='in',
                   left=True, labelleft=True,
                   right=True, labelright=False,
                   top=True, labeltop=False,
                   bottom=True, labelbottom=True)
    txt = ax.text(-0.2, 1.09, '({})'.format(next(paneliter)),
                  transform=ax.transAxes, ha='left', va='center',
                  color='k', fontweight='bold', fontsize=15)
    tag = ['_1250_1550', '_1650_1800'][tctr]
    txt = ax.text(-0.05, 1.09, '{}-{} $\mathrm{{\AA}}$'.format(
                        tag.split('_')[1],
                        tag.split('_')[2]),
                  transform=ax.transAxes, ha='left', va='center',
                  color='k', fontsize=11)
    
plt.savefig('{}/example_{}.png'.format(plotsavepath, samplefiles[0][:8]), bbox_inches='tight', dpi=300)
plt.close()

#%%
# =============================================================================
# Plot other stuff
# =============================================================================
        
#valn = np.where(np.nanmean(n_px_all[2,:,:], axis=-1)>2000)[0]
#plt.figure()
#plt.scatter(dtcent_all[valn], dg_int_all[2,valn], s=5)
##plt.scatter(dtcent_all, dg_int_all[1,:], s=5)
#plt.twinx()   
#plt.plot(sunspdt_m, sunspn_m)   
#plt.xlim([datetime(2005,1,1), datetime(2018,1,1)])
#
#plt.figure()
#val = np.where(n_img_all[0,:]>20)[0]
#plt.scatter(dtcent_all[val], n_img_all[0,val], s=5)
#val = np.where(n_img_all[1,:]>20)[0]
#plt.scatter(dtcent_all[val], n_img_all[1,val], s=5)
#plt.twinx()   
#plt.plot(sunspdt_m, sunspn_m)   
#plt.xlim([datetime(2014,5,17,1), datetime(2014,6,15)])

#plt.figure()
#plt.scatter(dtcent, dayglowwm2[0,:,50], s=5)
#plt.scatter(dtcent, dayglowwm2[1,:,50], s=5)
#
#plt.figure()
#plt.plot(szacents, np.sum(np.isfinite(dayglowwm2[0,:,:]), axis=0))
#plt.plot(szacents, np.sum(np.isfinite(dayglowwm2[1,:,:]), axis=0))

        
        