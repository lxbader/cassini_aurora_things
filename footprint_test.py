# find UVIS images taken when Cassini's footprint crossed the auroral region
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

tstart = '2017-017T12_00_00'
tstop = '2017-023T00_00_00'

fmt = '%Y-%jT%H_%M_%S'
etstart = tc.datetime2et(datetime.strptime(tstart, fmt))
etstop = tc.datetime2et(datetime.strptime(tstop, fmt))

times = np.linspace(etstart,etstop,num=100)
footp = cd.get_ionfootp(times)
loc = cd.get_locations(times, refframe='KSM')

fig = plt.figure()
ax = fig.add_subplot(111, projection='polar')
ax.plot(footp[3]/12*np.pi, footp[1])
ax.set_theta_zero_location('N')
plt.show()

plt.plot(times, footp[1])
plt.title('Colat N')
plt.show()

plt.plot(times, np.sqrt(loc[1]**2+loc[2]**2))
plt.title('sqrt(x**2+y**2)')
plt.axhline(cd.cim.RC_OUTER)
plt.show()

plt.plot(times, loc[3])
plt.title('z')
plt.axhline(cd.cim.RC_T)
plt.axhline(-cd.cim.RC_T)
plt.show()
