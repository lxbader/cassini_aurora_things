import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
from datetime import datetime
import glob
import numpy as np
import os
import plot_HST_UVIS
import scipy.io as sio
import time
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

TIME_DIFF_SEC = 3600*24
KR_MAX_UVIS = 10
KR_MAX_HST = 20

savepath = '%s/Plots/Greg/%s/' % (boxpath,time.strftime('%Y%m%d%H%M%S'))
if not os.path.exists(savepath):
    os.makedirs(savepath)

# import HST and UVIS image lists
hstlist = np.load('%s/HST/HSTlist.npz' % datapath)['data'][()]
uvislist = np.load('%s/UVIS/UVISlist_new.npz' % datapath)['data'][()]

# center of each exposure
hsttimes = np.mean(hstlist['ET_START_STOP'], axis=1)
uvistimes = np.mean(uvislist['ET_START_STOP'], axis=1)

# get FAC times
facpath = '%s/Greg FAC times' % boxpath
facfiles = glob.glob('%s/*.sav' % facpath)

facdata = np.zeros((0,5))
fachems = np.zeros((0))

for fctr in range(len(facfiles)):
    fff = facfiles[fctr]    
    savdata = sio.readsav(fff)
    for key in [*savdata]:
        tmpdata = savdata[[*savdata][0]]
        facdata = np.append(facdata,tmpdata,axis=0)
        if 'sh' in key:
            fachems = np.append(fachems, np.full((np.shape(tmpdata)[0]),'South'))
        else:
            fachems = np.append(fachems, np.full((np.shape(tmpdata)[0]),'North'))
            
garb, argunique = np.unique(facdata[:,1], return_index=True)

facdata = facdata[argunique]
fachems = fachems[argunique]
factimes = np.array([])

hstind = np.array([])
uvisind = np.array([])
    
for scpass in range(np.shape(facdata)[0]):
    doy2004 = facdata[scpass,1:]    
    ettimes = tc.doy20042et(doy2004)
    
    # Get close images
    hstind = np.append(hstind, np.where(np.abs(hsttimes-np.mean(ettimes))<TIME_DIFF_SEC)[0])
    uvisind = np.append(uvisind, np.where(np.abs(uvistimes-np.mean(ettimes))<TIME_DIFF_SEC)[0])
    
    factimes = np.append(factimes, np.mean(ettimes))
    
hstind = np.unique(hstind)
uvisind = np.unique(uvisind)

def makeSections(argfac, facdata, fachems):
    sections = []
    for iii in argfac:
        doy2004 = facdata[iii,1:]    
        ettimes = tc.doy20042et(doy2004)
        hem = fachems[iii]
        etstrings = tc.et2datetime(ettimes[::2])
        etstrings = ['%s, %s' % (datetime.strftime(iii, '%y-%j %H:%M'), hem) for iii in etstrings]
        sections.append([ettimes[0], ettimes[1], etstrings[0]])
        sections.append([ettimes[2], ettimes[3], etstrings[1]])
    return sections


#for hstindctr in hstind:
#    hstindctr = int(hstindctr)
#    argfac = np.where(np.abs(hsttimes[hstindctr]-factimes) < TIME_DIFF_SEC)[0]
#    if not np.any(argfac):
#        continue
#    
#    sections = makeSections(argfac, facdata, fachems)
#    
#    title = hstlist['FILEPATH'][hstindctr].strip('.fits')
#    title = title.split('\\')[-1]
#    savefilename = '%s/hs-%s' % (savepath,title)
#    plot_HST_UVIS.makeplot('HST', hstindctr, savefilename, 50, sections)
    
for uvisindctr in uvisind:
    uvisindctr = int(uvisindctr)
    argfac = np.where(np.abs(uvistimes[uvisindctr]-factimes) < TIME_DIFF_SEC)[0]
    if not np.any(argfac):
        continue
    
    sections = makeSections(argfac, facdata, fachems)
    
    title = uvislist['FILEPATH'][uvisindctr].strip('.fits')
    title = title.split('\\')[-1]
    savefilename = '%s/uv-%s' % (savepath,title)
    plot_HST_UVIS.makeplot('UVIS', uvisindctr, savefilename, sections, KR_MAX=20, scale='log')
            