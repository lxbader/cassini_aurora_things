# get paths to code and data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import glob
import matplotlib.pyplot as plt
import numpy as np
import time_conversions as tc


filelist = glob.glob('E:/JUNO_UVS/*.FIT')


#for iii in range(len(filelist)):
total = []
for iii in range(3,4):
    data = fits.open(filelist[iii])
    
    photontable = data[2]
    
    latbins = np.linspace(-90, 90, num=361)
    lonbins = np.linspace(0, 360, num=721)
    
    image, _, _ = np.histogram2d(photontable.data['JUPITER_LAT'],
                           photontable.data['JUPITER_LON'],
                           bins=[latbins, lonbins])
    print('Start', tc.et2datetime(photontable.data['EPHEMERIS_TIME'][0]))
    print('Stop', tc.et2datetime(photontable.data['EPHEMERIS_TIME'][-1]))
    
#    image_weighted = np.zeros_like(image)
#    for iii in range(len(latbins)-2):
#        for jjj in range(len(lonbins)-2):
#            tmp = np.where(
#                    (photontable.data['JUPITER_LAT']<latbins[iii+1]) &
#                    (photontable.data['JUPITER_LAT']>latbins[iii]) &
#                    (photontable.data['JUPITER_LON']<lonbins[jjj+1]) &
#                    (photontable.data['JUPITER_LON']>lonbins[jjj])
#                    )[0]
#            image_weighted[jjj,iii] += np.sum(photontable.data['WEIGHTED_COUNT'][tmp])
        
    if not len(total):
        total = image
    else:
        total += image
    
fig = plt.figure()
ax = plt.subplot(projection='polar')
ax.pcolormesh(np.radians(lonbins), np.linspace(0, 180, num=len(latbins)), np.flip(np.log10(total), axis=0))
ax.set_rlim([0,50])
ax.set_theta_zero_location('N')
ax.set_theta_direction(-1)

#fig = plt.figure()
#ax = plt.subplot(projection='polar')
#ax.pcolormesh(np.radians(lonbins), np.linspace(0, 180, num=len(latbins)),
#              np.flip(np.log10(image_weighted), axis=0))
#ax.set_rlim([0,50])
#ax.set_theta_zero_location('N')
#ax.set_theta_direction(-1)
