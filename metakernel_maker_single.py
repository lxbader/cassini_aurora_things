# Produce generic meta-kernel for Cassini
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath

import glob
import os

spicepath = '%s/SPICE' % datapath

mkpath = '%s/metakernels' % spicepath
if not os.path.exists(mkpath):
    os.makedirs(mkpath)

mk = open('%s/cassini_generic.mk' % (mkpath), 'w')
print('KPL/MK', file=mk)
print('\\begindata', file=mk)
print('KERNELS_TO_LOAD=(', file=mk)

#lsk
for kernel in glob.glob('%s/generic_kernels/lsk/*.tls' % spicepath):
    kernel = kernel.replace('\\','/')
    print('\'%s\',' % kernel, file=mk)

#fk
for kernel in glob.glob('%s/Cassini/fk/*.tf' % spicepath):
    kernel = kernel.replace('\\','/')
    print('\'%s\',' % kernel, file=mk)

#ik
for kernel in glob.glob('%s/Cassini/ik/*.ti' % spicepath):
    kernel = kernel.replace('\\','/')
    print('\'%s\',' % kernel, file=mk)

#pck
for kernel in glob.glob('%s/Cassini/pck/*.tpc' % spicepath):
    kernel = kernel.replace('\\','/')
    print('\'%s\',' % kernel, file=mk)

#sclk
for kernel in glob.glob('%s/Cassini/sclk/*.tsc' % spicepath):
    kernel = kernel.replace('\\','/')
    print('\'%s\',' % kernel, file=mk)

#ck
for kernel in glob.glob('%s/Cassini/ck/*.bc' % spicepath):
    kernel = kernel.replace('\\','/')
    tmp = kernel.split('/')[-1]
    tmp = tmp.split('.')[0]
    vers = tmp[11::]
    if vers.startswith('r'):
        print('\'%s\',' % kernel, file=mk)
        
#ck - ISS
for kernel in glob.glob('%s/Cassini/ck/*.bc' % spicepath):
    kernel = kernel.replace('\\','/')
    if 'ISS' in kernel:
        print('\'%s\',' % kernel, file=mk)

#spk
for kernel in glob.glob('%s/Cassini/spk/*.bsp' % spicepath):
    kernel = kernel.replace('\\','/')
    tmp = kernel.split('/')[-1]
    tmp = tmp.split('.')[0]
    try:
        [vers, note, tmpmin, tmpmax] = tmp.split('_')
        if note == 'SCPSE' and vers.find('R')>0:
            print('\'%s\',' % kernel, file=mk)
    except:
        continue

print(')', file=mk)
print('\\begintext', file=mk)
mk.close()
