import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cassinipy
import cubehelix
from datetime import datetime, timedelta
import glob
import matplotlib.pyplot as plt
import numpy as np
import ppo_mag_phases as ppo
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)
pmp = ppo.PPOMagPhases(datapath)

# define some colors
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5,
                         minLight=0.15, gamma=1.5)

# define data paths
listpath = '%s/full_set' % imglistpath
fitspath = '%s/fits/all' % datapath

filelist = glob.glob('%s/*_data.npz' % listpath)

hemispheres = ['N','S']
hemnames = ['northern', 'southern']

regions = ['equatorward', 'poleward']
regionnames = ['equatorward emissions', 'poleward injections']

lonsysnames = ['KSM', 'PPO North', 'PPO South']
lonsysnames_file = ['KSM', 'PPO_N', 'PPO_S']
lonsys_ticks = [['00','06','12','18'],['0','90','180','270'],['00','90','180','270']]

rrr = 0
# for both hemispheres
for hemctr in range(len(hemispheres)):
    theta = np.zeros((0,2))
    theta_PPO_N = np.zeros((0,2))
    theta_PPO_S = np.zeros((0,2))
    colat = np.zeros((0,2))
    UVISnames = np.zeros((0))
    UVIShems = np.zeros((0))
    UVIStimes = np.zeros((0))
    
    # import file by file and save necessary data
    for filectr in range(len(filelist)):
        fff = filelist[filectr]
        
        npzfile = np.load(fff)
        carclist = npzfile['arclist'][()]
        cimgtimes = [datetime.strptime(carclist['group'][iii], '%Y-%jT%H_%M_%S.fits')
                                            for iii in range(len(carclist['group']))]    
        cimgtimes = [cimgtimes[iii]+timedelta(seconds=carclist['exp'][iii]/2)
                                            for iii in range(len(carclist['exp']))]  
        cimgtimes = tc.datetime2et(cimgtimes)
        ccoords = carclist['coords']
        clabels = carclist['labels']
        chem = carclist['hemisphere']
        cimgind = carclist['imgind']
        
        for actr in range(len(clabels)):
            if clabels[actr]==regions[rrr] and chem[0]==hemispheres[hemctr]:
                theta = np.append(theta, [ccoords[actr,2:]], axis=0)
                theta_PPO_N = np.append(theta_PPO_N, [pmp.LT2ppoN(pmp.uvislon2LT(ccoords[actr,2:]),
                                                                 cimgtimes[int(cimgind[actr])])], axis=0)
                theta_PPO_S = np.append(theta_PPO_S, [pmp.LT2ppoS(pmp.uvislon2LT(ccoords[actr,2:]),
                                                                 cimgtimes[int(cimgind[actr])])], axis=0)
                colat = np.append(colat, [ccoords[actr,:2]], axis=0)
                UVISnames = np.append(UVISnames, [carclist['group'][int(cimgind[actr])]], axis=0)
                UVIShems = np.append(UVIShems, [chem[int(cimgind[actr])]], axis=0)
                UVIStimes = np.append(UVIStimes, [cimgtimes[int(cimgind[actr])]], axis=0)
        
    LTvalues = (theta/np.pi*12)%24
    PPOvalues = [theta_PPO_N[:,::-1], theta_PPO_S[:,::-1]]
    UVISnames = np.array(['%s/%s' % (fitspath, UVISnames[iii]) for iii in range(len(UVISnames))])
    pponames = ['PPO_N', 'PPO_S']
    
    # make histograms for both PPO frames
    for ppoctr in range(len(PPOvalues)):
        # getting the theta center of each longitude bin
        thetas = np.linspace(0, 2*np.pi, num=720, endpoint=True)
        thetacenter = thetas[:-1] + np.diff(thetas)/2
        ltcenter = pmp.uvislon2LT(thetacenter)
        # defining 15 deg bins in LT and PPO
        ltbins = np.linspace(0,24,num=25)
        ltstep = np.mean(np.diff(ltbins))
        ppobins = np.linspace(0,2*np.pi,num=25)
        ppostep = np.mean(np.diff(ppobins))
        # arrays collecting the sum of max intensities and number of samples
        intensity_sum = np.zeros((len(ltbins)-1, len(ppobins)-1))
        region_num = np.zeros((len(ltbins)-1, len(ppobins)-1))
        
        for rctr in range(len(LTvalues[:,0])):
            hdulist = fits.open(UVISnames[rctr])
            hdu = hdulist[0]
            cimage = hdu.data
            if UVIShems[rctr] == 'N':
                cimage = cimage[::-1]
                
            # get indices of marked region in latitude of cimage
            # extract and average in terms of lat/colat
            tmp = np.round(colat[rctr,:]/0.5).astype(int)                
            extract = np.nanmax(cimage[tmp[0]:tmp[1],:], axis=0)
            
            # get center of each longitude bin in PPO system            
            if pponames[ppoctr] == 'PPO_N':
                ppocenter = pmp.LT2ppoN(ltcenter,UVIStimes[rctr])
            elif pponames[ppoctr] == 'PPO_S':
                ppocenter = pmp.LT2ppoS(ltcenter,UVIStimes[rctr])
            
            # which LT bins are covered by the marked region
            ltlims = LTvalues[rctr,:]            
            if np.diff(ltlims) > 0:
                ltrange = np.linspace(ltlims[0], ltlims[1], num=1000)
            else:
                ltrange = np.concatenate([np.linspace(ltlims[0], 24, num=1000, endpoint=False),
                                          np.linspace(0, ltlims[1], num=1000)])
            arg_lt = np.unique(np.digitize(ltrange, ltbins)-1)
            
            # which PPO bins are covered by the marked region
            ppolims = PPOvalues[ppoctr][rctr,:]
            if np.diff(ppolims) > 0:
                pporange = np.linspace(ppolims[0], ppolims[1], num=1000)
            else:
                pporange = np.concatenate([np.linspace(ppolims[0], 2*np.pi, num=1000, endpoint=False),
                                          np.linspace(0, ppolims[1], num=1000)])
            arg_ppo = np.unique(np.digitize(pporange, ppobins)-1)
            
            # for each LT-PPO bin covered by the marked region
            # find all corresponding latitudinal intensity maxima and take maximum again,
            # add up to intensity_sum
            # count up number of samples in region_num
            for iii in arg_lt:
                tmp1 = np.where(np.digitize(ltcenter, ltbins)-1 == iii)
                for jjj in arg_ppo:
                    tmp2 = np.where(np.digitize(ppocenter, ppobins)-1 == jjj)
                    common = np.intersect1d(tmp1,tmp2)
                    if np.any(common):
                        intensity_sum[iii,jjj] += np.nanmedian(extract[np.intersect1d(tmp1,tmp2)])
                        region_num[iii,jjj] += 1
        
        # plot all the stuff
        fig = plt.figure()
        fig.set_size_inches(6,6)
        ax = plt.subplot(111)                    
        ax.pcolormesh(ltbins, ppobins*180/np.pi, region_num.T, cmap=cmap_UV) 
        ax.set_xlabel('LT (h)')
        ax.set_yticks(np.linspace(0,360,num=7))
        ax.set_ylabel('Magnetic longitude (deg)')
        ax.set_title('Locations of %s %s, %s' % (hemnames[hemctr],
                                                            regionnames[rrr],
                                                         pponames[ppoctr]))            
        plt.savefig('%s/%s_arc_hist_location_%s_%s.jpg' % (listpath,regions[rrr],
                                                      hemispheres[hemctr],
                                               pponames[ppoctr]),
                                            bbox_inches='tight', dpi=500)
        plt.show()
        plt.close()
        
        fig = plt.figure()
        fig.set_size_inches(6,6)
        ax = plt.subplot(111)
        data = intensity_sum.T/region_num.T
        data[np.where(np.isnan(data))] = 0            
        ax.pcolormesh(ltbins, ppobins*180/np.pi, data, cmap=cmap_UV) 
        ax.set_xlabel('LT (h)')
        ax.set_yticks(np.linspace(0,360,num=7))
        ax.set_ylabel('Magnetic longitude (deg)')
        ax.set_title('Mean max intensity of %s %s, %s' % (hemnames[hemctr],
                                                            regionnames[rrr],
                                                         pponames[ppoctr]))            
        plt.savefig('%s/%s_arc_hist_location_max_%s_%s.jpg' % (listpath,regions[rrr],
                                                      hemispheres[hemctr],
                                               pponames[ppoctr]),
                                            bbox_inches='tight', dpi=500)
        plt.show()
        plt.close()
                
            