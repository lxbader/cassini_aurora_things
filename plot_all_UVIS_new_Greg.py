# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
boxpath = pr.boxpath
gitpath = pr.gitpath
plotpath = pr.plotpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import glob
import numpy as np
import os
import plot_HST_UVIS
import scipy.io as sio
import sys
import time
import time_conversions as tc
import uvisdb

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF
cp = plot_HST_UVIS.CassPlotter()

sys.exit()

# get FAC times
facpath = '%s/Greg FAC times' % boxpath
facfiles = glob.glob('%s/*.sav' % facpath)

facdata = np.zeros((0,5))
fachems = np.zeros((0))

for fctr in range(len(facfiles)):
    fff = facfiles[fctr]    
    savdata = sio.readsav(fff)
    for key in [*savdata]:
        tmpdata = savdata[[*savdata][0]]
        facdata = np.append(facdata,tmpdata,axis=0)
        if 'sh' in key:
            fachems = np.append(fachems, np.full((np.shape(tmpdata)[0]),'South'))
        else:
            fachems = np.append(fachems, np.full((np.shape(tmpdata)[0]),'North'))
            
savepath = os.path.join(plotpath, 'UVIS_ALL', time.strftime('%Y%m%d%H%M%S'))

for iii in imglist.index:    
    print(iii, '/', len(imglist))
    print(imglist.loc[iii,'FILEPATH'])
    year = imglist.loc[iii,'YEAR']
    doy = imglist.loc[iii,'DOY']
    if not (year==2014 and ((doy==145))):
        continue
    name = imglist.loc[iii,'FILEPATH'].strip('.fits').split('/')[-1].split('\\')[-1]
    tmpdir = os.path.join(savepath, str(year), '{}xx'.format(int(doy/100)))
    if not os.path.exists(tmpdir):
        os.makedirs(tmpdir)
        
    cp.makeplot(imglist.loc[iii,:], os.path.join(tmpdir, name), KR_MIN=1, KR_MAX=20, noorbit=True)
#    sys.exit()
