import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.optimize as sopt
import skr_power
import time
import time_conversions as tc
import uvisdb

udb = uvisdb.UVISDB()
imgdb = udb.currentDF

skrpw = skr_power.SKRpower(datapath)
skrpw.loadIntegratedSKR()

#sys.exit()

def linFit(x, m, k):
    return m*x+k

savepath = '%s/Plots/SKR_UV_comp/%s' % (boxpath,time.strftime('%Y%m%d%H%M%S'))
if not os.path.exists(savepath):
    os.makedirs(savepath)

#imglist = np.load('%s/UVIS/UVISlist_RES2.npz' % datapath)['data'][()]

#sys.exit()
#imgtimes = np.mean(imglist['ET_START_STOP'], axis=1)

imgtimes = np.mean(imgdb.loc[:,['ET_START','ET_STOP']], axis=1)

hems = ['North','South']

    
#====================
# define time windows
subintervnames = ['all','pre-equinox','post-equinox','2008','2013','2014','2016','2017']
subintervind = np.zeros((len(imgtimes),len(subintervnames)))
for iii in range(len(subintervnames)):
    if subintervnames[iii]=='all':
        subintervind[:,iii] = 1
    elif subintervnames[iii]=='pre-equinox':
        valid = np.where(imgtimes<tc.datetime2et(datetime(2010,1,1)))[0]
        subintervind[valid,iii] = 1
    elif subintervnames[iii]=='post-equinox':
        valid = np.where(imgtimes>tc.datetime2et(datetime(2010,1,1)))[0]
        subintervind[valid,iii] = 1
    elif subintervnames[iii][:2]=='20':
        year = int(subintervnames[iii])
        valid = np.where((imgtimes>tc.datetime2et(datetime(year,1,1)))*
                         (imgtimes<tc.datetime2et(datetime(year+1,1,1)))
                         )[0]
        subintervind[valid,iii] = 1
            
# plot
dists = [20,30,70]
for MAX_RS in dists:
    for method in ['average']:
#        plot_subinterv = ['pre-equinox','post-equinox']
#        colors = ['r','b']
        
        plot_subinterv = ['2008','2013','2014','2016','2017']
        colors = ['r','b','g','y','k']
        
        fig, axes = plt.subplots(1, 2, sharex='all', sharey='all')
        fig.set_size_inches(12,6)
        for iii in range(2):
            ax = axes[iii]    
            ax.set_title(hems[iii])
            ax.set_xlabel('SKR power (W/m^2)')
            
            thishem = hems[iii]
            
            for sss in range(len(plot_subinterv)):
                thissic = np.asscalar(np.where([subintervnames[iii]==plot_subinterv[sss]
                                        for iii in range(len(subintervnames))])[0])
                # find right images            
                arg = np.where((imgdb.loc[:,'HEMISPHERE'] == thishem)*
                               (imgdb.loc[:,'POS_KRTP_R'] < MAX_RS)*
                               (subintervind[:,thissic])
                               )[0]
                
                # get valid data
                valtimes = imgdb.loc[arg,['ET_START', 'ET_STOP']].mean(axis=1)
                if method == 'nearest':
                    xdata = np.log10(skrpw.getNearestSKR(valtimes, 'RH' if thishem=='North' else 'LH'))
                else:
                    xdata = np.log10(skrpw.getAveragedSKR(valtimes, 'RH' if thishem=='North' else 'LH'))
                ydata = np.array(np.log10(imgdb.loc[arg,'UV_POWER']))
                valid = np.where((np.isfinite(xdata))*(np.isfinite(ydata)))[0]
                xdata = xdata[valid]
                ydata = ydata[valid]
                
                if method == 'nearest':
                    xplotdata = np.log10(skrpw.getNearestSKR(valtimes, 'RH' if thishem=='North' else 'LH'))
                else:
                    xplotdata = np.log10(skrpw.getAveragedSKR(valtimes, 'RH' if thishem=='North' else 'LH'))
                ax.scatter(skrpw.getNearestSKR(valtimes, 'RH' if thishem=='North' else 'LH'),
                           imgdb.loc[arg,'UV_POWER'], marker='+',
                           color=colors[sss], edgecolor='none', s=100, linewidth=0.5,
                           label=plot_subinterv[sss])
    #            ax.scatter(imgdb.loc[arg,'SKR_POWER_N_RH_INST' if thishem=='North' else 'SKR_POWER_S_LH_INST'],
    #                       imgdb.loc[arg,'UV_POWER'], marker='+',
    #                       color=colors[sss], edgecolor='none', s=120, linewidth=0.5,
    #                       label=plot_subinterv[sss])
    
                if np.size(valid) < 10:
                    continue                
                opt, cov = sopt.curve_fit(linFit, xdata, ydata)
                xs = np.logspace(-20,-13)    
                ys = 10**opt[1]*xs**opt[0]
                ax.plot(xs,ys, c=colors[sss])
            ax.legend()
            ax.text(0.5, 0.05, r'max {} R$_\mathrm{{S}}$'.format(MAX_RS),
                transform=ax.transAxes, ha='center', va='center',
                fontweight='bold', fontsize=14)
        axes[0].set_xscale('log')
        axes[0].set_yscale('log')
        axes[0].set_xlim([1e-20, 5e-14])
        axes[0].set_ylim([1e9, 3e10])
        axes[0].set_xlabel('SKR power (W/m^2)')
        axes[0].set_ylabel('UV power (W/sr)')
        
        
        plt.subplots_adjust(wspace=0.1)
        plt.savefig('{}/skr_comp_{}_{}.png'.format(savepath,method,MAX_RS), bbox_inches='tight', dpi=250)
        plt.savefig('{}/skr_comp_{}_{}.pdf'.format(savepath,method,MAX_RS), bbox_inches='tight', dpi=250)
        plt.show()
        plt.close()

