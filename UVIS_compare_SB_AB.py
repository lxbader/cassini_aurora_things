# find UVIS differences between Sarah's and Alex' files

import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from datetime import datetime
import glob
import numpy as np
import time_conversions as tc

listpath = '%s/UVIS/UVISlist_RES2.npz' % datapath
ABlist = np.load(listpath)['data'][()]

SBpath = '{}/UVIS/All/*.fits'.format(datapath)
SBfiles = glob.glob(SBpath)
SBfiles = np.array([iii.split('\\')[-1] for iii in SBfiles])
SBfiles = np.array([iii.split('/')[-1] for iii in SBfiles])
SBfiles = np.array([iii.strip('.fits') for iii in SBfiles])
SBdt = np.array([datetime.strptime(iii, '%Y-%jT%H_%M_%S') for iii in SBfiles])
SBet = tc.datetime2et(SBdt)

ABexposures = np.copy(ABlist['ET_START_STOP'])
ABexposures[:,0] -= 10
ABexposures[:,1] += 10

f = open('E:/SB_missed.txt', 'w')
for abctr in range(len(ABexposures[:,0])):
    window = ABexposures[abctr,:]
    cond1 = SBet<window[1]
    cond2 = SBet>=window[0]
    tmp = np.logical_and(cond1,cond2)
    if np.any(tmp):
        continue
    abfile = ABlist['FILEPATH'][abctr].split('\\')[-1]
    abfile = abfile.split('/')[-1]
    
    argmin = np.argmin(np.abs(SBet-np.mean(window)))
    sbclosest = SBfiles[argmin]
    
    f.write('AB file: {} ====== nearest SB file: {} ====== distance (s): {}\n'.format(
            abfile,sbclosest,int(np.abs(window[0]-SBet[argmin]))))
f.close()