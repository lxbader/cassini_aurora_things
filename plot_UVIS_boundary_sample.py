# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import boundaries
from datetime import datetime
import numpy as np
import os
import plot_HST_UVIS
import time
import time_conversions as tc
import uvisdb

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF
ib = boundaries.ImageBounds()
cp = plot_HST_UVIS.CassPlotter()


files = [
#        r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0023\2008_129T09_23_54.fits',
        r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0024\2008_239T03_37_51.fits',
#        r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0048\2014_230T12_10_46.fits',
#        r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0056\2016_229T18_09_37.fits',
#        r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0057\2016_275T12_49_21.fits',
#        r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0041\2012_343T07_42_14.fits',
        ]

savepath = os.path.join(plotpath, 'UVIS_ALL', time.strftime('%Y%m%d%H%M%S'))
#imglist['OCB_VALID'] = True
#imglist.at[imglist['POS_KRTP_R']>30, 'OCB_VALID'] = False
#imglist.at[imglist['OCBMAXGAP']>30, 'OCB_VALID'] = False
#imglist.at[imglist['OPENCV_ELEVS'].apply(np.nanmin)<20, 'OCB_VALID'] = False

for iii in imglist.index:
#    if not imglist.loc[iii, 'OCB_VALID']:
#        continue
#    fmt = '%Y-%j %H:%M'
#    start = datetime.strptime('2017-042 10:30', fmt)
#    stop = datetime.strptime('2017-042 11:37', fmt)
#    cond1 = imglist.loc[iii,'ET_START'] > tc.datetime2et(start)
#    cond2 = imglist.loc[iii,'ET_START'] < tc.datetime2et(stop)
#    if not (cond1 & cond2):
#        continue
    
    valid = False
    for fff in range(len(files)):
        if not imglist.loc[iii,'FILEPATH'].endswith(files[fff].split('\\')[-1]):
            continue
        valid = True
        break
    if not valid:
        continue
    
    ib.calculate(files[fff], plot=True, pdf=True)
    
    print(iii, '/', len(imglist))
    print(imglist.loc[iii,'FILEPATH'])
    year = imglist.loc[iii,'YEAR']
    doy = imglist.loc[iii,'DOY']
    name = imglist.loc[iii,'FILEPATH'].strip('.fits').split('/')[-1].split('\\')[-1]
    tmpdir = os.path.join(savepath, str(year), '{}xx'.format(int(doy/100)))
#    tmpdir = os.path.join(savepath, str(year))
    if not os.path.exists(tmpdir):
        os.makedirs(tmpdir)
        
    cp.makeplot(imglist.loc[iii,:], os.path.join(tmpdir, name))

    lonbins = np.linspace(0,2*np.pi,num=np.shape(imglist.loc[iii,'OPENCV_POLEW'])[-1]+1)
    loncenters = lonbins[:-1] + np.diff(lonbins)/2
    latval = imglist.loc[iii,'OCBPOINTS']
    ocbpoints = np.array([loncenters, latval]).T
    ocbpoints=[]
    
    lonbins = np.linspace(0,2*np.pi,num=np.shape(imglist.loc[iii,'OPENCV_CENTERS'])[-1]+1)
    loncenters = lonbins[:-1] + np.diff(lonbins)/2
    points = {}
    points['centers_cv_new'] = np.array([loncenters, imglist.loc[iii,'OPENCV_CENTERS']]).T
    points['centers_cv_new'] = np.array([loncenters, ib.opencv_centers]).T
#    points['polew'] = np.array([loncenters, imglist.loc[iii,'OPENCV_POLEW']]).T
#    points['eqw'] = np.array([loncenters, imglist.loc[iii,'OPENCV_EQW']]).T
#    pointcolors = ['gold', 'red', 'red']
    pointcolors = ['w']
    
    points_available = False
    for pctr in range(len([*points])):
        if np.any(np.isfinite(points[[*points][pctr]][:,1])):
            points_available = True
            break
    if not points_available:
        continue
    
    ellipse = {}
    lon, lat, x, y, r, res = ib.circle
    if np.isfinite(x):
        ellipse['CV_NEW'] = [x,
                        y,
                        r,
                        r,
                        0, 'gold']

#    if np.isfinite(imglist.loc[iii,'CIRCLE_X']):
#        ellipse['CV_NEW'] = [imglist.loc[iii,'CIRCLE_X'],
#                        imglist.loc[iii,'CIRCLE_Y'],
#                        imglist.loc[iii,'CIRCLE_R'],
#                        imglist.loc[iii,'CIRCLE_R'],
#                        0, 'gold']
#                   
#    else:
#        ellipse['CV_NEW'] = []
#    if np.isfinite(imglist.loc[iii,'ELLIPSE_X']):
#        ellipse['Ellipse'] = [imglist.loc[iii,'ELLIPSE_X'],
#                        imglist.loc[iii,'ELLIPSE_Y'],
#                        imglist.loc[iii,'ELLIPSE_WIDTH'],
#                        imglist.loc[iii,'ELLIPSE_HEIGHT'],
#                        imglist.loc[iii,'ELLIPSE_PHI'],
#                        'darkorange']
#                   
#    else:
#        ellipse['Ellipse'] = []
  
    cp.makeplot(imglist.loc[iii,:], os.path.join(tmpdir, name),
                           points=points, pointcolors=pointcolors,
                           ellipse=ellipse, ocbpoints=ocbpoints, noorbit=True)
    cp.makeplot(imglist.loc[iii,:], os.path.join(tmpdir, name))
    sys.exit()
    
