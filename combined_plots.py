# Large plots with all necessary information
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
imglistpath = pr.imglistpath

plotpath = 'E:/Box Sync/Plots/20171201'

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cassinipy
import cubehelix
from cycler import cycler
from datetime import datetime, timedelta
import time_conversions as tc
import glob
import matplotlib.colors as colors
import matplotlib.gridspec as gridspec
import numpy.ma as ma
import matplotlib.patches as ptch
import matplotlib.pyplot as plt
import numpy as np
import os
import ppo_mag_phases
import sys

listtype = 'low_lat'
listpath = '%s/%s' % (imglistpath,listtype)
imgsavedir = '%s/%s' % (plotpath,listtype)
if not os.path.exists(imgsavedir):
    os.makedirs(imgsavedir)

# Set up Cassinipy
cd = cassinipy.CassiniData(datapath)
cd.set_refframe('KRTP')
magdata = cd.cmag
rpwsdata = cd.crpws
incadata = cd.cinca
lemmsdata = cd.clemms
elsdata = cd.cels

# General settings
#cmap_UV = cubehelix.cmap(reverse=False, start=0.3, rot=-0.5)
#cmap_UV = cubehelix.cmap(reverse=False, start=0.3, rot=-0.8)
#cmap_UV = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
#cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
cmap_SPEC_r = cubehelix.cmap(reverse=True, start=1.8, rot=1, gamma=1.5)
fpcolordark = 'k'
fpcolorbright = 'white'
mydpi = 500
timeframe = 6 #h

# Constants
SATURN_R = 60268 # km

# Magnetopause model
# Input: Dp (dynamic pressure in nPa)
def mploc(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    theta = np.arange(0,170,0.1)/180*np.pi
    radius = r0*(2/(1+np.cos(theta)))**K
    [x,r] = radius*[np.cos(theta), np.sin(theta)]
    return [x,r]

# Import list of interesting plots
filelist = glob.glob('%s/*.txt' % listpath)

# Make list with failed plots
f_fail = '%s/failed.txt' % imgsavedir
f_tmp = open(f_fail, 'w')
f_tmp.close()

for fff in filelist:
#for ijk in range(0,5):
#    fff = filelist[ijk]
    
    UVISimages = np.array([])
    f = open(fff, 'r')
    while True:
        tmp = f.readline()
        if tmp:
            UVISimages = np.append(UVISimages, tmp.split('\n')[0])
        else:
            f.close()
            break

    tmp = fff.split('\\')[-1]
    tmp = tmp.split('.')[0]
    fullimgsavedir = '%s/%s' % (imgsavedir,tmp)
    if not os.path.exists(fullimgsavedir):
        os.makedirs(fullimgsavedir)
    
    for ccc in range(len(UVISimages)):
            
        try:
            # Import fits file
            thisfile = UVISimages[ccc]
            hdulist = fits.open(thisfile)
            hdu = hdulist[0]
            image = hdu.data
            filename = thisfile.split('\\')[-1]
            filename = filename.split('.')[0]
            # Get timestamp and exposure time for plot title
            timestamp = filename.replace('_',':')
            exp = hdu.header['TOTALEXP']
            pyimgtime = datetime.strptime(timestamp, '%Y-%jT%H:%M:%S')
            imgtime = tc.datetime2et(pyimgtime)
    
            tlimet=[imgtime-timeframe*3600, imgtime+timeframe*3600]
            
            # Define timespan over which to plot the other stuff
            thisday = datetime(pyimgtime.year, pyimgtime.month, pyimgtime.day)
            pytimemarkers = [thisday+iii*timedelta(hours=12) for iii in range(-6, 7)]
            ettimemarkers = tc.datetime2et(pytimemarkers)
            etdaymarkers = [ettimemarkers[2*iii] for iii in range(int(np.ceil(len(pytimemarkers)/2)))]
            tmp = tc.et2datetime(etdaymarkers)
            daylabels = [datetime.strftime(tmp[iii], ' DOY%j') for iii in range(len(etdaymarkers))]
            tlimet_ext = [ettimemarkers[0], ettimemarkers[-1]]
            
            # Get data
            cd.set_timeframe(tlimet)
#            cd.clemms.setETlim(tlimet)
#            cd.clemms.update()
#            continue
#            sys.exit()
            cd.update()
    
            print('File %s: %s' % (ccc+1, filename))
    
            # Define subplot arrangement
            fig = plt.figure()
            fig.set_size_inches(24,15)
            fig.suptitle('%s' % datetime.strftime(pyimgtime, '%Y-%j, %H:%M'), fontsize=20)
    
            gs1 = gridspec.GridSpec(2, 1)
            gs1.update(left=0, right=0.22)
            ax1 = plt.subplot(gs1[0, 0])
            ax2 = plt.subplot(gs1[1, 0])
    
            gs2 = gridspec.GridSpec(8, 1, height_ratios=[3,3,3,3,1,3,3,3])
            gs2.update(left=0.27, right=0.69, hspace=0)
            bx1 = plt.subplot(gs2[0, 0])
            bx2 = plt.subplot(gs2[1, 0], sharex=bx1)
            bx3 = plt.subplot(gs2[2, 0], sharex=bx1)
            bx4 = plt.subplot(gs2[3, 0], sharex=bx1)
            bx5 = plt.subplot(gs2[4, 0], sharex=bx1)
            bx6 = plt.subplot(gs2[5, 0], sharex=bx1)
            bx7 = plt.subplot(gs2[6, 0], sharex=bx1)
            bx8 = plt.subplot(gs2[7, 0], sharex=bx1)
            gs3 = gridspec.GridSpec(8, 1, height_ratios=[3,3,3,3,1,3,3,3])
            gs3.update(left=0.70, right=0.71, hspace=0.2)
            cx2 = plt.subplot(gs3[1, 0])
            cx3 = plt.subplot(gs3[2, 0])
            cx4 = plt.subplot(gs3[3, 0])
            cx5 = plt.subplot(gs3[4, 0])
            cx6 = plt.subplot(gs3[5, 0])
            cx8 = plt.subplot(gs3[7, 0])
            gs4 = gridspec.GridSpec(2, 1)
            gs4.update(left=0.77, right=1.0)
            dx1 = plt.subplot(gs4[0, 0], projection='polar')
            dx2 = plt.subplot(gs4[1, 0], projection='polar')
    
            # Plot side view
            tmp = cd.get_locations([imgtime], 'KSM')
            ax1.scatter(tmp[1], tmp[2], s=40, marker='D', color='r', zorder=5)
            # Trajectory
            [tmin, tmax] = tlimet_ext
            step = 1000
            times = [xxx*(tmax-tmin)/step + tmin for xxx in range(step)]
            tmp = cd.get_locations(times, 'KSM')
            ax1.plot(tmp[2], tmp[3], color='k')
            # Plot dots and day markers
            tmp = cd.get_locations(ettimemarkers, 'KSM')
            ax1.scatter(tmp[2], tmp[3], color='k')
            tmp = cd.get_locations(etdaymarkers, 'KSM')
            for jjj in range(len(etdaymarkers)):
                ax1.text(tmp[1][jjj], tmp[3][jjj], daylabels[jjj], color=fpcolordark)
            ax1.set_xlim([-40,40])
            ax1.set_ylim([-40,40])
            ax1.add_patch(ptch.Wedge((0,0), 1, 0, 360, fill=True, color='0.7'))
            ax1.set_aspect('equal')
            ax1.set_xlabel('$X_{KSM}$')
            ax1.set_ylabel('$Y_{KSM}$')
            ax1.grid()
    
            # Plot top view of Cassini location
            tmp = cd.get_locations([imgtime], 'KSM')
            ax2.scatter(tmp[1], tmp[3], s=40, marker='D', color='r', zorder=5)
            # Trajectory
            [tmin, tmax] = tlimet_ext
            step = 1000
            times = [xxx*(tmax-tmin)/step + tmin for xxx in range(step)]
            tmp = cd.get_locations(times, 'KSM')
            ax2.plot(tmp[1], np.sqrt(tmp[2]**2+tmp[3]**2), color='k')
            # Plot dots and day markers
            tmp = cd.get_locations(ettimemarkers, 'KSM')
            ax2.scatter(tmp[1], np.sqrt(tmp[2]**2+tmp[3]**2), color='k')
            tmp = cd.get_locations(etdaymarkers, 'KSM')
            for jjj in range(len(etdaymarkers)):
                ax2.text(tmp[1][jjj], np.sqrt(tmp[2][jjj]**2+tmp[3][jjj]**2), daylabels[jjj], color=fpcolordark)
    
            ax2.set_xlim([-60,40])
            ax2.set_ylim([0,80])
            [xmp, rmp] = mploc(0.01)
            ax2.plot(xmp,rmp,'k-')
            [xmp, rmp] = mploc(0.1)
            ax2.plot(xmp,rmp,'k-')
            ax2.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7'))
            ax2.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0'))
            ax2.set_aspect('equal')
            ax2.set_xlabel('$X_{KSM}$')
            ax2.set_ylabel('$Z_{KSM}$')
            ax2.grid()
    
            # Plot B field in KRTP
            if np.any(magdata.ettimes):
                bx1.plot(magdata.ettimes, magdata.data[:,0], c='blue', lw=1, label=r'$B_r$')
                bx1.plot(magdata.ettimes, magdata.data[:,1], c='green', lw=1, label=r'$B_\theta$')
                bx1.plot(magdata.ettimes, magdata.data[:,2], c='red', lw=1, label=r'$B_\phi$')
                bx1.legend(loc=2)
            bx1.set_ylabel(r'$B_{RTP}$ (nT)')
    
            # Plot parallel ELS spectrum
            if np.any(elsdata.dist_ppa):
                data = np.transpose(elsdata.dist_ppa[:,0,:])
                data = ma.masked_invalid(data)
                ppp = bx2.pcolormesh(elsdata.ettimes, elsdata.energies, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=1e8, vmax=1e11))
                ppp.cmap.set_bad('gray')
                cbar = plt.colorbar(ppp, cax=cx2)
            bx2.set_ylim([0.8,2.5e4])
            bx2.set_yscale('log')
            bx2.set_ylabel('E (eV), para')
            
            # Plot perpendicular ELS spectrum
            if np.any(elsdata.dist_ppa):
                data = np.transpose(elsdata.dist_ppa[:,1,:])
                data = ma.masked_invalid(data)
                ppp = bx3.pcolormesh(elsdata.ettimes, elsdata.energies, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=1e8, vmax=1e11))
                ppp.cmap.set_bad('gray')
                cbar = plt.colorbar(ppp, cax=cx3)
                cbar.set_label(r'e$^-$ energy flux (eV/m$^2$/s/sr/eV)', rotation=270, labelpad=10)            
            bx3.set_ylim([0.8,2.5e4])
            bx3.set_yscale('log')
            bx3.set_ylabel('E (eV), perp')
            
            # Plot antiparallel ELS spectrum
            if np.any(elsdata.dist_ppa):
                data = np.transpose(elsdata.dist_ppa[:,2,:])
                data = ma.masked_invalid(data)
                ppp = bx4.pcolormesh(elsdata.ettimes, elsdata.energies, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=1e8, vmax=1e11))
                ppp.cmap.set_bad('gray')
                cbar = plt.colorbar(ppp, cax=cx4)
            bx4.set_ylim([0.8,2.5e4])
            bx4.set_yscale('log')
            bx4.set_ylabel('E (eV), antip')
    
            # Plot ELS pitch angle distribution
            if np.any(elsdata.pa_dist):
                data = np.transpose(elsdata.pa_dist)
                data = ma.masked_invalid(data)
                ppp = bx5.pcolormesh(elsdata.ettimes, elsdata.pa_bins, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=3e12, vmax=5e13))
                ppp.cmap.set_bad('gray')
                cbar = plt.colorbar(ppp, cax=cx5)
                cbar.set_label(r'e^- energy flux (eV/m$^2$/s/sr)', rotation=270, labelpad=10)
            bx5.set_ylim([0,180])
            bx5.set_yticks([0,90,180])
            bx5.set_ylabel('PA (deg)')
    
            # Plot RPWS spectrum
            if np.any(rpwsdata.data):
                data = np.transpose(rpwsdata.data)
                data = ma.masked_invalid(data)
                ppp = bx6.pcolormesh(rpwsdata.ettimes, rpwsdata.freq, data, cmap=cmap_SPEC, vmin=0, vmax=35)
                ppp.cmap.set_bad('gray')
                cbar = plt.colorbar(ppp, cax=cx6)
                cbar.set_label(r'dB over background', rotation=270, labelpad=10)
            bx6.set_ylim([1.1,1.5e7])
            bx6.set_yscale('log')
            bx6.set_ylabel('f (Hz)')        
    
            # Plot INCA ion fluxes
            if np.any(incadata.data):
                mycycler = cycler('color', ['red', 'coral', 'orange', 'gold', 'yellowgreen', 'turquoise', 'steelblue', 'blueviolet'])
                bx7.set_prop_cycle(mycycler)
                for iii in range(len(incadata.labels)):
                    tmp = bx7.plot(incadata.ettimes, incadata.data[:,iii], lw=1.5)
                    bx7.text(1.02, 0.1+iii*0.1, incadata.labels[iii], color=tmp[0].get_color(), transform=bx7.transAxes)
            bx7.set_yscale('log')
            bx7.set_ylabel(r'Ion flux (1/cm$^2$/s/sr/keV)')
    
            # Hide x-axis labels
            plt.setp(bx1.get_xticklabels(), visible=False)
            plt.setp(bx2.get_xticklabels(), visible=False)
            plt.setp(bx3.get_xticklabels(), visible=False)
            plt.setp(bx4.get_xticklabels(), visible=False)
            plt.setp(bx5.get_xticklabels(), visible=False)
            plt.setp(bx6.get_xticklabels(), visible=False)
            plt.setp(bx7.get_xticklabels(), visible=False)
    
            # Plot LEMMS spectrum
            if np.any(lemmsdata.data):
                data = np.transpose(lemmsdata.data)
                data = ma.masked_invalid(data)
                ppp = bx8.pcolormesh(lemmsdata.ettimes, lemmsdata.energies, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=1e-1, vmax=1e3))
                ppp.cmap.set_bad('gray')
                bx8.set_ylim([20,1750])
                cbar = plt.colorbar(ppp, cax=cx8)
                cbar.set_label(r'e$^-$ flux (1/cm$^2$/s/sr/keV)', rotation=270, labelpad=10)
            bx8.set_yscale('log')
            bx8.set_ylabel('E (keV)')        
    
            # Put proper x-axis labels
            pytlim = tc.et2datetime(tlimet)
            if pytlim[0].hour < 22:
                ticklist = np.array([datetime(pytlim[0].year, pytlim[0].month, pytlim[0].day, pytlim[0].hour+1)])
            else:
                tmp = pytlim[0]+timedelta(hours=2)
                ticklist = np.array([datetime(tmp.year, tmp.month, tmp.day, 0)])
            if ticklist[0].hour % 2:
                ticklist[0] += timedelta(hours=1)
            while pytlim[1]-ticklist[-1]>timedelta(0):
                ticklist = np.append(ticklist, ticklist[-1]+timedelta(hours=timeframe/3))
            ticklistet = tc.datetime2et(ticklist)
    
            thisLOC = cd.get_locations(ticklistet, 'KRTP')
            ticlab = ['%s\n%.1f\n%.1f\n%.1f' % (datetime.strftime(ticklist[iii],'%j/%H'),
                                                thisLOC[1][iii],
                                                thisLOC[2][iii],
                                                thisLOC[3][iii]) for iii in range(len(ticklistet))]
            bx8.set_xticks(ticklistet)
            bx8.set_xticklabels(ticlab)
    
            # Label description at the right
            ticklab = bx8.xaxis.get_ticklabels()[0]
            trans = ticklab.get_transform()
            bx8.xaxis.set_label_coords(tlimet[0]-timeframe/8*3600, 0, transform=trans)
            bx8.set_xlabel('DOY/HH\n r (RS) \n el (deg)\n LT (h)')
            bx8.set_xlim(tlimet)
    
            # Vertical lines indicating start and stop of UVIS image
            bx8.axvline(x=imgtime, ymin=0, ymax=7.33, c='k',
                                   linestyle='--', lw=2, zorder=2, clip_on=False)
    
            # Plot north and south view
            theta = np.array(range(720))*.5
            theta = np.radians(theta)
            r = np.array(range(360))*.5
            levels = np.linspace(0,10,41)
    
            view = ['North', 'South']
            viewdata = [image[::-1], image]
            axes = [dx1, dx2]
            for dirctr in range(2):
                ax = axes[dirctr]
                data = viewdata[dirctr]
                data[np.where(data==0)] = np.nan
    #            data[np.where(np.isnan(data))] = 0
    
                p = ax.contourf(theta, r, data, levels, cmap=cmap_UV, extend='both')
                p.cmap.set_under('k')
                p.cmap.set_over('orangered')
                p.set_clim(0, 10)
                ax.set_axis_bgcolor('gray')
    
                # Plot lines of ion footprint
                [tmin, tmax] = tlimet_ext
                step = 1000
                times = [xxx*(tmax-tmin)/step + tmin for xxx in range(step)]
                tmp = cd.get_ionfootp(times)
                ax.plot(tmp[3]*np.pi/12, tmp[dirctr+1], color=fpcolorbright, linestyle='-', lw=2.5)
    
                # Plot dots and day markers
                tmp = cd.get_ionfootp(ettimemarkers)
                ax.scatter(tmp[3]*np.pi/12, tmp[dirctr+1], s=20, color=fpcolorbright)
                tmp = cd.get_ionfootp(etdaymarkers)
                for jjj in range(len(etdaymarkers)):
                    ax.text(tmp[3][jjj]*np.pi/12, tmp[1][jjj], daylabels[jjj], color=fpcolorbright)
    
                # Cassini location
                tmp = cd.get_ionfootp(tc.datetime2et([pyimgtime]))
                ax.scatter(tmp[3]*np.pi/12, tmp[dirctr+1], s=40, color='r', marker='D', zorder=5)
                ax.set_theta_zero_location("N")
                ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
                ax.set_xticklabels(['00','06','12','18'])
                ax.set_yticks([10,20,30])
                ax.set_yticklabels([])
                ax.grid('on', color='w', linewidth=2)
                ax.set_title('%s, %s s' % (view[dirctr], exp), y=1.1)
                ax.set_rmax(30)
                ax.set_rmin(0)
                cbar = plt.colorbar(p, pad=0.1, ax=ax)
                cbar.set_label(r'Intensity (kR)', rotation=270)
                cbar.set_ticks([0,1,2,3,4,5,6,7,8,9,10])
    
            plt.savefig('%s/%s.jpg' % (fullimgsavedir, datetime.strftime(pyimgtime, '%Y-%j_%H%M')), bbox_inches='tight', dpi=mydpi)
            plt.show()
            plt.close()
        except:
            f_tmp = open(f_fail, 'a')
            f_tmp.write('ccc: %s. %s\n' % (ccc, fff))
            f_tmp.close()
            continue
