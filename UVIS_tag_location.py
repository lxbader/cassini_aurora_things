import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime, timedelta
import glob
import matplotlib.pyplot as plt
import numpy as np
import ppo_mag_phases as ppo
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)
pmp = ppo.PPOMagPhases(datapath)

# define some colors
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5,
                         minLight=0.15, gamma=1.5)

# define data paths
listpath = '%s/full_set' % imglistpath

filelist = glob.glob('%s/*_data.npz' % listpath)

hemispheres = ['N','S']
hemnames = ['northern', 'southern']

regions = ['equatorward', 'poleward']
regionnames = ['equatorward emissions', 'poleward injections']

lonsysnames = ['KSM', 'PPO North', 'PPO South']
lonsysnames_file = ['KSM', 'PPO_N', 'PPO_S']
lonsys_ticks = [['00','06','12','18'],['0','90','180','270'],['00','90','180','270']]

#points 
for rrr in range(len(regions)):    
    for hemctr in range(len(hemispheres)):
        theta = np.array([])
        theta_PPO_N = np.array([])
        theta_PPO_S = np.array([])
        colat = np.array([])
        
        for filectr in range(len(filelist)):
            fff = filelist[filectr]
            
            npzfile = np.load(fff)
            cpointlist = npzfile['pointlist'][()]
            cimgtimes = [datetime.strptime(cpointlist['group'][iii], '%Y-%jT%H_%M_%S.fits')
                                                for iii in range(len(cpointlist['group']))]    
            cimgtimes = [cimgtimes[iii]+timedelta(seconds=cpointlist['exp'][iii]/2)
                                                for iii in range(len(cpointlist['exp']))]  
            cimgtimes = tc.datetime2et(cimgtimes)
            ccoords = cpointlist['coords']
            clabels = cpointlist['labels']
            ctrack = cpointlist['trackable']
            chem = cpointlist['hemisphere']
            
            for pctr in range(len(clabels)):
                if clabels[pctr]==regions[rrr] and chem[0]==hemispheres[hemctr]:
                    argvalid = np.where(np.isfinite(ccoords[pctr,:,0]))[0]                        
                    tmptimes = cimgtimes[argvalid]
    
                    theta = np.append(theta, ccoords[pctr,argvalid,0])
                    theta_PPO_N = np.append(theta_PPO_N, np.pi+ccoords[pctr,argvalid,0]
                                                        -pmp.getMagPhase(tmptimes,'N'))
                    theta_PPO_S = np.append(theta_PPO_S, np.pi+ccoords[pctr,argvalid,0]
                                                        -pmp.getMagPhase(tmptimes,'S'))
                    colat = np.append(colat, ccoords[pctr,argvalid,1])                                    
    
        lonsys=[theta, theta_PPO_N, theta_PPO_S]
    
        for lonctr in range(len(lonsys)):               
            fig = plt.figure()
            fig.set_size_inches(6,6)
            ax = plt.subplot(111, projection='polar')
            
            theta = np.array(range(720))*.5
            theta = np.radians(theta)
            r = np.array(range(360))*.5            
            ax.scatter(lonsys[lonctr], colat, color='k', marker='+', s=25)            
            ax.set_theta_zero_location("N")
            ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
            ax.set_xticklabels(lonsys_ticks[lonctr])
            ax.set_yticks([10,20,30])
            ax.set_yticklabels([])
            ax.grid('on', color='k', linewidth=1)
            ax.set_rmax(30)
            ax.set_rmin(0)
            ax.set_title('Locations of all tagged %s %s, %s' % (hemnames[hemctr],
                                                                regionnames[rrr],
                                                             lonsysnames[lonctr]))
                
            plt.savefig('%s/%s_location_%s_%s.jpg' % (listpath,regions[rrr],
                                                   lonsysnames_file[lonctr],
                                                   hemispheres[hemctr]),
                                                bbox_inches='tight', dpi=500)
            plt.show()
            plt.close()
            
#regions/arcs 
rrr = 0
for hemctr in range(len(hemispheres)):
    theta = np.zeros((0,2))
    theta_PPO_N = np.zeros((0,2))
    theta_PPO_S = np.zeros((0,2))
    colat = np.zeros((0,2))
    
    for filectr in range(len(filelist)):
        fff = filelist[filectr]
        
        npzfile = np.load(fff)
        carclist = npzfile['arclist'][()]
        cimgtimes = [datetime.strptime(carclist['group'][iii], '%Y-%jT%H_%M_%S.fits')
                                            for iii in range(len(carclist['group']))]    
        cimgtimes = [cimgtimes[iii]+timedelta(seconds=carclist['exp'][iii]/2)
                                            for iii in range(len(carclist['exp']))]  
        cimgtimes = tc.datetime2et(cimgtimes)
        ccoords = carclist['coords']
        clabels = carclist['labels']
        chem = carclist['hemisphere']
        cimgind = carclist['imgind']
        
        for actr in range(len(clabels)):
            if clabels[actr]==regions[rrr] and chem[0]==hemispheres[hemctr]:
                theta = np.append(theta, [ccoords[actr,2:]], axis=0)
                theta_PPO_N = np.append(theta_PPO_N, [pmp.LT2ppoN(pmp.uvislon2LT(ccoords[actr,2:]),
                                                                 cimgtimes[int(cimgind[actr])])], axis=0)
                theta_PPO_S = np.append(theta_PPO_S, [pmp.LT2ppoS(pmp.uvislon2LT(ccoords[actr,2:]),
                                                                 cimgtimes[int(cimgind[actr])])], axis=0)
                colat = np.append(colat, [ccoords[actr,:2]], axis=0) 

#    lonsys = [(theta/np.pi*12)%24, theta_PPO_N[:,::-1], theta_PPO_S[:,::-1]]
#    for lonctr in range(len(lonsys)):        
#        # polar cap plots
#        trange = np.linspace(0,2*np.pi,num=720)
#        rrange = np.linspace(0,30,num=60)        
#        data = np.zeros((len(trange), len(rrange)))        
#        for xctr in range(len(theta[:,0])):
#            rlims = colat[xctr,:]
#            tlims = lonsys[lonctr][xctr,:]
#            tlims = tlims % (2*np.pi)
#            if tlims[1]>= tlims[0]:
#                tmp1 = np.where(np.logical_and(trange>=tlims[0], trange<=tlims[1]))[0]
#            else:
#                tmp1 = np.append(np.where(trange<=tlims[0])[0], np.where(trange>=tlims[1])[0])
#            tmp2 = np.where(np.logical_and(rrange>=rlims[0], rrange<=rlims[1]))[0]
#            for iii in tmp2:
#                data[tmp1,iii] += 1        
#        fig = plt.figure()
#        fig.set_size_inches(6,6)
#        ax = plt.subplot(111, projection='polar')                    
#        ax.pcolormesh(trange, rrange, data.T, cmap=cmap_UV)        
#        ax.set_theta_zero_location("N")
#        ax.set_theta_direction(-1)
#        ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
#        ax.set_xticklabels(lonsys_ticks[lonctr])
#        ax.set_yticks([10,20,30])
#        ax.set_yticklabels([])
#        ax.grid('on', color='k', linewidth=1)
#        ax.set_rmax(30)
#        ax.set_rmin(0)
#        ax.set_title('Locations of all tagged %s %s, %s' % (hemnames[hemctr],
#                                                            regionnames[rrr],
#                                                         lonsysnames[lonctr]))            
#        plt.savefig('%s/%s_arc_location_%s_%s.jpg' % (listpath,regions[rrr],
#                                               lonsysnames_file[lonctr],
#                                               hemispheres[hemctr]),
#                                            bbox_inches='tight', dpi=500)
#        plt.show()
#        plt.close()
        
    LTvalues = (theta/np.pi*12)%24
    PPOvalues = [theta_PPO_N[:,::-1], theta_PPO_S[:,::-1]]
    pponames = ['PPO_N', 'PPO_S']
        
    for ppoctr in range(len(PPOvalues)):
        ltbins = np.linspace(0,24,num=25)
        ltstep = np.mean(np.diff(ltbins))
        ppobins = np.linspace(0,2*np.pi,num=25)
        ppostep = np.mean(np.diff(ppobins))
        data = np.zeros((len(ltbins)-1, len(ppobins)-1))
        
        for rctr in range(len(LTvalues[:,0])):
            ltlims = LTvalues[rctr,:]
            ppolims = PPOvalues[ppoctr][rctr,:]
            if np.diff(ltlims) > 0:
                tmp1 = np.where(np.logical_and(ltlims[0]-ltstep<ltbins, ltlims[1]>ltbins))[0]
            else:
                tmp1 = np.append(np.where(ltlims[1]>ltbins)[0],
                                            np.where(ltlims[0]-ltstep<ltbins)[0][:-1])
            if np.diff(ppolims) > 0:
                tmp2 = np.where(np.logical_and(ppolims[0]-ppostep<ppobins,
                                               ppolims[1]>ppobins))[0]
            else:
                tmp2 = np.append(np.where(ppolims[1]>ppobins)[0],
                                            np.where(ppolims[0]-ppostep<ppobins)[0][:-1])
            for iii in tmp1:
                data[iii,tmp2] += 1                
        fig = plt.figure()
        fig.set_size_inches(6,6)
        ax = plt.subplot(111)                    
        ax.pcolormesh(ltbins, ppobins*180/np.pi, data.T, cmap=cmap_UV) 
        ax.set_xlabel('LT (h)')
        ax.set_yticks(np.linspace(0,360,num=7))
        ax.set_ylabel('Magnetic longitude (deg)')
        ax.set_title('Locations of %s %s, %s' % (hemnames[hemctr],
                                                            regionnames[rrr],
                                                         pponames[ppoctr]))            
        plt.savefig('%s/%s_arc_hist_location_%s_%s.jpg' % (listpath,regions[rrr],
                                                      hemispheres[hemctr],
                                               pponames[ppoctr]),
                                            bbox_inches='tight', dpi=500)
        plt.show()
        plt.close()
                
            