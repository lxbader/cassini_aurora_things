# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import boundaries
import os
import plot_HST_UVIS
import time
import uvisdb
import re
import sys

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF
cp = plot_HST_UVIS.CassPlotter()


## calculate boundaries
#ib = boundaries.ImageBounds()
#p = re.compile(r'\d{4}_\d{3}T\d{2}_\d{2}_\d{2}')
#test = r'2008_239T03_37_51'
#p.findall(test)
#tmp = imglist['FILEPATH'].apply(p.findall)
#for iii in range(len(tmp)):
#    if tmp[iii][0] == test:
#        break
#filepath = imglist.iloc[iii]['FILEPATH']
#fullpath = '{}{}'.format(uvispath, filepath)
#ib.calculate(fullpath, plot=True, pdf=False)
#sys.exit()

savepath = os.path.join(plotpath, 'UVIS_ALL', time.strftime('%Y%m%d%H%M%S'))

for iii in imglist.index:
    if imglist.loc[iii, 'YEAR'] != 2017:
        continue
    print(iii, '/', len(imglist))
    print(imglist.loc[iii,'FILEPATH'])
    year = int(imglist.loc[iii,'YEAR'])
    doy = int(imglist.loc[iii,'DOY'])
    name = imglist.loc[iii,'FILEPATH'].strip('.fits').split('/')[-1].split('\\')[-1]
    tmpdir = os.path.join(savepath, str(year), '{}xx'.format(int(doy/100)))
    if not os.path.exists(tmpdir):
        os.makedirs(tmpdir)
        
    cp.makeplot(imglist.loc[iii,:], os.path.join(tmpdir, name), KR_MIN=0.5, KR_MAX=30,
                nodayglow=False)
#    sys.exit()
