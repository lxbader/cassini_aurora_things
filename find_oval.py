# plot HST/UVIS files
import path_register
pr = path_register.PathRegister()
gitpath = pr.gitpath
datapath = pr.datapath
plotpath = pr.plotpath
uvispath = pr.uvispath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime
import get_image
import matplotlib as mpl
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import os
import plot_HST_UVIS
from scipy.interpolate import UnivariateSpline
import time_conversions as tc
import uvisdb

import cv2

cd = cassinipy.CassiniData(datapath)

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

mpl.rcParams['image.cmap'] = 'inferno'

title = ''
savepath = '{}/find_oval/20180719'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

def myimshow(image):
    plot_HST_UVIS.simpleplot(image, 'E:/test', title, KR_MIN=0, KR_MAX=255, scale='lin')

udb = uvisdb.UVISDB()
imglist = udb.currentDF

# spherical to cartesian coordinates
def spher_to_cart(lat, lon):
    x = np.cos(lat) * np.cos(lon)
    y = np.cos(lat) * np.sin(lon)
    z = np.sin(lat)
    return x, y, z

# cartesian to spherical coordinates
def cart_to_spher(x, y, z):
    lat = np.arctan2(z, np.sqrt(x * x + y * y))
    lon = np.arctan2(y, x)
    return lat, lon

# 2d rotation about the angle a
def rotate(x, y, a):
    c = np.cos(a)
    s = np.sin(a)
    u = x * c + y * s
    v = -x * s + y * c
    return u, v

# get distance and great circle azimuth between two points
def inverse(lat1, lon1, lat2, lon2):
    x, y, z = spher_to_cart(lat2, lon2)
    x, y = rotate(x, y, lon1)
    z, x = rotate(z, x, np.pi / 2 - lat1)
    lat, lon = cart_to_spher(x, y, z)
    dist = np.pi / 2 - lat
    azi = np.pi - lon
    return dist, azi

# get point from starting point, distance and azimuth
def direct(lat1, lon1, dist, azi):
    x, y, z = spher_to_cart(np.pi / 2 - dist, np.pi - azi)
    z, x = rotate(z, x, lat1 - np.pi / 2)
    x, y = rotate(x, y, -lon1)
    lat2, lon2 = cart_to_spher(x, y, z)
    return lat2, lon2

def angular(lat1, lon1, lat2, lon2, azi13, azi23):
    failure = False
    dist12, azi21 = inverse(lat2, lon2, lat1, lon1)
    dist12, azi12 = inverse(lat1, lon1, lat2, lon2)
    cos_beta1 = np.cos(azi13 - azi12)
    sin_beta1 = np.sin(azi13 - azi12)
    cos_beta2 = np.cos(azi21 - azi23)
    sin_beta2 = np.sin(azi21 - azi23)
    cos_dist12 = np.cos(dist12)
    sin_dist12 = np.sin(dist12)
    lat3 = lon3 = 0
    if sin_beta1 == 0. and sin_beta2 == 0.:
        failure = True
        lat3 = 0.
        lon3 = 0.
    elif sin_beta1 == 0.:
        lat3 = lat2
        lon3 = lon2
    elif sin_beta2 == 0.:
        lat3 = lat1
        lon3 = lon1
    elif sin_beta1 * sin_beta2 < 0.:
        if np.fabs(sin_beta1) >= np.fabs(sin_beta2):
            cos_beta2 *= -1
            sin_beta2 *= -1
        else:
            cos_beta1 *= -1
            sin_beta1 *= -1
    else:
        dist13 = np.arctan2(np.fabs(sin_beta2) * sin_dist12,
                            cos_beta2 * np.fabs(sin_beta1) + np.fabs(sin_beta2) * cos_beta1 * cos_dist12)
        lat3, lon3 = direct(lat1, lon1, dist13, azi13)
    return failure, lat3, lon3



selec = imglist[(imglist['HEMISPHERE']=='North') &
                (imglist['POS_KRTP_R']<35)]

indices = [899,3093]
indices = selec.sample(10).index
indices = [2808,3826,2969,765,4034,3572,3638,3584,294,3303]
#indices = [3072]
for IND in indices:
    # print filename
    print(imglist.loc[IND,'FILEPATH'])
    
    # show plots or not
    plotall = True
    
    name = imglist.loc[IND,'FILEPATH'].strip('.fits').split('/')[-1]
    etstart = imglist.loc[IND,'ET_START']
    et_start_stop = [imglist.loc[IND,'ET_START'],imglist.loc[IND,'ET_STOP']]
    pystart = tc.et2datetime(etstart)
    hemsph = imglist.loc[IND,'HEMISPHERE']
    exp = imglist.loc[IND,'EXP']
    
    # get image
    image, _ = get_image.getRedUVIS('{}{}'.format(uvispath, imglist.loc[IND,'FILEPATH']), minangle=20)
    lonbins = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1)
    # only positive latitudes, no matter which hemisphere
    latbins = np.pi/2-np.linspace(0,1/6*np.pi,num=np.shape(image)[1]+1)
    
    # guess points in lat,lon [red]
    # simply using intensity maximum in each longitude slice
    # from 5 to 25 deg colatitude
    argguess = np.full(np.shape(image[:,0]), np.nan)
    for iii in range(len(argguess)):
        try:
            if np.sum(np.isfinite(image[iii,20:100]))/np.size(image[iii,20:100]) < 0.5:
                continue
            argguess[iii] = np.nanargmax(image[iii,20:100]) + 20
        except:
            continue
    # make 720 bins coinciding with lon=0, lon=0.5, ...
    argguess = np.concatenate([[argguess[-1]], argguess, [argguess[0]]])
    argguess = (argguess[:-1]+np.diff(argguess)/2)
    # get latitude values
    tmplats = latbins[:-1] + np.diff(latbins)/2
    tmp =  np.array([tmplats[argguess[iii].astype(np.int)]
                    if np.isfinite(argguess[iii])
                    else np.nan
                    for iii in range(len(argguess))])
    guess = np.array([tmp,lonbins])
    # get rid of outliers
    med = np.nanmedian(guess[0])
    tmp = np.where(np.abs(guess[0,:]-med) > np.radians(5))[0]
    guess[0,tmp] = med
    # median filter latitudes with a big box
    halfwin = int(len(argguess)/6)
    a = np.concatenate([guess[0,-halfwin:], guess[0,:], guess[0,:halfwin]])
    guess[0,:] = np.array([np.nanmedian(a[iii:iii+1+2*halfwin]) for iii in range(len(guess[0,:]))])

    # determine boundaries using spherical geometry / great circles
    center = guess[:,:-1]+np.diff(guess, axis=1)/2
    _, azi = inverse(guess[0,:-1], guess[1,:-1], guess[0,1:], guess[1,1:])
    # median box filter azimuth angles to ignore discontinuities
    # with steep azimuth angles
    halfwin = 21
    a = np.concatenate([azi[-halfwin:], azi, azi[:halfwin]])
    azi = np.array([np.nanmedian(a[iii:iii+1+2*halfwin])
                    for iii in range(len(azi))])
    
    # which range of angle-distance to extract in which resolution
    WIDTH = 10 #degree
    distrange = np.radians(np.linspace(-WIDTH,WIDTH,num=200))
    # storage arrays for extracted data
    lons = np.zeros((len(azi), len(distrange)), dtype=np.float64)
    lats = np.zeros_like(lons, dtype=np.float64)
    ints = np.full((len(azi), len(distrange)), np.nan)
    # extract "linearized" intensity data together with lon-lat values
    for iii in range(len(azi)):
        for jjj in range(len(distrange)):
            tmplat, tmplon = direct(center[0,iii], center[1,iii], distrange[jjj], azi[iii]+np.pi/2)
            tmplon = tmplon % (2*np.pi)
            lons[iii,jjj] = tmplon
            lats[iii,jjj] = tmplat
            if tmplat>latbins[0] or tmplat<latbins[-1] or np.any(np.isnan([tmplon,tmplat])):
                continue
            arglat = np.digitize(tmplat, latbins)-1
            arglon = np.digitize(tmplon, lonbins)-1
            ints[iii,jjj] = image[arglon, arglat]

    if plotall:
        # polar plot of selection and original image
        fig, ax = plt.subplots(1, 1, subplot_kw=dict(projection='polar'))
        fig.set_size_inches(8,8)
        ax.set_axis_bgcolor('gray')
        
        data = image.T
        data[np.where(data<=0)] = 0.00001
        quad = ax.pcolormesh(lonbins, np.pi/2-latbins, data, cmap=cmap_UV)
        quad.set_norm(mcolors.LogNorm(0.3,20))
        quad.cmap.set_under('k')
        quad.cmap.set_over('orangered')
        cbar = plt.colorbar(quad, pad=0.1, ax=ax, extend='both')
        cbar.set_label('Intensity (kR)', rotation=270, labelpad=20)
        ticklist = np.append(np.append(np.linspace(0.1,0.9,num=9),
                                           np.linspace(1,9,num=9)),
                                 np.linspace(10,90,num=9))
        cbar.set_ticks(ticklist)
        
        bound1 = np.array(direct(center[0,:], center[1,:], np.min(distrange), azi+np.pi/2))
        bound1[1,:] = bound1[1,:] % (2*np.pi)
        bound2 = np.array(direct(center[0,:], center[1,:], np.max(distrange), azi+np.pi/2))
        bound2[1,:] = bound2[1,:] % (2*np.pi)
        
        data = np.flipud(np.copy(guess))
        data[1,:] = np.pi/2 - data[1,:]
        ax.plot(*data, 'w')
        ax.plot(*data, 'k:')
        
        data = np.flipud(np.copy(bound1))
        data[1,:] = np.pi/2 - data[1,:]
        ax.plot(*data, 'r-')
        
        data = np.flipud(np.copy(bound2))
        data[1,:] = np.pi/2 - data[1,:]
        ax.plot(*data, 'b-')
        
        ax.set_rlim([0,np.pi/6])
        ax.grid()
        ax.set_theta_zero_location('N')
        ax.set_title('%s\n%s\n%s, %s s' % (datetime.strftime(pystart, '%Y-%j %H:%M:%S'),
                                   datetime.strftime(pystart, '%Y-%m-%d %H:%M:%S'),
                                   hemsph, exp), y=1.08)
        
        ax.set_xticks(np.arange(0,2*np.pi,np.pi/2))
        ax.set_xticklabels(['00','06','12','18'])
        ax.set_yticks(np.radians(np.arange(0,31,10)))
        ax.set_yticklabels([])
        
        tmp = cd.get_locations(np.mean(et_start_stop), 'KRTP')
        hemshort = 'N' if tmp[2]>=0 else 'S'
        ax.text(0.77*np.pi, 0.7,
                '{0:.2f} Rs\n{1:.2f}$\degree$ {2}\n{3:.2f} LT'.format(
                        tmp[1],tmp[2],hemshort,tmp[3]), color='k', fontsize=12,
                        horizontalalignment='center')

        plt.show()
        plt.close()
            
        # lon-lat plot of original and boundaries
        fig = plt.figure()
        fig.set_size_inches(9,6)
        ax = fig.add_subplot(111)
        ax.set_axis_bgcolor('gray')
        data = image.T
        data[np.where(data<=0)] = 0.00001
        quad = ax.pcolormesh(np.degrees(lonbins), np.degrees(latbins), data, cmap=cmap_UV)
        quad.set_norm(mcolors.LogNorm(0.3,20))
        quad.cmap.set_under('k')
        quad.cmap.set_over('orangered')
        cbar = plt.colorbar(quad, ax=ax, extend='both')
        cbar.set_label('Intensity (kR)', rotation=270)
        ticklist = np.append(np.append(np.linspace(0.1,0.9,num=9),
                                           np.linspace(1,9,num=9)),
                                 np.linspace(10,90,num=9))
        cbar.set_ticks(ticklist)
        
        ax.plot(*np.degrees(np.flipud(np.copy(guess))), 'w-')
        ax.plot(*np.degrees(np.flipud(np.copy(guess))), 'k:')
        ax.scatter(*np.degrees(np.flipud(np.copy(bound1))), color='r', marker='+', s=8)
        ax.scatter(*np.degrees(np.flipud(np.copy(bound2))), color='b', marker='+', s=3)
        ax.set_xlim([0,360])
        ax.set_xticks(np.linspace(0,360,num=5))
        ax.set_xlabel('Longitude (deg)')
        ax.set_ylim([90,60])
        ax.set_yticks(np.flipud(np.arange(60,91,5)))
        ax.set_yticklabels(np.arange(0,31,5))
        ax.set_ylabel('Colatitude (deg)')
        plt.show()
        plt.close()
        
    # define function for quick plots
    def plotIntermImage(img, title):
        fig = plt.figure()
        fig.set_size_inches(10,5)
        ax = fig.add_subplot(111)
        ax.pcolormesh(img.T)
        ax.set_title(title)
        plt.show()
        plt.close()
        
    # set up big plot
    if plotall:
        fig = plt.figure()
        fig.set_size_inches(16,24)
        gs = gridspec.GridSpec(4,2)
        gs.update(hspace=0.15, wspace=0.075)
        ax00 = plt.subplot(gs[0,0])
        ax01 = plt.subplot(gs[0,1])
        ax10 = plt.subplot(gs[1,0])
        ax11 = plt.subplot(gs[1,1])
        ax20 = plt.subplot(gs[2,0])
        ax21 = plt.subplot(gs[2,1])
        ax30 = plt.subplot(gs[3,0])
        ax31 = plt.subplot(gs[3,1])
#        ax40 = plt.subplot(gs[4,0])
#        ax41 = plt.subplot(gs[4,1])
        
    def addPlot(ax, img, title, xlabel=False, ylabel=False):
        ax.set_axis_bgcolor('gray')
        ax.pcolormesh(np.linspace(0,360,num=np.shape(img)[0]+1),
                      np.linspace(-WIDTH, WIDTH, num=np.shape(img)[1]+1),
                      img.T,
                      cmap=cmap_UV)
        ax.set_title(title)
        ax.set_xticks(np.arange(0,361,90))
        if xlabel:
            ax.set_xlabel('Longitude (deg)')
        ax.set_yticks(np.arange(-WIDTH,WIDTH+1,2))
        if ylabel:
            ax.set_ylabel('Distance from initial guess (deg)\n (-/+ poleward/equatorward)')

    # getting log10 original extracted part
    orig = np.copy(np.log10(ints))
    orig[orig<0] = 0
    if plotall:
        addPlot(ax00, orig, 'Original extraction in log-scaling', ylabel=True)
    
    # filler for nan values
    filler = np.copy(orig)
    filler[np.where(np.isnan(orig))] = 0
    filler = cv2.blur(filler, (int(np.shape(orig)[1]/2),
                               int(np.shape(orig)[0]/2)))
    filler *= np.nanmean(orig/filler)
#    if plotall:
#        #plotIntermImage(filler, 'filler')
    
    # original with nan filled
    orig_uint8 = np.copy(orig)
    orig_uint8[np.where(np.isnan(orig))] = filler[np.where(np.isnan(orig))]
    # convert to uint8
    orig_uint8 = np.array(orig_uint8*255/np.max(orig_uint8), dtype=np.uint8)
    if plotall:
        addPlot(ax01, orig_uint8, 'NaNs filled with box-filtered original')
                        
    # adaptive thresholding to detect features, blur
    adapt = cv2.adaptiveThreshold(orig_uint8, 1, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                   cv2.THRESH_BINARY, int(np.shape(orig_uint8)[1]+1), -10)
    if plotall:
        addPlot(ax10, adapt, 'Adaptive threshold applied', ylabel=True)
    adapt = cv2.medianBlur(adapt, int(np.shape(adapt)[1]/10)
                                        if np.shape(adapt)[1]/10 % 2
                                        else int(np.shape(adapt)[1]/10+1))
    if plotall:
        addPlot(ax11, adapt, 'Median filter applied')
    
    # filter the original
    blurred = cv2.blur(orig_uint8, (int(np.shape(orig_uint8)[1]/2),
                                    int(np.shape(orig_uint8)[0]/6)))

    if plotall:
        addPlot(ax20, adapt*orig_uint8, 'original masked with thresholding result', ylabel=True)
#        addPlot(ax21, blurred, 'B: box-filtered original')
#        # compare the original intensities of the extracted values with the
#        # median-filtered original
#        prominence = (adapt*orig_uint8)**2/blurred
#        prominence[np.isnan(prominence)] = 0
#        addPlot(ax30, prominence, 'A/B: "signal strength"', ylabel=True)
        
    # box filter (wide in lat, fine in lon)
    filt_img = cv2.blur(adapt*orig_uint8, (int(np.shape(orig_uint8)[1]/5),
                                           int(np.shape(orig_uint8)[0]/36)))
    filt_img = filt_img.astype(np.float)
    filt_img[np.where(np.isnan(orig))] = np.nan
    if plotall:
        addPlot(ax21, filt_img, 'box-filter applied')

    
    # find latitude of emission maximum
    # using center location of FWHM
    plotfwhm = False
    argpeak = np.full((np.shape(filt_img)[0]), np.nan)
    argpole = np.full((np.shape(filt_img)[0]), np.nan)
    for iii in range(np.shape(filt_img)[0]):
        if np.nanmax(filt_img[iii,:]) < 0.2:
            continue
        if np.sum(np.isnan(filt_img[iii,:]))/np.size(filt_img[iii,:]) > 1/3:
            continue
        # find maximum peak intensity
        p2pmax = np.nanmax(filt_img[iii,:])-np.nanmin(filt_img[iii,:])
        # spline, shifted downward by half the maximum value (0.7 in log scale)
        x = np.arange(np.shape(filt_img)[1])
        y = filt_img[iii,:]-0.7*p2pmax-np.nanmin(filt_img[iii,:])
        val = np.where(np.isfinite(y))[0]
        spl = UnivariateSpline(x[val],y[val],k=3, s=0)
        # roots = fwhm locations
        roots = spl.roots()
        # take care of no / multiple peaks
        if len(roots)<2:
            continue
        
        if len(roots)>2:
            try:
                tmp = np.argmax(filt_img[iii,:])
                rmin = np.max(roots[roots<tmp])
                rmax = np.min(roots[roots>tmp])
                roots = np.array([rmin, rmax])
            except:
                continue
            
        p_lats = lats[iii,roots.astype(np.int)]
        argpole[iii] = roots[np.nanargmax(p_lats)]
                
        # skip double peaks
        if len(roots)>2:
            xs = np.arange(roots[0], roots[-1], 0.1)
            slc = spl(xs)
            if np.any(slc<-p2pmax/5):
                continue
        # arc location = middle between fwhm locations
        argpeak[iii] = np.mean([roots[0],roots[-1]])

    # get standard deviation of detected centers in bigger window
    # UNUSED
    halfwin = 21
    a = np.concatenate([argpeak[-halfwin:], argpeak, argpeak[:halfwin]])
    std = np.array([np.nanstd(a[iii:iii+1+2*halfwin])
                    for iii in range(len(argpeak))])
    
    argpeak[np.where(np.abs(argpeak-np.nanmedian(argpeak))>50)] = np.nan
    argpole[np.where(np.abs(argpole-np.nanmedian(argpole))>50)] = np.nan
    
    # median box filter arc locations where std is low
    halfwin = 21
    a = np.concatenate([argpeak[-halfwin:], argpeak, argpeak[:halfwin]])
    argpeak = np.array([np.nanmedian(a[iii:iii+1+2*halfwin])
                        if (np.sum(np.isnan(a[iii:iii+1+2*halfwin]))/len(a[iii:iii+1+2*halfwin])<0.5)
                        else np.nan
                        for iii in range(len(argpeak))])
    
    # extract lon and lat values of the arc locations
    fin_lons = np.array([lons[iii,int(argpeak[iii])]
                        if not np.isnan(argpeak[iii])
                        else np.nan
                        for iii in range(len(argpeak))])
    fin_lats = np.array([lats[iii,int(argpeak[iii])] 
                        if not np.isnan(argpeak[iii]) 
                        else np.nan 
                        for iii in range(len(argpeak))])
    
    # bin lon-lat value pairs into regular grid as usually used for Saturn
    point_lonbins = np.linspace(0,2*np.pi,num=73)
    point_lons = point_lonbins[:-1] + np.diff(point_lonbins)/2
    tmp = np.digitize(fin_lons, point_lonbins)-1
    point_lats = np.zeros_like(point_lons)
    for iii in range(len(point_lats)):
        point_lats[iii] = np.nanmedian(fin_lats[np.where(tmp == iii)])
        
    # median box filter poleward boundaries where std is low
    halfwin = 21
    a = np.concatenate([argpole[-halfwin:], argpole, argpole[:halfwin]])
    argpole = np.array([np.nanmedian(a[iii:iii+1+2*halfwin])
                        if (np.sum(np.isnan(a[iii:iii+1+2*halfwin]))/len(a[iii:iii+1+2*halfwin])<0.5)
                        else np.nan
                        for iii in range(len(argpole))])
    
    # extract lon and lat values of the arc locations
    fin_lons_pole = np.array([lons[iii,int(argpole[iii])]
                        if not np.isnan(argpole[iii])
                        else np.nan
                        for iii in range(len(argpole))])
    fin_lats_pole = np.array([lats[iii,int(argpole[iii])] 
                        if not np.isnan(argpole[iii]) 
                        else np.nan 
                        for iii in range(len(argpole))])
    
    # bin lon-lat value pairs into regular grid as usually used for Saturn
    tmp = np.digitize(fin_lons_pole, point_lonbins)-1
    point_lats_pole = np.zeros_like(point_lons)
    for iii in range(len(point_lats_pole)):
        point_lats_pole[iii] = np.nanmedian(fin_lats_pole[np.where(tmp == iii)])
    
    if plotall:
        x_plot = np.linspace(0,360,num=721)
        x_plot = x_plot[:-1] + np.diff(x_plot)/2
        argpeak_plot = np.array([np.linspace(-WIDTH, WIDTH, num=201)[int(argpeak[iii])]
                                if np.isfinite(argpeak[iii])
                                else np.nan
                                for iii in range(len(argpeak))])
    
        # plot blurred thresholded image with arc locations
        addPlot(ax30, filt_img, 'with arc location', xlabel=True, ylabel=True)
        ax30.plot(x_plot,
                  argpeak_plot,
                  'w-')
        ax30.plot(x_plot,
                  argpeak_plot,
                  'k:')
        
        # plot original extracted image with approximate arc locations
        addPlot(ax31, orig, 'original with arc location', xlabel=True)
        ax31.plot(x_plot,
                  argpeak_plot,
                  'w-')
        ax31.plot(x_plot,
                  argpeak_plot,
                  'k:')
        plt.show()
        
    # polar plot of selection and original image
    fig, ax = plt.subplots(1, 1, subplot_kw=dict(projection='polar'))
    fig.set_size_inches(10,10)
    ax.set_axis_bgcolor('gray')
    
    data = image.T
    data[np.where(data<=0)] = 0.00001
    quad = ax.pcolormesh(lonbins, np.pi/2-latbins, data, cmap=cmap_UV)
    quad.set_norm(mcolors.LogNorm(0.3,20))
    quad.cmap.set_under('k')
    quad.cmap.set_over('orangered')
    cbar = plt.colorbar(quad, pad=0.1, ax=ax, extend='both')
    cbar.set_label('Intensity (kR)', rotation=270, labelpad=20)
    ticklist = np.append(np.append(np.linspace(0.1,0.9,num=9),
                                       np.linspace(1,9,num=9)),
                             np.linspace(10,90,num=9))
    cbar.set_ticks(ticklist)
    
    data = np.flipud(np.copy(guess))
    data[1,:] = np.pi/2 - data[1,:]
    ax.plot(*data, 'r:')
        
    data = np.flipud(np.array([point_lats,point_lons]))
    data[1,:] = np.pi/2 - data[1,:]
    ax.scatter(*data, color='w', edgecolor='k', s=20)
    
#    data = np.flipud(np.array([point_lats_pole,point_lons]))
#    data[1,:] = np.pi/2 - data[1,:]
#    ax.scatter(*data, color='y', edgecolor='k', s=20)    
    ax.set_rlim([0,np.pi/6])
    ax.grid()
    ax.set_theta_zero_location('N')
    ax.set_title('%s\n%s\n%s, %s s' % (datetime.strftime(pystart, '%Y-%j %H:%M:%S'),
                               datetime.strftime(pystart, '%Y-%m-%d %H:%M:%S'),
                               hemsph, exp), y=1.08)
    
    ax.set_xticks(np.arange(0,2*np.pi,np.pi/2))
    ax.set_xticklabels(['00','06','12','18'])
    ax.set_yticks(np.radians(np.arange(0,31,10)))
    ax.set_yticklabels([])
    
    tmp = cd.get_locations(np.mean(et_start_stop), 'KRTP')
    hemshort = 'N' if tmp[2]>=0 else 'S'
    ax.text(0.77*np.pi, 0.7,
            '{0:.2f} Rs\n{1:.2f}$\degree$ {2}\n{3:.2f} LT'.format(
                    tmp[1],tmp[2],hemshort,tmp[3]), color='k', fontsize=12,
                    horizontalalignment='center')

    plt.show()
    plt.close()    