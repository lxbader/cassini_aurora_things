import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cassinipy
from datetime import datetime
import get_image
import glob
import numpy as np
import os
import ppo_mag_phases as ppo
import skr_phases as skr
import time_conversions as tc

pmp = ppo.PPOMagPhases(datapath)
skrp = skr.SKRPhases(datapath)
cd = cassinipy.CassiniData(datapath)

instrument = ['HST', 'UVIS']
startkw = ['UDATE', 'STARTUTC']
expkw = ['EXPT', 'TOTALEXP']
timefmt = ['%Y-%m-%d %H:%M:%S', '%Y-%m-%d %H:%M:%S']

#===================================
# functions for magnetosheath model

def thetaSun(et):
    yy = tc.et2datetime(et).year
    years = np.array([tc.datetime2et(datetime(iii,1,1)) for iii in [yy,yy+1]])
    yearlen = np.asscalar(np.diff(years))
    fracyy = yy+(et-years[0])/yearlen
    a = -1.371
    b = -25.69*np.cos(2.816+0.213499*fracyy)
    c = 1.389*np.cos(5.4786+0.426998*fracyy)
    return a+b+c

def getZcs(et,r):
    THS = thetaSun(et)
    RH = 29
    return (r-RH*np.tanh(r/RH))*np.tan(THS)

def getCSdist(et,r,z):
    zcs = getZcs(et,r)
    RH = 29
    ncs = np.array([-np.tan(thetaSun(et))*np.tanh(r/RH)**2,0,1])
    thn = np.arccos(np.dot(ncs,np.array([0,0,1]))/np.linalg.norm(ncs))
    return (z-zcs)*np.cos(thn)

#============
# make lists
    
# count unreadable files
invctr = 0

#for iii in range(len(instrument)):
for iii in [1]:
    inst = instrument[iii]
    savepath = '%s/%s' % (datapath, inst)    
    if not os.path.exists(savepath):
        os.makedirs(savepath)
        
    data = {}
    data['FILEPATH'] = []
    data['ET_START_STOP'] = np.zeros((0,2))
    data['EXP'] = []
    data['HEMISPHERE'] = []
    data['PPO_N_S'] = np.zeros((0,2))
    data['SKR_N_S'] = np.zeros((0,2))
    data['CAP_COV_PERC'] = []
    data['POS_KSM'] = np.zeros((0,3))
    data['POS_KRTP'] = np.zeros((0,3))
    data['POS_ION'] = np.zeros((0,3))
    data['CS_DIST'] = []
    
    if inst == 'HST':
        fitsfiles = glob.glob('%s/all/**/*nobg.fits' % savepath, recursive=True)
    else:
        tmp = glob.glob('%s/PDS_UVIS/PROJECTIONS_RES2/**/*.fits' % savepath)
        fitsfiles = [tmp[iii] for iii in range(len(tmp)) if 'spec' not in tmp[iii]]
                
    for fctr in range(len(fitsfiles)):
        fff = fitsfiles[fctr]
        if fff.find('reduced')>0:
            continue
        print('File %s of %s: %s' % (fctr+1, len(fitsfiles), fff))
        try:
            # open file
            hdulist = fits.open(fff)
            hdu = hdulist[0]
            # get starttime
            pystart = hdu.header[startkw[iii]]
            pystart = datetime.strptime(pystart, timefmt[iii])
            start = tc.datetime2et(pystart)
        except:
            print('Failed to read')
            invctr += 1
        else:            
            # for HST: subtract light time
            if inst=='HST':
                lighttime = hdu.header['LIGHTIME']
                start = start-lighttime
            # get exposure time
            exp = hdu.header[expkw[iii]]
            if pystart.year == 2009 and 'F125LP' in fff:
                exp = 1200
            elif pystart.year == 2009 and 'F115LP' in fff:
                exp = 700
            # get stop time
            stop = start+exp
            
            # write basic file information
            data['FILEPATH'] = np.append(data['FILEPATH'], fff)
            data['ET_START_STOP'] = np.append(data['ET_START_STOP'], [[start,stop]], axis=0)
            data['EXP'] = np.append(data['EXP'], exp)
            
            # get hemisphere
            if inst=='HST':
                # HST image shape: [lat(S-N), lon(0-360)]
                image = hdu.data
                [latsize, lonsize] = np.shape(image)
                south = np.sum(np.where(image[:int(latsize/2),:]))
                north = np.sum(np.where(image[int(latsize/2):,:]))
                if north>south:
                    hemisphere = 'North'
                else:
                    hemisphere = 'South'
            elif inst=='UVIS':
                hemisphere = hdu.header['HEMSPH']
            data['HEMISPHERE'] = np.append(data['HEMISPHERE'], hemisphere)
             
            # get PPO angles
            try:
                ppo_n = pmp.getMagPhase(start+exp/2, 'N')
            except:
                ppo_n = np.nan
            try:
                ppo_s = pmp.getMagPhase(start+exp/2, 'S')
            except:
                ppo_s = np.nan
            # PPO phases CCW FROM MIDNIGHT
            data['PPO_N_S'] = np.append(data['PPO_N_S'], [[ppo_n,ppo_s]], axis=0)
            
            # get SKR angles
            try:
                skr_n = skrp.getSKRPhase(start+exp/2, 'N')
            except:
                skr_n = np.nan
            try:
                skr_s = skrp.getSKRPhase(start+exp/2, 'S')
            except:
                skr_s = np.nan
            data['SKR_N_S'] = np.append(data['SKR_N_S'], [[skr_n,skr_s]], axis=0)
                                                    
            # get KSM
            tmp = cd.get_locations([start+exp/2], refframe='KSM')
            data['POS_KSM'] = np.append(data['POS_KSM'], [tmp[1:]], axis=0)
            # get KRTP [R,LAT,LOCT]
            tmp = cd.get_locations([start+exp/2], refframe='KRTP')
            data['POS_KRTP'] = np.append(data['POS_KRTP'], [tmp[1:]], axis=0)
            # get ionospheric footprint [colat_N, colat_S, LOCT]
            tmp = cd.get_ionfootp([start+exp/2])
            data['POS_ION'] = np.append(data['POS_ION'], [np.squeeze(tmp[1:])], axis=0)
            
            # get distance from model current sheet
            loc = cd.get_locations(start+exp/2,'KSM')
            r = np.sqrt(loc[1]**2+loc[2]**2)
            z = loc[3]
            csdist = getCSdist(start+exp/2,r,z)
            data['CS_DIST'] = np.append(data['CS_DIST'], csdist)
            
    print('Saving data')
    np.savez('%s/%slist_RES2_20180531' % (savepath, inst), data=data)
    
    #=========================================================
    # rotate images according to PPO and save in one big file
    if inst=='UVIS':
        testimg, _ = get_image.getRedUVIS(data['FILEPATH'][0])
    elif inst == 'HST':
        testimg, _ = get_image.getRedHST(data['FILEPATH'][0])
    rot_img = np.full((len(data['FILEPATH']),3,)+np.shape(testimg), np.nan)
    
    print('Rotating images into PPO frame...')
    for imgctr in range(len(data['FILEPATH'])): 
        if not imgctr % 100:
            print(imgctr)
            
        red_image, thishem = get_image.getRedUVIS(data['FILEPATH'][imgctr], minangle=25)
        
        # save original image
        rot_img[imgctr,0,:,:] = red_image
        
        # calculate PPO phases and roll image accordingly
        refphases = data['PPO_N_S'][imgctr]
        [ppo_N,ppo_S] = refphases
        
        for ppoctr in range(2):            
            if np.isnan(refphases[ppoctr]):
                continue
            shift = -int(np.floor((np.shape(red_image)[0])*refphases[ppoctr]/(2*np.pi))) 
            rot_img[imgctr,ppoctr+1,:,:] = np.roll(red_image, shift, axis=0)
    np.savez('{}/{}_rot_img'.format(savepath, inst), rot_img=rot_img)

