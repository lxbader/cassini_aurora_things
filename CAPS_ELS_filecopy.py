from shutil import copyfile
import glob
import os

srcpath = 'F:/CAPS/DATA/CALIBRATED'
dstpath = 'D:/PhD/Data/CAPS/CO-E_J_S_SW-CAPS-3-CALIBRATED-V1.0/DATA/CALIBRATED'

filectr = 0
nofiles = len(glob.glob('%s/*/*/ELS*' % srcpath))
for fff in glob.glob('%s/*/*/ELS*' % srcpath):
    print('Copying file %s of %s' % (filectr, nofiles))
    tmp = fff.replace('\\', '/')
    tmp = tmp.split('/')
    year = tmp[4]
    doy = tmp[5]
    name = tmp[6]

    savedir = '%s/%s/%s' % (dstpath, year, doy)
    if not os.path.exists(savedir):
        os.makedirs(savedir)

    copyfile(fff, '%s/%s' % (savedir, name))
    filectr += 1