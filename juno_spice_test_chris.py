# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 14:10:56 2019

@author: bader
"""

import collections
from datetime import datetime
import numpy as np
import spiceypy as spice
import sys

spice.kclear()




junofolder = 'E:/data/JUNO'
ck = 'juno_sc_rec_190623_190629_v01.bc'
pck = 'spk_rec_190626_190817_190822.bsp'
sclk = 'JNO_SCLKSCET.00091.tsc'
ik = 'juno_jade_v01.ti'
lsk = 'naif0012.tls'
fk = 'juno_v12.tf'
kernels = [ck, pck, sclk, ik, lsk, fk]

fullkernels = ['{}/{}'.format(junofolder, k) for k in kernels]

spice.furnsh(fullkernels)
#spice.kinfo(lsk)





#test = 'E:/GIT/cassinipy//naif0012.tls'
#spice.furnsh(test)
#spice.kinfo('lsk')

#sys.exit()




print(spice.ktotal('all'))
    
# Turns python datetime into spice/ET time
# Input: datetime 1-D array/list or single value
def datetime2et(pytimes):
    isscalar = False
    if not isinstance(pytimes, collections.Iterable):
        isscalar = True
        pytimes = [pytimes]
    utctimes = np.array([datetime.strftime(iii, '%Y-%m-%d, %H:%M:%S.%f') for iii in pytimes])
    ettimes = np.array([spice.utc2et(iii) for iii in utctimes])
    if isscalar:
        return np.asscalar(ettimes)
    else:
        return ettimes

t = datetime(2019, 6, 27)
spice.pxform('JUNO_JADE_E060', 'IAU_JUPITER', datetime2et(t))


