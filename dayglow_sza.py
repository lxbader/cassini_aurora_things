import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
from datetime import datetime, timedelta
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as patheffects
import matplotlib.pyplot as plt
import numpy as np
import remove_solar_reflection
import scipy.optimize as sopt
import time_conversions as tc
import uvisdb

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF

rsr = remove_solar_reflection.RemoveSolarReflection()

times = np.arange(datetime(2007, 3, 1), datetime(2017, 10, 1), timedelta(days=1))
fmt = '%Y_%j'
stamps = np.array([datetime.strftime(iii.astype(datetime), fmt) for iii in times])

bins = 45
szabins = np.linspace(45, 90, num=bins+1)
szacenters = szabins[:-1] + np.diff(szabins)

npzfile = '{}/cassini_aurora_things/dayglow_sza_data'.format(gitpath)
try:
    tmp = np.load('{}.npz'.format(npzfile))
    bgn = tmp['bgn']
    bgs = tmp['bgs']
except:
    for hem in ['North', 'South']:
        allimg = imglist[imglist['HEMISPHERE']==hem]
        bg = np.full((len(stamps), bins), np.nan)
        for timectr in range(len(stamps)):
            year = int(stamps[timectr][:4])
            doy = int(stamps[timectr][5:])
            selec = allimg[(allimg['YEAR']==year) & (allimg['DOY']==doy)]
            if len(selec)<5:
                continue
            print(stamps[timectr])
            bgfiles = selec['FILEPATH'].as_matrix()
            if len(bgfiles)>10:
                bgfiles = np.random.choice(bgfiles, 10)
            try:
                rsr.calculate('{}{}'.format(pr.uvispath, bgfiles[0]), bgfiles=bgfiles)
            except:
                print('Background could not be estimated')
                continue
            bg[timectr, :] = rsr.f(szacenters)
        if hem=='North':
            bgn = np.copy(bg)
        else:
            bgs = np.copy(bg)
    np.savez(npzfile, bgn=bgn, bgs=bgs)

bg = np.concatenate([[bgn], [bgs]])
bg_filt = np.full(np.shape(bg), np.nan)
delt = 50
for hhh in range(bg.shape[0]):
    for lll in range(bg.shape[-1]):
        for iii in range(bg.shape[1]):
            bg_filt[hhh,iii,lll] = np.nanmean(bg[hhh,
                   np.max([0, iii-delt]):np.min([bg.shape[1], iii+delt]),lll])
sys.exit()
# make figure
#%%
panels = 'abcdefghijklmnopqrstuvwxyz'
pan_iter = iter(panels)

fig = plt.figure()
fig.set_size_inches(12,9)
gs = gridspec.GridSpec(2,2)
dtimes = np.append(times, [times[-1].astype(datetime)+timedelta(days=1)])
axn1 = plt.subplot(gs[0,0])
axs1 = plt.subplot(gs[0,1], sharex=axn1, sharey=axn1)
axn2 = plt.subplot(gs[1,0], sharex=axn1)
axs2 = plt.subplot(gs[1,1], sharex=axn1, sharey=axn2)

def cosine(x, a, b):
    # northern summer solstice
    solst = tc.datetime2et(datetime(2017,5,24))
    # orbital period
    orbit_t = 10759*24*3600 # [s]
    return a*np.cos((x-solst)/orbit_t*2*np.pi)+b

def setupAx(ax, left=False, bottom=False):
    ax.tick_params(axis='both', which='both', direction='out',
                   left=True, labelleft=left,
                   right=False, labelright=False,
                   top=False, labeltop=False,
                   bottom=True, labelbottom=bottom)
    ax.grid('on', which='both')
    ax.grid(which='major', linewidth=0.5, color='0.7')
    txt = ax.text(0.02, 0.95,
                 '({})'.format(next(pan_iter)),
                  transform=ax.transAxes, ha='left', va='top',
                  color='k', fontweight='bold', fontsize=15)
    txt.set_path_effects([patheffects.withStroke(linewidth=4, foreground='w')])

for hhh in range(2):
    ax = [axn1, axs1][hhh]
    ax.pcolormesh(dtimes, colats, bg_filt[hhh,:,:].T)
    setupAx(ax, left=True if not hhh else False)
    if not hhh:
        ax.set_ylabel('noon $\\leftarrow$ colatitude (deg) $\\rightarrow$ midnight')
        ax.set_yticks(np.arange(-30, 31, 10))
        ax.set_yticklabels(np.abs(np.arange(-30, 31, 10)))
    
for hhh in range(2):
    ax = [axn2, axs2][hhh]
    ax.scatter(times, bg[hhh,:,10], color='k', s=3, marker='o')
    setupAx(ax, left=True if not hhh else False, bottom=True)
    ax.set_ylim([0,6])
    valid = np.where(np.isfinite(bg[hhh,:,10]))[0]
    xval = tc.datetime2et(times[valid].astype(datetime))
    yval = bg[hhh,:,10][valid]
    opt, cov = sopt.curve_fit(cosine, xval, yval,
                               p0=(2 if not hhh else 1,
                                   5.5e8 if not hhh else 2.7e8,
                                   3 if not hhh else 1))
    ax.plot(times, cosine(tc.datetime2et(times.astype(datetime)), *opt))
    print(opt)

#%%
fig = plt.figure()
ax = plt.subplot()
equinox = tc.datetime2et(datetime(2009, 8, 10, 13, 1))
ettimes = tc.datetime2et(times.astype(datetime))
ettimes_mirr = equinox-(ettimes-equinox)

comb_x = np.concatenate([ettimes, ettimes_mirr])
comb_y = np.concatenate([bg[0,:,10], bg[1,:,10]])
val = np.where(np.isfinite(comb_y))[0]
comb_x = comb_x[val]
comb_y = comb_y[val]

ax.scatter(times, bg[0,:,10], color='royalblue', s=3, marker='o', label='N')
ax.scatter(times, bg[1,:,10], color='crimson', s=3, marker='o', label='S')
opt, cov = sopt.curve_fit(cosine, comb_x, comb_y,
                           p0=(2, 5.5e8, 3))
print(opt)
ax.plot(times, cosine(ettimes, *opt), color='k', lw=1)
ax.plot(times, 2*opt[-1]-cosine(ettimes, *opt), color='k', lw=1)

ax.set_xlabel('Years')
ax.set_ylabel('Dayglow intensity (kR)')
ax.set_ylim([0,6])
ax.legend(loc=2)
    
plt.savefig('{}/dayglow.png'.format(plotpath), bbox_inches='tight', dpi=300)



