# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath
plotpath = pr.plotpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import uvisdb

savepath = '{}/power_size'.format(plotpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

myblue='royalblue'
myred='crimson'

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF

# check open flux vs brightness
fig = plt.figure()
fig.set_size_inches(6,4)
selec = imglist[(imglist['HEMISPHERE']=='North') &
                (imglist['OCBMAXGAP']<30) &
                (np.isfinite(imglist['OPENFLUX'])) &
                (np.isfinite(imglist['UV_POWER']))]
ax = fig.add_subplot(111)
ax.scatter(selec['OPENFLUX'], selec['UV_POWER']/1e9, s=1, color='k', zorder=10)
ax.set_yscale('log')
ax.set_xlabel('Open flux (GWb)')
ax.set_ylabel('UV radiant flux (GW)')
ax.grid(which='both')
p = np.corrcoef(selec['OPENFLUX'], selec['UV_POWER'])[0,1]
ax.text(0.95, 0.95, '$p={:0.2f}$'.format(p), transform=ax.transAxes,
        fontsize=12, ha='right', va='top')
plt.savefig('{}/openflux_power.png'.format(savepath), bbox_inches='tight', dpi=300)
plt.savefig('{}/openflux_power.pdf'.format(savepath), bbox_inches='tight')
plt.show()

# check radius vs brightness
fig = plt.figure()
fig.set_size_inches(6,4)
selec = imglist[(imglist['HEMISPHERE']=='North') &
                (np.isfinite(imglist['CIRCLE_R'])) &
                (np.isfinite(imglist['UV_POWER']))]
ax = fig.add_subplot(111)
ax.scatter(selec['CIRCLE_R'], selec['UV_POWER']/1e9, s=1, color='k', zorder=10)
ax.set_yscale('log')
ax.set_xlabel('Circle fit radius (deg)')
ax.set_ylabel('UV radiant flux (GW)')
ax.grid(which='both')
p = np.corrcoef(selec['CIRCLE_R'], selec['UV_POWER'])[0,1]
ax.text(0.95, 0.95, '$p={:0.2f}$'.format(p), transform=ax.transAxes,
        fontsize=12, ha='right', va='top')
plt.savefig('{}/radius_power.png'.format(savepath), bbox_inches='tight', dpi=300)
plt.savefig('{}/radius_power.pdf'.format(savepath), bbox_inches='tight')
plt.show()
