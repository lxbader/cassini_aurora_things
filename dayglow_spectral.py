import path_register

pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
plotpath = pr.plotpath

import sys
sys.path.insert(0, '%s/uvis_stitch' % gitpath)

import glob
import matplotlib.cm as mcm
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.interpolate as sint
import spiceypy as spice
import stitch_calibrate_project
import sys

get2016data = False

metakernelpath = '{}/SPICE/metakernels'.format(datapath)
spice.kclear()
spice.furnsh('{}/cassini_generic.mk'.format(metakernelpath))
        
us = stitch_calibrate_project.UVISstitcher()
mainsavepath = '{}/Projects/2019 AB dayglow/projections_spectra'.format(boxpath)
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)

savefigpath = '{}/UVIS_dayglow'.format(plotpath)
if not os.path.exists(savefigpath):
    os.makedirs(savefigpath)

us.setSavePath(mainsavepath)
us.setResolution(2)
us.saveSpecs(False)

#%%
# =============================================================================
# Skim through entire datasets looking for files of interest
# =============================================================================
def getValue(line):
    value = line.split('\n')[0]
    value = value.split('= ')[1]
    value = value.strip('"')
    value = value.strip('(')
    value = value.strip(')')
    value = value.strip(') ')
    value = value.split(' <SECOND>')[0]
    return value

def getBandBin(lblfile):       
    with open(lblfile,'r') as lf:
        line = lf.readline()
        band_bin = np.nan
        try:
            while line:
                if (('BAND_BIN ' in line) or ('BAND BIN ' in line)) and '= ' in line:
                    band_bin = float(getValue(line))
                elif (('TARGET_NAME ' in line) or ('TARGET NAME ' in line)) and '= ' in line:
                    target = getValue(line)
                    if 'SATURN' not in target:
                        return np.nan
                elif (('SLIT_STATE ' in line) or ('SLIT STATE ' in line)) and '= ' in line:
                    slit_state = getValue(line)
                    slit_state = slit_state.replace('_',' ')
                    if ('LOW RESOLUTION' not in slit_state) and ('HIGH RESOLUTION' not in slit_state):
                        return np.nan                  
                line = lf.readline()
            return band_bin
        except:
            return np.nan
                
# Find all UVIS files with certain band binning
pdsfiles = glob.glob('{}/UVIS/PDS_UVIS/COUVIS_*/DATA/*/FUV*.LBL'.format(datapath), recursive=True)
laspfiles = glob.glob('{}/UVIS/PDS_UVIS/COUVIS_0060/FUV*.metadata'.format(datapath), recursive=True)
lblfiles = np.array(pdsfiles + laspfiles)

if get2016data:
    arg = np.where(np.array([True if ('FUV2016_303_02_27' in iii) or ('FUV2016_303_05_57' in iii) else False
                             for iii in lblfiles]))[0]
    fileselec = lblfiles[arg]
    BANDBIN = 64
else:
    BANDBIN = 1
    bandbins = np.array([getBandBin(iii) for iii in lblfiles]).astype(np.float)
    fileselec = lblfiles[np.where(bandbins==BANDBIN)[0]]

#%%
# =============================================================================
# Open interesting files and pick out relevant spectra, save them
# =============================================================================
COLATMIN = 27
LATMIN = -90+COLATMIN
LATMAX = 90-COLATMIN
ELMIN = 10

for typectr in range(2):
    savefilectr = 0
    specctr = 0
    specsavepath = '{}/UVIS/sorted_spectra/bandbin_{}{}'.format(datapath,
                    '2016303' if get2016data else BANDBIN,
                    '_corr' if typectr else '')
    if not os.path.exists(specsavepath):
        os.makedirs(specsavepath)
    SPECSPERFILE = 10000
    
    currentspecs = np.full((SPECSPERFILE, 1024//BANDBIN), np.nan)
    currentdata = np.full((SPECSPERFILE, 4), np.nan)
    
    for fff in range(len(fileselec)):
        thisfile = fileselec[fff].split('.')[0]
        LASP = True if 'COUVIS_0060' in thisfile else False
        
        try:
            # get calibrated spectral data [kR/A]
            us.getCalibIntegData(thisfile, LASP=LASP, ignoreSingleRecs=False)
        except Exception as e:
            print(e)
            continue
        # get spectral band width [A]
        bdiff = np.mean(np.diff(us.wave))  # it's a linear scale so we can simplify a bit
        cal_data = us.calib_data*bdiff
    
        # get FOV vectors in UVIS instrument frame
        us.getFovVectors()
        for rec in range(cal_data.shape[0]):
            thiset = np.mean(us.ettimes[rec:rec+2])
            # sun direction in KSM
            try:
                sunvec, _ = spice.spkpos('SUN', thiset, 'CASSINI_KSM', 'NONE', 'SATURN')
            except Exception as e:
                print(e)
                continue
            for px in range(cal_data.shape[1]):
                thiscenter = us.FOVcenters[px,:]
                try:
                    ll, el = us.proj_point(thiscenter, thiset)
                except Exception as e:
                    print(e)
                    continue
                
                # continue if outside region of interest
                if el<ELMIN: continue
                if ll[1]<LATMIN: continue
                if ll[1]>LATMAX: continue
                
                a = 1100+60268
                b = 1100+54364        
                geolon = np.radians(ll[0])
                geolat = np.arctan(a**2/b**2*np.tan(np.radians(ll[1])))
                
                x = np.cos(geolat)*np.cos(geolon+np.pi)
                y = np.cos(geolat)*np.sin(geolon+np.pi)
                z = np.sin(geolat)
                ksm_loc = (a + b) / 2 * np.array([x,y,z])
                
                # continue if ring-shadowed
                mult = z / sunvec[-1]
                if mult < 0:
                    ringplane_loc = ksm_loc + mult*sunvec
                    ringplane_dist = np.sqrt(np.sum(ksm_loc[:2]**2))
                    if (ringplane_dist > 60000) & (ringplane_dist<140000):
                        print('Ring shadowed', sunvec, ksm_loc, ll)
                        continue
                
                ab = np.sum(sunvec*np.array([x,y,z]))
                absa = np.linalg.norm(sunvec)
                absb = np.sqrt(x**2 + y**2 + z**2)
                sza = np.degrees(np.arccos(ab/absa/absb))
                
                # save files
                # correct intensities for viewing angle
                if not typectr:
                    currentspecs[specctr,:] = cal_data[rec, px,:]
                else:
                    currentspecs[specctr,:] = cal_data[rec, px,:]*np.sin(np.radians(el))
                currentdata[specctr,:] = [thiset, ll[0], ll[1], sza]
                specctr += 1
                if specctr==SPECSPERFILE:
                    np.savez('{}/{:05d}'.format(specsavepath, savefilectr),
                             currentspecs=currentspecs, currentdata=currentdata)
                    specctr = 0
                    savefilectr += 1
                    currentspecs = np.full((SPECSPERFILE, 1024//BANDBIN), np.nan)
                    currentdata = np.full((SPECSPERFILE, 4), np.nan)
    # save last half-full array               
    np.savez('{}/{:05d}'.format(specsavepath, savefilectr),
             currentspecs=currentspecs, currentdata=currentdata)           
    
#%%
# =============================================================================
# Load saved spectra, average and save result
# =============================================================================
for typectr in range(2):
    specsavepath = '{}/UVIS/sorted_spectra/bandbin_{}{}'.format(datapath,
                    '2016303' if get2016data else BANDBIN,
                    '_corr' if typectr else '')    

    specfiles = glob.glob('{}/0*.npz'.format(specsavepath))
    
#    for BINS in [8, 24]:
    for BINS in [8, 12, 24, 48]:
        szabins = np.linspace(0, 120, num=BINS+1)
        szaspectra = np.full((BINS, 1024//BANDBIN), np.nan)
        szaspectra_err = np.full((BINS, 1024//BANDBIN), np.nan)
        specnum = np.full((BINS), np.nan)
        for szactr in range(len(szabins)-1):
            allspectra = None
            alldata = None
            for fctr in range(len(specfiles)):
                data = np.load(specfiles[fctr])
                currentspecs = data['currentspecs']
                currentdata = data['currentdata']
                val = np.where((currentdata[:,-1]>szabins[szactr]) & 
                               (currentdata[:,-1]<szabins[szactr+1]))[0]
                if not len(val):
                    continue
                if allspectra is None:
                    allspectra = currentspecs[val,:]
                    alldata = currentdata[val,:]
                else:
                    allspectra = np.append(allspectra, currentspecs[val,:], axis=0)
                    alldata = np.append(alldata, currentdata[val,:], axis=0)
            if not allspectra is None:
                szaspectra[szactr, :] = np.nanmean(allspectra, axis=0)
                szaspectra_err[szactr, :] = np.nanstd(allspectra, axis=0)
                specnum[szactr] = allspectra.shape[0]
        np.savez('{}/avg_spectra_{}'.format(specsavepath, BINS),
             szabins=szabins, szaspectra=szaspectra,
             szaspectra_err=szaspectra_err,
             specnum=specnum)

#%%      
# =============================================================================
# Plot some spectra
# =============================================================================
for typectr in [0]:
    
    BANDBIN=1
    us.getCalibIntegData('E:/data/UVIS/PDS_UVIS\\COUVIS_0019\\DATA\\D2007_095\\FUV2007_095_08_41',
                         LASP=False, ignoreSingleRecs=False)
    wave = us.wave
    
#    BANDBIN=2
#    wave = (us.wave[::2]+us.wave[1::2])/2
    
    specsavepath = '{}/UVIS/sorted_spectra/bandbin_{}{}'.format(datapath,
                    '2016303' if get2016data else BANDBIN,
                    '_corr' if typectr else '')
    BINS = 8
    data = np.load('{}/avg_spectra_{}.npz'.format(specsavepath, BINS))
    szabins = data['szabins']
    szaspectra = data['szaspectra']
    szaspectra_err = data['szaspectra_err']
    specnum = data['specnum']
    
    cm = mcm.get_cmap('viridis')
    colorlist = [cm(iii) for iii in np.linspace(1, 0, num=BINS-1)]
        
    # full spectrum
    fig = plt.figure()
    fig.set_size_inches(9,6.3)
    ax = plt.subplot()
    for iii in range(1, BINS):
        ax.plot(wave, szaspectra[iii,:]*1e3, color=colorlist[iii-1], lw=1,
                label='{:d}-{:d}$^\circ$'.format(int(szabins[iii]), int(szabins[iii+1])))
    ax.set_yscale('log')
    ax.set_xlim([1150,1850])
    ax.set_ylim([1e-1, 300])
    handles, labels = plt.gca().get_legend_handles_labels()
    order = [0,4,1,5,2,6,3]
    handles, labels = zip(*[(handles[iii], labels[iii]) for iii in order])
    ax.legend(handles, labels, loc=9, shadow=True, facecolor='w', edgecolor='k', fancybox=True, ncol=4)
    ax.set_xlabel(r'Wavelength ($\mathrm{\AA}$)')
    ax.set_ylabel('Brightness (R)')
    ax.grid()
    ax.tick_params(axis='both', which='both', direction='in',
           left=True, labelleft=True,
           right=True, labelright=False,
           top=True, labeltop=False,
           bottom=True, labelbottom=True)

    plt.savefig('{}/spectral_bandbin_{}{}.png'.format(savefigpath, BANDBIN,
                '_corr' if typectr else ''), bbox_inches='tight', dpi=300)
    plt.savefig('{}/spectral_bandbin_{}{}.pdf'.format(savefigpath, BANDBIN,
                '_corr' if typectr else ''), bbox_inches='tight')
    plt.close()
    
    # with zoom views
    fig = plt.figure()
    fig.set_size_inches(9,19)
    gs = gridspec.GridSpec(3,1, hspace=0.1)
    for ctr in range(3):
        ax = plt.subplot(gs[ctr,0])
        for iii in range(1, BINS):
            ax.plot(wave, szaspectra[iii,:]*1e3, color=colorlist[iii-1], lw=1,
                    label='{:d}-{:d}$^\circ$'.format(int(szabins[iii]), int(szabins[iii+1])))
        ax.set_yscale('log')
        if not ctr:
            ax.set_xlim([1150,1850])
            ax.set_ylim([1e-1, 300])
            handles, labels = plt.gca().get_legend_handles_labels()
            order = [0,4,1,5,2,6,3]
            handles, labels = zip(*[(handles[iii], labels[iii]) for iii in order])
            ax.legend(handles, labels, loc=9, shadow=True, facecolor='w', edgecolor='k', fancybox=True, ncol=4)
        elif ctr==1:
            ax.set_xlim([1300,1525])
            ax.set_ylim([5e-2, 7])
        elif ctr==2:
            ax.set_xlim([1525,1850])
            ax.set_ylim([2e-1, 300])
        if ctr==2: ax.set_xlabel(r'Wavelength ($\mathrm{\AA}$)')
        ax.set_ylabel('Brightness (kR)')
        ax.grid()
        ax.tick_params(axis='both', which='both', direction='in',
               left=True, labelleft=True,
               right=True, labelright=False,
               top=True, labeltop=False,
               bottom=True, labelbottom=True)
    
    plt.savefig('{}/spectral_bandbin_zoom_{}{}.png'.format(savefigpath, BANDBIN,
                '_corr' if typectr else ''), bbox_inches='tight', dpi=300)
    plt.savefig('{}/spectral_bandbin_zoom_{}{}.pdf'.format(savefigpath, BANDBIN,
                '_corr' if typectr else ''), bbox_inches='tight')
    plt.close()

#%%
# =============================================================================
# Integrate average spectra the same way as the images
# =============================================================================
for typectr in [1]:
    BANDBIN=1
    us.getCalibIntegData('E:/data/UVIS/PDS_UVIS\\COUVIS_0019\\DATA\\D2007_095\\FUV2007_095_08_41',
                         LASP=False, ignoreSingleRecs=False)
    wave = us.wave
    
#    BANDBIN=2
#    wave = (us.wave[::2]+us.wave[1::2])/2
    
    for intmethod in ['DAYGLOW','REFL_SUNLIGHT']:
        specsavepath = '{}/UVIS/sorted_spectra/bandbin_{}{}'.format(datapath,
                        '2016303' if get2016data else BANDBIN,
                        '_corr' if typectr else '')
        BINS = 24
        data = np.load('{}/avg_spectra_{}.npz'.format(specsavepath, BINS))
        szabins = data['szabins']
        szaspectra = data['szaspectra']
        szaspectra_err = data['szaspectra_err']
        specnum = data['specnum']
        
        szacents = szabins[:-1]+np.mean(np.diff(szabins))/2
        # desired wavelength range (A)
        if intmethod=='DAYGLOW':
            min_lim = 1250
            max_lim = 1550
        elif intmethod=='REFL_SUNLIGHT':
            min_lim = 1650
            max_lim = 1800
        # interpolate to find out which bins we need
        f_int = sint.interp1d(wave,np.arange(len(wave)))
        min_bin = f_int(min_lim)
        max_bin = f_int(max_lim)
        # for each bin, find out which fraction overlaps with the desired range
        multipliers = np.zeros((len(wave)))
        for iii in range(len(multipliers)):
            left = np.max([iii-0.5,min_bin])
            right = np.min([iii+0.5,max_bin])
            if right<left:
                continue
            else:
                multipliers[iii] = right-left
        # integrate (Gustin 2016, see UVIS projection code)
        bdiff = np.mean(np.diff(wave))
        szabrightness = np.sum(szaspectra*np.tile(multipliers, (BINS, 1)), axis=-1)*bdiff#*8.1
        szabrightness_err = np.sqrt(np.sum((szaspectra_err*np.tile(multipliers, (BINS, 1)))**2, axis=-1))*bdiff#*8.1
        
        np.savez('{}/sza_brightness_{}_{}'.format(specsavepath, BINS, intmethod),
                 szabins=szabins, szacents=szacents,
                 szabrightness=szabrightness,
                 szabrightness_err=szabrightness_err,
                 specnum=specnum)
    
#    fig = plt.figure()
#    fig.set_size_inches(5,5)
#    ax = plt.subplot()
#    ax.errorbar(szacents, szabrightness, yerr=szabrightness_err)
#    ax.set_xlim([30,120])
#    plt.savefig('{}/sza_brightness_{}{}.png'.format(savefigpath, BANDBIN,
#                '_corr' if typectr else ''), bbox_inches='tight', dpi=300)
#    plt.savefig('{}/sza_brightness_{}{}.pdf'.format(savefigpath, BANDBIN,
#                '_corr' if typectr else ''), bbox_inches='tight')
#    plt.close()
#        
