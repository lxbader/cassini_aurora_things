import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from datetime import datetime
import numpy as np
import os
import plot_HST_UVIS
import time
import time_conversions as tc

TIME_DIFF_SEC = 3600*24

savepath = '%s/Plots/HST-UVIS/%s' % (boxpath, time.strftime('%Y%m%d%H%M%S'))
if not os.path.exists(savepath):
    os.makedirs(savepath)

# import HST and UVIS image lists
hstlist = np.load('%s/HST/HSTlist.npz' % datapath)['data'][()]
uvislist = np.load('%s/UVIS/UVISlist_RES2.npz' % datapath)['data'][()]

# center of each exposure
hsttimes = np.mean(hstlist['ET_START_STOP'], axis=1)
uvistimes = np.mean(uvislist['ET_START_STOP'], axis=1)

hstind = np.array([], dtype=np.int16)
uvisind = np.array([], dtype=np.int16)

for hstctr in range(len(hsttimes)):
    tmp1 = np.where(np.abs(uvistimes-hsttimes[hstctr]) < TIME_DIFF_SEC)
    tmp2 = np.where(uvislist['EXP']>400)
    tmp3 = np.intersect1d(tmp1, tmp2)
    if np.size(tmp3):
        hstind = np.append(hstind, hstctr)
        uvisind = np.append(uvisind, tmp3)
        
hstind = np.unique(hstind)
uvisind = np.unique(uvisind)

alltimes = np.concatenate([np.take(hsttimes, hstind), np.take(uvistimes, uvisind)])
alltimes = np.sort(alltimes)
tmp = np.where(np.diff(alltimes)>3600*24*2)[0]+1
argtimelims = np.concatenate([np.array([0]),tmp,np.array([len(alltimes)])])

labels = np.take(alltimes, argtimelims[:-1])
labels = tc.et2datetime(labels)
labels = [datetime.strftime(labels[iii], '%Y-%j') for iii in range(len(labels))]


for tctr in range(len(argtimelims)-2):
    
    fullsavepath = '%s/%s/HST' % (savepath, labels[tctr])    
    if not os.path.exists(fullsavepath):
        os.makedirs(fullsavepath)
    tmp = np.where((hsttimes>alltimes[argtimelims[tctr]]-1)*(hsttimes<alltimes[argtimelims[tctr+1]]-1))[0]
    hstselec = np.intersect1d(tmp, hstind)
    for iii in hstselec:
        try:
            thisdate = datetime.strftime(tc.et2datetime(hstlist['ET_START_STOP'][iii][0]), '%Y_%jT%H_%M')
            print('Plotting HST %s' % thisdate)
            plot_HST_UVIS.makeplot('HST', iii, '%s/HST_%s' % (fullsavepath,thisdate), KR_MAX=20, scale='log')
        except:
            print('HST %s failed' % iii)
    
    fullsavepath = '%s/%s/UVIS' % (savepath, labels[tctr])    
    if not os.path.exists(fullsavepath):
        os.makedirs(fullsavepath)
    tmp = np.where((uvistimes>alltimes[argtimelims[tctr]]-1)*(uvistimes<alltimes[argtimelims[tctr+1]]-1))[0]
    uvisselec = np.intersect1d(tmp, uvisind)
    for iii in uvisselec:
        try:
            thisdate = datetime.strftime(tc.et2datetime(uvislist['ET_START_STOP'][iii][0]), '%Y_%jT%H_%M')
            print('Plotting UVIS %s' % thisdate)
            plot_HST_UVIS.makeplot('UVIS', iii, '%s/UVIS_%s' % (fullsavepath,thisdate),
                                   KR_MAX=20, scale='log')
        except:
            print('UVIS %s failed' % iii)
        
        
        
        
        
        