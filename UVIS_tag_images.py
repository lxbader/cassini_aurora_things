from __future__ import unicode_literals

import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cubehelix
from datetime import datetime
# For use outside of spyder:
#import matplotlib
#matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import os
from PyQt5 import QtCore, QtWidgets

progname = "UVIS Image Tagger"
progversion = "0.1"

# define some colors
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5,
                         minLight=0.15, gamma=1.5)
hex_red = '#e74c3c'
hex_green = '#16a085'
KR_MAX = 5 

# define data paths
fitspath = '%s/UVIS/All' % datapath
listpath = '%s/full_set' % imglistpath

# which subsets and tags are available
subsetlist = np.array(['all', 'low_latitude'])
taglist = np.array(['none', 'main', 'poleward', 'equatorward'])

# import info file about image groups
ext_npzfile = np.load('%s/group_info.npz' % listpath)
ext_groupdata = ext_npzfile['groupdata'][()]
ext_labellist = ext_npzfile['labellist'][()]

# create group lists for each subset
grouplists = {}
groupnames = {}
grouptimes = {}
shgroupnames = {}
for iii in subsetlist:
    grouplists[iii] = ext_groupdata['name'][np.where(ext_groupdata[iii])]
    grouptimes[iii] = np.array([datetime.strptime(jjj, '%Y-%jT%H_%M_%S')
                            for jjj in grouplists[iii]])
    shgroupnames[iii] = np.array([datetime.strftime(jjj, '%Y-%m-%dT%H:%M')
                            for jjj in grouptimes[iii]])
    grouplists[iii] = np.array(['%s/%s.txt' % (listpath, jjj) for jjj in grouplists[iii]])

# turns filename of a UVIS file into an absolute path
def makePath(imgfile):
    return '%s/%s' % (fitspath, imgfile)

# manages displayed image and points of the current group
class DataContainer(QtCore.QObject):
    updateSignal = QtCore.pyqtSignal()
    arcstatuslabel = ['set inner radius', 'set outer radius', 'set lower longitude', 'set higher longitude']
    csubset = []
    cgroupfile = []
    cgroup = []
    cimage = []
    csubsetind = 0
    cgroupind = 0
    cimageind = 0
    cview = 'None'
    
    cdatafile = []
    pointlist = {}
    point_highlight = np.nan
    arclist = {}
    arc_highlight = np.nan
    
    npointcoords = []
    npointactive = False    
    narccoords = []
    narcactive = False
    narcstatus = np.nan
    
    def __init__(self):
        QtCore.QObject.__init__(self)
        self.updateSubset()
        
    def updateSubset(self):
        self.csubset = grouplists[subsetlist[self.csubsetind]]
        self.updateGroup()
        
    def updateGroup(self):
        if self.npointactive:
            self.point_deactivate()
        if self.narcactive:
            self.arc_deactivate()
        if np.any(self.pointlist) and np.any(self.arclist):
            self.saveData()
        self.point_highlight = np.nan
        self.arc_highlight = np.nan
        self.cimageind = 0
        self.cgroupfile = self.csubset[self.cgroupind]
        self.cdatafile = '%s_data.npz' % self.cgroupfile.split('.')[0]
        self.cgroup = np.array([])
        f = open(self.cgroupfile, 'r')
        while True:
            tmp = f.readline()
            if tmp:
                self.cgroup = np.append(self.cgroup, tmp.split('\n')[0])
            else:
                f.close()
                break
        self.loadData()
        self.updateImage()
            
    def updateImage(self):
        if self.narcactive:
            self.arc_deactivate()
        hdulist = fits.open(makePath(self.cgroup[self.cimageind]))
        hdu = hdulist[0]
        self.cimage = hdu.data
        if np.any(self.cimage[0:60,:]>0.1):
            self.cview = 'South'
        else:
            self.cimage = self.cimage[::-1]
            self.cview = 'North'
        self.updateSignal.emit()
        
    def setGroupInd(self, ind):
        self.cgroupind = ind
        self.updateGroup()
        
    def nextGroup(self):
        maxind = len(self.csubset)-1
        if self.cgroupind == maxind:
            pass
        else:
            self.setGroupInd(self.cgroupind+1)
    
    def prevGroup(self):
        minind = 0
        if self.cgroupind == minind:
            pass
        else:
            self.setGroupInd(self.cgroupind-1)
    
    def setImageInd(self, ind):
        self.cimageind = ind
        self.updateImage()
        
    def nextImage(self):
        maxind = len(self.cgroup)-1
        if self.cimageind == maxind:
            pass
        else:
            self.setImageInd(self.cimageind+1)
    
    def prevImage(self):
        minind = 0
        if self.cimageind == minind:
            pass
        else:
            self.setImageInd(self.cimageind-1)
    
    def saveData(self):
        np.savez(self.cdatafile, pointlist=self.pointlist, arclist=self.arclist)
    
    def loadData(self):
        if os.path.isfile(self.cdatafile):
            npzfile = np.load(self.cdatafile)
            self.pointlist = npzfile['pointlist'][()]
            self.arclist = npzfile['arclist'][()]
        else:
            self.resetPoints()
            self.resetArcs()
            
    def resetPoints(self):
        self.pointlist['group'] = self.cgroup
        self.pointlist['exp'] = np.full((len(self.cgroup)), np.nan)
        self.pointlist['hemisphere'] = np.full((len(self.cgroup)), 'X')
        for iii in range(len(self.cgroup)):
            try:
                fff = makePath(self.cgroup[iii])
                hdulist = fits.open(fff)
                hdu = hdulist[0]
                self.pointlist['exp'][iii] = hdu.header['TOTALEXP']
                tmp = hdu.data
                if np.any(tmp[0:60,:]>0.1):
                    self.pointlist['hemisphere'][iii] = 'S'
                else:
                    self.pointlist['hemisphere'][iii] = 'N'
            except:
                pass
        self.pointlist['coords'] = np.full((0, len(self.cgroup), 2), np.nan)
        self.pointlist['labels'] = np.full((0), 'none', dtype='<U25')
        self.pointlist['trackable'] = np.full((0), np.nan, dtype=np.bool)
        
    def resetArcs(self):        
        self.arclist['group'] = self.pointlist['group']
        self.arclist['exp'] = self.pointlist['exp']
        self.arclist['hemisphere'] = self.pointlist['hemisphere']
        self.arclist['coords'] = np.full((0,4), np.nan)
        self.arclist['labels'] = np.full((0), 'none', dtype='<U25')
        self.arclist['imgind'] = np.full((0), np.nan)
        
    # activating adding of points
    def point_activate(self):
        if self.narcactive:
            self.arc_deactivate()
        self.npointcoords = np.full((len(self.cgroup), 2), np.nan)
        self.npointactive = True
        self.updateSignal.emit()
    
    # ... and deactivating
    def point_deactivate(self):
        self.npointcoords = []
        self.npointactive = False
        self.updateSignal.emit()
        
    # activating adding of arcs
    def arc_activate(self):
        if self.npointactive:
            self.point_deactivate()
        self.narccoords = np.full((4), np.nan)
        self.narcactive = True
        self.narcstatus = 0
        self.updateSignal.emit()
    
    # ... and deactivating
    def arc_deactivate(self):
        self.narccoords = np.array([])
        self.narcactive = False
        self.narcstatus = np.nan
        self.updateSignal.emit()
    
    # progressing one step in the arc input
    def arc_next(self):
        if np.isfinite(self.narcstatus):
            if self.narcstatus < 4:
                if self.narcstatus < 2:
                    cond1 = self.narccoords[self.narcstatus] > 0
                    cond2 = self.narccoords[self.narcstatus] < 30
                    if cond1 and cond2:
                        self.narcstatus += 1
                else:
                    self.narcstatus += 1
                self.updateSignal.emit()
    
    # adding a point to the saved list
    def addPoint(self, label, trackable):
        if np.any(self.npointcoords):
            self.pointlist['coords'] = np.append(self.pointlist['coords'],
                                                  [self.npointcoords],axis=0)
            self.pointlist['labels'] = np.append(self.pointlist['labels'],
                                                  [label],axis=0)
            self.pointlist['trackable'] = np.append(self.pointlist['trackable'],
                                                  [trackable],axis=0)
            self.point_deactivate()
            self.updateSignal.emit()
        
    # deleting a point from the saved list
    def removePoint(self, ind):
        if ind>=0:
            self.pointlist['coords'] = np.delete(self.pointlist['coords'],
                                                  ind,axis=0)
            self.pointlist['labels'] = np.delete(self.pointlist['labels'],
                                                  ind,axis=0)
            self.pointlist['trackable'] = np.delete(self.pointlist['trackable'],
                                                  ind,axis=0)
            self.point_highlight = np.nan
#        elif ind==0:
#            self.resetPoints()
#            self.point_highlight = np.nan
            self.updateSignal.emit()
        
    # adding an arc to the saved list
    def addArc(self, label):
        if np.all(self.npointcoords) and self.narcstatus==3:
            self.arclist['coords'] = np.append(self.arclist['coords'],
                                                  [self.narccoords],axis=0)
            self.arclist['labels'] = np.append(self.arclist['labels'],
                                                  [label],axis=0)
            self.arclist['imgind'] = np.append(self.arclist['imgind'],
                                                  [self.cimageind],axis=0)
            self.arc_deactivate()
            self.updateSignal.emit()
        
    # deleting a point from the saved list
    def removeArc(self, ind):
        if ind>=0:
            self.arclist['coords'] = np.delete(self.arclist['coords'],
                                                  ind,axis=0)
            self.arclist['labels'] = np.delete(self.arclist['labels'],
                                                  ind,axis=0)
            self.arclist['imgind'] = np.delete(self.arclist['imgind'],
                                                  ind,axis=0)
            self.arc_highlight = np.nan
#        elif ind==0:
#            self.resetArcs()
#            self.arc_highlight = np.nan
            self.updateSignal.emit()
        
dc = DataContainer()
           
# widget displaying the current UVIS image and the marked points
class PlotWidget(FigureCanvas):
    
    theta = np.array(range(720))*.5
    theta = np.radians(theta)
    r = np.array(range(360))*.5
    levels = np.linspace(0.1, KR_MAX, 40)
        
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111, projection='polar')
        
        self.initial_plot()        
        self.plot_figure()

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)
        
    def initial_plot(self):
        self.quad = self.axes.pcolormesh(self.theta, self.r, dc.cimage, cmap=cmap_UV)
        
        self.quad.cmap.set_under('k')
        self.quad.cmap.set_over('orangered')
        self.quad.set_clim(0, KR_MAX)
        
        self.axes.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
        self.axes.set_xticklabels(['00','06','12','18'])
        self.axes.set_yticks([10,20,30])
        self.axes.set_yticklabels([])
        self.axes.grid('on', color='w', linewidth=2)
        self.rescale()
        
    def rescale(self):
        self.axes.set_theta_zero_location("N")
        self.axes.set_rmax(30)
        self.axes.set_rmin(0)        
        
    def plot_figure(self): 
#        # contourf looks nicer but is not updateable, so much slower
#        p = self.axes.contourf(self.theta, self.r, dc.cimage, self.levels,
#                               cmap=cmap_UV, extend='both')
        
        # plot image
        self.quad.set_array(dc.cimage[:-1,:-1].ravel())
        
        # plot points
        if dc.npointactive:
            self.axes.scatter(dc.npointcoords[dc.cimageind,0],
                              dc.npointcoords[dc.cimageind,1],
                              color='gold', marker='x', s=25)
        if np.size(dc.pointlist['labels']):
            self.axes.scatter(dc.pointlist['coords'][:,dc.cimageind,0],
                              dc.pointlist['coords'][:,dc.cimageind,1],
                              color='k', marker='x', s=25)
        if np.isfinite(dc.point_highlight):
            self.axes.scatter(dc.pointlist['coords'][dc.point_highlight,dc.cimageind,0],
                              dc.pointlist['coords'][dc.point_highlight,dc.cimageind,1],
                              edgecolor='lime', marker='o', facecolor='None', s=80)
        # plot regions
        if dc.narcactive:
            self.axes.plot(np.linspace(0,2*np.pi, num=100), np.ones(100)*dc.narccoords[0], color='gold')
            self.axes.plot(np.linspace(0,2*np.pi, num=100), np.ones(100)*dc.narccoords[1], color='gold')
            self.axes.plot(np.ones(50)*dc.narccoords[2], np.linspace(0.1,30,num=50), color='gold')
            self.axes.plot(np.ones(50)*dc.narccoords[3], np.linspace(0.1,30,num=50), color='gold')
            if dc.narcstatus == 3 and np.isfinite(dc.narccoords[3]):
                tmp = self.makeRegion(dc.narccoords)
                self.axes.plot(tmp[0,:],tmp[1,:],color='red')
        if np.size(dc.arclist['labels']):
            tmpvalid = np.where(dc.arclist['imgind'] == dc.cimageind)[0]
            if np.size(tmpvalid):
                for iii in tmpvalid:
                    tmp = self.makeRegion(dc.arclist['coords'][iii,:])
                    self.axes.plot(tmp[0,:],tmp[1,:],color='k')
        if np.isfinite(dc.arc_highlight):
            if dc.arclist['imgind'][dc.arc_highlight] == dc.cimageind:
                tmp = self.makeRegion(dc.arclist['coords'][dc.arc_highlight,:])
                self.axes.plot(tmp[0,:],tmp[1,:],color='lime')
        self.rescale()
        
    def update_figure(self):
        # clear scatters
        if len(self.axes.collections) > 1:
            del self.axes.collections[1:len(self.axes.collections)]
        # clear lines/boxes
        if hasattr(self.axes, 'lines'):
            del self.axes.lines[0:]
        self.plot_figure()
        self.draw()
        
    def on_click(self, event):
        if dc.npointactive:
            dc.npointcoords[dc.cimageind,:] = [event.xdata, event.ydata]
            self.update_figure()
        if dc.narcactive:
            if dc.narcstatus==0:
                dc.narccoords[dc.narcstatus] = event.ydata
            elif dc.narcstatus==1:
                if event.ydata >= dc.narccoords[0]:
                    dc.narccoords[dc.narcstatus] = event.ydata
            elif dc.narcstatus==2:
                dc.narccoords[dc.narcstatus] = event.xdata
            elif dc.narcstatus==3:
                if event.xdata <= dc.narccoords[2]:
                    dc.narccoords[dc.narcstatus] = event.xdata+2*np.pi
                else:
                    dc.narccoords[dc.narcstatus] = event.xdata
            self.update_figure()
                    
    def makeRegion(self, coords):
        [rmin,rmax,tmin,tmax] = coords
        rrange = np.linspace(rmin, rmax, num=100)
        trange = np.linspace(tmin, tmax, num=100)
        line1 = np.array([rrange, np.ones(100)*tmin])
        line2 = np.array([np.ones(100)*rmax, trange])
        line3 = np.array([np.flipud(rrange), np.ones(100)*tmax])
        line4 = np.array([np.ones(100)*rmin, np.flipud(trange)])
        return np.flipud(np.concatenate([line1, line2, line3, line4], axis=1))
        
# main window, organizing layout and functionality     
class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("%s, Version %s" % (progname,progversion))
        
        # menu bar on top
        self.file_menu = QtWidgets.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)
                    
        self.help_menu = QtWidgets.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)
        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtWidgets.QWidget(self)
        box = QtWidgets.QGridLayout(self.main_widget)
        
        #------------
        # main plot
        self.pw = PlotWidget(self.main_widget)
        box.addWidget(self.pw,1,1)
        
        #----------------------
        # prev/next buttons
        h1 = QtWidgets.QHBoxLayout(self)        
        self.prev_button = QtWidgets.QPushButton('<Previous', self)
        self.prev_button.clicked.connect(self.on_prev_button_clicked)
        h1.addWidget(self.prev_button)                
        self.next_button = QtWidgets.QPushButton('Next>', self)
        self.next_button.clicked.connect(self.on_next_button_clicked)
        h1.addWidget(self.next_button)
        box.addLayout(h1,2,1)
        
        #----------------------------
        # info and menu
        v = QtWidgets.QVBoxLayout(self)
        
        # subset selection
        tmp = QtWidgets.QLabel('<b>Select subset</b>', self)
        v.addWidget(tmp)
        self.subset_box = QtWidgets.QComboBox(self)
        for iii in subsetlist:
            self.subset_box.addItem(iii)
        self.subset_box.currentIndexChanged.connect(self.on_subset_changed)
        v.addWidget(self.subset_box)        
        
        # some labels
        tmp = QtWidgets.QLabel('<b>Current file</b>', self)
        v.addWidget(tmp)
        self.file_label = QtWidgets.QLabel(dc.cgroup[dc.cimageind], self)
        v.addWidget(self.file_label) 
        
        sbox = QtWidgets.QGridLayout()
        tmp = QtWidgets.QLabel('<b>Hemisphere</b>', self)
        sbox.addWidget(tmp,1,1)
        self.ns_label = QtWidgets.QLabel(dc.cview, self)
        sbox.addWidget(self.ns_label,2,1)
        tmp = QtWidgets.QLabel('<b>File number</b>', self)
        sbox.addWidget(tmp,1,2)
        self.filenumber_label = QtWidgets.QLabel('%d/%d' % (dc.cimageind+1,
                                                            len(dc.cgroup)), self)
        sbox.addWidget(self.filenumber_label,2,2)
        tmp = QtWidgets.QLabel('<b>Select file</b>', self)
        sbox.addWidget(tmp,1,3)
        self.filenumber_box = QtWidgets.QComboBox(self)
        self.fill_filenumberlist()
        self.filenumber_box.currentIndexChanged.connect(self.on_filenumber_changed)
        sbox.addWidget(self.filenumber_box,2,3)
        v.addLayout(sbox)        
        
        tmp = QtWidgets.QLabel('<b>Current image group</b>', self)
        v.addWidget(tmp)
        self.dataset_label = QtWidgets.QLabel(dc.cgroupfile, self)
        v.addWidget(self.dataset_label)
        
        # dataset selection
        tmp = QtWidgets.QLabel('<b>Select new image group</b>', self)
        v.addWidget(tmp)
        
        h1a = QtWidgets.QHBoxLayout(self)
        self.dataset_prev_button = QtWidgets.QPushButton('<', self)
        self.dataset_prev_button.clicked.connect(self.on_dataset_prev_button_clicked)
        h1a.addWidget(self.dataset_prev_button)  
        self.group_select = QtWidgets.QComboBox(self)
        self.fill_grouplist()
        self.group_select.currentIndexChanged.connect(self.on_group_changed)
        h1a.addWidget(self.group_select)
        self.dataset_next_button = QtWidgets.QPushButton('>', self)
        self.dataset_next_button.clicked.connect(self.on_dataset_next_button_clicked)
        h1a.addWidget(self.dataset_next_button)
        v.addLayout(h1a)              
        
        # point editor        
        tmp = QtWidgets.QLabel('<b>Saved points</b>', self)
        v.addWidget(tmp)
        tmp = QtWidgets.QLabel('LT\tColat\tTrackable\t\tLabel', self)
        v.addWidget(tmp)
        self.point_list = QtWidgets.QListWidget(self)
        v.addWidget(self.point_list)
        self.point_list.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)        
        self.fill_pointlist()
        
        h2 = QtWidgets.QHBoxLayout(self)
        self.point_show_button = QtWidgets.QPushButton('Show', self)
        self.point_show_button.clicked.connect(self.on_point_show_button_clicked)
        h2.addWidget(self.point_show_button)        
        self.point_del_button = QtWidgets.QPushButton('Delete', self)
        self.point_del_button.clicked.connect(self.on_point_del_button_clicked)
        h2.addWidget(self.point_del_button)        
        v.addLayout(h2)
                
        # adding point menu
        self.point_add_button = QtWidgets.QPushButton('Add new point', self)
        self.point_add_button.clicked.connect(self.on_point_add_button_clicked)
        v.addWidget(self.point_add_button)
        
        box1 = QtWidgets.QGridLayout()
        tmp = QtWidgets.QLabel('<b>Status</b>', self)
        box1.addWidget(tmp,1,1)
        self.point_active_label = QtWidgets.QLabel('Startup', self)
        box1.addWidget(self.point_active_label,1,2)
        
        tmp = QtWidgets.QLabel('Label', self)
        box1.addWidget(tmp,2,1)
        self.point_tag_box = QtWidgets.QComboBox(self)
        for tag in taglist:
            self.point_tag_box.addItem(np.asscalar(tag))
        box1.addWidget(self.point_tag_box,2,2)
        
        tmp = QtWidgets.QLabel('Trackable?', self)
        box1.addWidget(tmp,3,1)
        self.trackable_checkbox = QtWidgets.QCheckBox(self)
        box1.addWidget(self.trackable_checkbox,3,2)
        
        self.point_cancel_button = QtWidgets.QPushButton('Cancel', self)
        self.point_cancel_button.clicked.connect(self.on_point_cancel_button_clicked)
        box1.addWidget(self.point_cancel_button,4,1)
        self.point_finish_button = QtWidgets.QPushButton('Finish', self)
        self.point_finish_button.clicked.connect(self.on_point_finish_button_clicked)
        box1.addWidget(self.point_finish_button,4,2)
        v.addLayout(box1)
        self.point_add_inactive()
        
        v.addWidget(self.HLine())
        
        box.addLayout(v,1,2)
        
        v2 = QtWidgets.QVBoxLayout(self)
        
        # arc editor        
        tmp = QtWidgets.QLabel('<b>Saved regions/arcs</b>', self)
        v2.addWidget(tmp)
        tmp = QtWidgets.QLabel('Image\tColat\t         LT\t\tLabel', self)
        v2.addWidget(tmp)
        self.arc_list = QtWidgets.QListWidget(self)
        v2.addWidget(self.arc_list)
        self.arc_list.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)        
        self.fill_arclist()
        
        h2 = QtWidgets.QHBoxLayout(self)
        self.arc_show_button = QtWidgets.QPushButton('Show', self)
        self.arc_show_button.clicked.connect(self.on_arc_show_button_clicked)
        h2.addWidget(self.arc_show_button)        
        self.arc_del_button = QtWidgets.QPushButton('Delete', self)
        self.arc_del_button.clicked.connect(self.on_arc_del_button_clicked)
        h2.addWidget(self.arc_del_button)        
        v2.addLayout(h2)
                
        # adding arc menu
        self.arc_add_button = QtWidgets.QPushButton('Add new region/arc', self)
        self.arc_add_button.clicked.connect(self.on_arc_add_button_clicked)
        v2.addWidget(self.arc_add_button)
        
        box1 = QtWidgets.QGridLayout()
        tmp = QtWidgets.QLabel('<b>Status</b>', self)
        box1.addWidget(tmp,1,1)
        self.arc_active_label = QtWidgets.QLabel('Startup', self)
        box1.addWidget(self.arc_active_label,1,2)
        
        tmp = QtWidgets.QLabel('Label', self)
        box1.addWidget(tmp,2,1)
        self.arc_tag_box = QtWidgets.QComboBox(self)
        for tag in taglist:
            self.arc_tag_box.addItem(np.asscalar(tag))
        box1.addWidget(self.arc_tag_box,2,2)
                
        self.arc_cancel_button = QtWidgets.QPushButton('Cancel', self)
        self.arc_cancel_button.clicked.connect(self.on_arc_cancel_button_clicked)
        box1.addWidget(self.arc_cancel_button,4,1)
        self.arc_finish_button = QtWidgets.QPushButton('Finish', self)
        self.arc_finish_button.clicked.connect(self.on_arc_finish_button_clicked)
        box1.addWidget(self.arc_finish_button,4,2)
        v2.addLayout(box1)
        self.arc_add_inactive()
        
        v2.addWidget(self.HLine())
        
        box.addLayout(v2,1,3)
        
        # big fat save button
        self.save_button = QtWidgets.QPushButton('Save all data', self)
        self.save_button.clicked.connect(self.on_save_button_clicked)
        self.save_button.setMinimumHeight(2*self.save_button.minimumHeight())
        box.addWidget(self.save_button,2,2)
        
        dc.updateSignal.connect(self.update)
        
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)
        
#        self.main_widget.keyPressEvent.connect(self.on_key_pressed)
        
        self.statusBar().showMessage("Ready", 2000)
        
    def HLine(self):
        toto = QtWidgets.QFrame()
        toto.setFrameShape(QtWidgets.QFrame.HLine)
        toto.setFrameShadow(QtWidgets.QFrame.Sunken)
        return toto
    
    def VLine(self):
        toto = QtWidgets.QFrame()
        toto.setFrameShape(QtWidgets.QFrame.VLine)
        toto.setFrameShadow(QtWidgets.QFrame.Sunken)
        return toto

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtWidgets.QMessageBox.about(self, "About", """Created by Alexander Bader,
Lancaster University, December 2017.
Contact: a.bader@lancaster.ac.uk""")
        
    def update(self):
        self.pw.update_figure()
        self.file_label.setText(dc.cgroup[dc.cimageind])
        self.ns_label.setText(dc.cview)
        self.filenumber_label.setText('%d/%d' % (dc.cimageind+1, len(dc.cgroup)))
        self.filenumber_box.setCurrentIndex(dc.cimageind)
        self.dataset_label.setText(dc.cgroupfile)
        if dc.npointactive:
            self.point_add_active()
        else:
            self.point_add_inactive()
        if dc.narcactive:
            self.arc_add_active()
        else:
            self.arc_add_inactive()
        self.fill_pointlist()
        self.fill_arclist()
        self.group_select.setCurrentIndex(dc.cgroupind)
                
    def point_add_active(self):
        self.point_active_label.setText('<b><font color=%s>Active</font></b>' % hex_red)
        self.point_finish_button.setEnabled(True)
        self.point_cancel_button.setEnabled(True)
        self.point_tag_box.setEnabled(True)
        self.trackable_checkbox.setEnabled(True)
        
    def point_add_inactive(self):
        self.point_active_label.setText('<b><font color=%s>Inactive</font></b>' % hex_green)
        self.point_finish_button.setEnabled(False)
        self.point_cancel_button.setEnabled(False)
        self.point_tag_box.setEnabled(False)
        self.trackable_checkbox.setEnabled(False)
        
    def arc_add_active(self):
        self.arc_active_label.setText('<b><font color=%s>%s</font></b>' % (hex_red,dc.arcstatuslabel[dc.narcstatus]))
        self.arc_finish_button.setEnabled(True)
        if dc.narcstatus < 3:
            self.arc_finish_button.setText('Next')
        else:
            self.arc_finish_button.setText('Finish')
        self.arc_cancel_button.setEnabled(True)
        self.arc_tag_box.setEnabled(True)
        
    def arc_add_inactive(self):
        self.arc_active_label.setText('<b><font color=%s>Inactive</font></b>' % hex_green)
        self.arc_finish_button.setEnabled(False)
        self.arc_finish_button.setText('Next')
        self.arc_cancel_button.setEnabled(False)
        self.arc_tag_box.setEnabled(False)
        
    def fill_pointlist(self):
        self.point_list.clear()
        tmp = dc.pointlist
        if np.any(tmp['coords']):
            labels = tmp['labels']
            trackable = tmp['trackable']
            LT = tmp['coords'][:,dc.cimageind,0]
            LT = LT % (2*np.pi)
            LT = LT*12/np.pi
            colat = tmp['coords'][:,dc.cimageind,1]
            LT[np.where(np.isnan(LT))] = 99.99
            colat[np.where(np.isnan(colat))] = 99.99
            for iii in range(len(tmp['labels'])):
                self.point_list.addItem('%05.2f       %05.2f       %s             %s' % (LT[iii],
                                        colat[iii], trackable[iii], labels[iii]))
            if np.isfinite(dc.point_highlight):
                self.point_list.setCurrentRow(dc.point_highlight)
                
    def fill_arclist(self):
        self.arc_list.clear()
        tmp = dc.arclist
        if np.size(tmp['imgind']):
            imgind = tmp['imgind']
            labels = tmp['labels']
            coords = tmp['coords']
            fromcolat = coords[:,1]
            tocolat = coords[:,0]
            fromLT = (coords[:,2]%(2*np.pi))*12/np.pi
            toLT = (coords[:,3]%(2*np.pi))*12/np.pi
            for iii in range(len(labels)):
                self.arc_list.addItem('%02d      %05.2f-%05.2f     %05.2f-%05.2f     %s'
                                        % (imgind[iii]+1, fromLT[iii],toLT[iii], fromcolat[iii], tocolat[iii], labels[iii]))
            if np.isfinite(dc.arc_highlight):
                self.arc_list.setCurrentRow(dc.arc_highlight)

    def fill_filenumberlist(self):
        self.filenumber_box.clear()
        for iii in range(len(dc.cgroup)):
            self.filenumber_box.addItem('%s' % (iii+1))
            
    def fill_grouplist(self):
        self.group_select.clear()
        for sfn in shgroupnames[subsetlist[dc.csubsetind]]:
            self.group_select.addItem(np.asscalar(sfn))
                
    def on_prev_button_clicked(self):
        dc.prevImage()
        
    def on_next_button_clicked(self):
        dc.nextImage()
        
    def on_subset_changed(self):
        tmp = self.subset_box.currentIndex()
        dc.csubsetind = tmp
        dc.updateSubset()
        self.fill_grouplist()
        
    def on_filenumber_changed(self):
        tmp = self.filenumber_box.currentIndex()
        dc.setImageInd(tmp)
        
    def on_group_changed(self):
        tmp = self.group_select.currentIndex()
        dc.setGroupInd(tmp)
        self.fill_filenumberlist()
    
    def on_dataset_prev_button_clicked(self):
        dc.prevGroup()
        
    def on_dataset_next_button_clicked(self):
        dc.nextGroup()
        
    def on_point_show_button_clicked(self):
        arg = self.point_list.currentRow()
        if arg >= 0:
            dc.point_highlight = arg
            tmp = dc.pointlist['coords'][arg,:,:]
            tmp = np.where(np.isfinite(tmp[:,0]))[0]
            dc.setImageInd(tmp[0])
            self.update()
        
    def on_point_del_button_clicked(self):
        tmp = self.point_list.currentRow()
        dc.removePoint(tmp)
        
    def on_point_add_button_clicked(self):
        dc.point_activate()
    
    def on_point_finish_button_clicked(self):
        tmp = self.point_tag_box.currentIndex()
        trackable = self.trackable_checkbox.isChecked()
        dc.addPoint(taglist[tmp], trackable)
    
    def on_point_cancel_button_clicked(self):
        dc.point_deactivate()
          
    def on_arc_show_button_clicked(self):        
        arg = self.arc_list.currentRow()
        if arg >= 0:
            dc.arc_highlight = arg
            tmp = int(dc.arclist['imgind'][arg])
            dc.setImageInd(tmp)
            self.update()
        
    def on_arc_del_button_clicked(self):
        tmp = self.arc_list.currentRow()
        dc.removeArc(tmp)
        
    def on_arc_add_button_clicked(self):
        dc.arc_activate()
    
    def on_arc_finish_button_clicked(self):
        if dc.narcstatus<3:
            dc.arc_next()
        elif dc.narcstatus==3:
            tmp = self.arc_tag_box.currentIndex()
            dc.addArc(taglist[tmp])
    
    def on_arc_cancel_button_clicked(self):
        dc.arc_deactivate()
        
    def on_save_button_clicked(self):
        dc.saveData()
        self.statusBar().showMessage("Data saved.", 2000)
        
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Right:
            self.on_next_button_clicked()
        elif event.key() == QtCore.Qt.Key_Left:
            self.on_prev_button_clicked()
        elif event.key() == QtCore.Qt.Key_Return:
            self.on_arc_finish_button_clicked()

qApp = QtWidgets.QApplication(sys.argv)

aw = ApplicationWindow()
aw.setWindowTitle("%s, Version %s" % (progname,progversion))
aw.show()
sys.exit(qApp.exec_())
#qApp.exec_()
