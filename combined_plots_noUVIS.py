# Large plots with all necessary information
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from cycler import cycler
from datetime import datetime, timedelta
import matplotlib
import matplotlib.colors as colors
import matplotlib.gridspec as gridspec
import numpy.ma as ma
import matplotlib.patches as ptch
import matplotlib.pyplot as plt
import numpy as np
import os
from pympler import asizeof
import sys
import time
import time_conversions as tc

import psutil
process = psutil.Process(os.getpid())

matplotlib.interactive(False)

#import traceback

## Import list of timestamps
#timelist = '%s/HST-UVIS simultaneous/timelist.txt' % boxpath
#timestamps = np.array([])
#f = open(timelist, 'r')
#while True:
#    tmp = f.readline()
#    tmp2 = f.readline()
#    if tmp:
#        tmp = tmp.split('.')[0]
#        tmp = tmp.split('/')[-1]
#        tmp = tmp.split('\\')[-1]
#        timestamps = np.append(timestamps, tmp)
#    else:
#        f.close()
#        break
    
timestamps = ['2013_138T02_30_30','2014_100T02_30_30']
#timestamps = ['2014_100T02_30_30']
timeformat = '%Y_%jT%H_%M_%S'

plotpath = '%s/Plots/Dusk' % boxpath
mainsavedir = '%s/%s' % (plotpath, time.strftime('%Y%m%d%H%M%S'))
#imgsavedir = plotpath
if not os.path.exists(mainsavedir):
    os.makedirs(mainsavedir)

# Set up Cassinipy
cd = cassinipy.CassiniData(datapath)
cd.set_refframe('KRTP')
magdata = cd.cmag
rpwsdata = cd.crpws
incadata = cd.cinca
lemmsdata = cd.clemms
elsdata = cd.cels

# General settings
#cmap_UV = cubehelix.cmap(reverse=False, start=0.3, rot=-0.5)
#cmap_UV = cubehelix.cmap(reverse=False, start=0.3, rot=-0.8)
#cmap_UV = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
#cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
cmap_SPEC_r = cubehelix.cmap(reverse=True, start=1.8, rot=1, gamma=1.5)
fpcolordark = 'k'
fpcolorbright = 'white'
mydpi = 500
timeframe = 1 #h

# Constants
SATURN_R = 60268 # km

# Get UVIS and HST image times
hsttimes = np.load('%s/HST/HSTlist.npz' % datapath)['data'][()]
uvistimes = np.load('%s/UVIS/UVISlist.npz' % datapath)['data'][()]

# Magnetopause model
# Input: Dp (dynamic pressure in nPa)
def mploc(Dp):
    a1 = 9.7
    a2 = 0.24
    a3 = 0.77
    a4 = -1.5
    r0 = a1*Dp**(-a2)
    K = a3+a4*Dp
    theta = np.arange(0,170,0.1)/180*np.pi
    radius = r0*(2/(1+np.cos(theta)))**K
    [x,r] = radius*[np.cos(theta), np.sin(theta)]
    return [x,r]

for tst in timestamps:
    imgsavedir = '%s/%s' % (mainsavedir,tst)
    if not os.path.exists(imgsavedir):
        os.makedirs(imgsavedir)
    for timeframe in [1,2,6,12]:
        tmpstamp = datetime.strptime(tst, timeformat)
        newtimestamps = np.arange(tmpstamp-timedelta(hours=timeframe*15),tmpstamp+timedelta(hours=timeframe*15), timedelta(hours=timeframe))
        newtimestamps = newtimestamps.tolist()
        
    #for timeframe in [12]:
    #    starttimestamp = '2004_140T00_00_30'
    #    stoptimestamp = '2017_300T00_00_31'
    #    tmpstart = datetime.strptime(starttimestamp, timeformat)
    #    tmpstop = datetime.strptime(stoptimestamp, timeformat)
    #    newtimestamps = np.arange(tmpstart, tmpstop, timedelta(hours=12))
    #    newtimestamps = newtimestamps.tolist()
    
        for tctr in range(len(newtimestamps)):
            pytime = newtimestamps[tctr]
            print(pytime)
            ettime = tc.datetime2et(pytime)
        
            tlimet=[ettime-timeframe*3600, ettime+timeframe*3600]
            
            # Define timespan over which to plot the other stuff
            thisday = datetime(pytime.year, pytime.month, pytime.day)
            pytimemarkers = [thisday+iii*timedelta(hours=12) for iii in range(-6, 7)]
            ettimemarkers = tc.datetime2et(pytimemarkers)
            etdaymarkers = [ettimemarkers[2*iii] for iii in range(int(np.ceil(len(pytimemarkers)/2)))]
            tmp = tc.et2datetime(etdaymarkers)
            daylabels = [datetime.strftime(tmp[iii], ' DOY%j') for iii in range(len(etdaymarkers))]
            tlimet_ext = [ettimemarkers[0], ettimemarkers[-1]]
            
            print('before update')
            print(process.memory_info().rss/1e9)
                    
            # Get data
            cd.set_timeframe(tlimet)
            cd.update()
            print('after update')
            print(process.memory_info().rss/1e9)
            
                    
            print('Plot %s/%s' % (tctr+1, len(newtimestamps)))
        
            # Define subplot arrangement
            fig = plt.figure()
            fig.set_size_inches(24,12)
            fig.suptitle('%s' % datetime.strftime(pytime, '%Y-%j, %H:%M'), fontsize=20)
        
            gs1 = gridspec.GridSpec(2, 1)
            gs1.update(left=0, right=0.2)
            ax1 = plt.subplot(gs1[0, 0])
            ax2 = plt.subplot(gs1[1, 0])
        
            gs2 = gridspec.GridSpec(8, 1, height_ratios=[3,3,3,3,1,3,3,3])
            gs2.update(left=0.24, right=0.97, hspace=0)
            bx1 = plt.subplot(gs2[0, 0])
            bx2 = plt.subplot(gs2[1, 0], sharex=bx1)
            bx3 = plt.subplot(gs2[2, 0], sharex=bx1)
            bx4 = plt.subplot(gs2[3, 0], sharex=bx1)
            bx5 = plt.subplot(gs2[4, 0], sharex=bx1)
            bx6 = plt.subplot(gs2[5, 0], sharex=bx1)
            bx7 = plt.subplot(gs2[6, 0], sharex=bx1)
            bx8 = plt.subplot(gs2[7, 0], sharex=bx1)
            
            gs3 = gridspec.GridSpec(8, 1, height_ratios=[3,3,3,3,1,3,3,3])
            gs3.update(left=0.98, right=1.00, hspace=0.2)
            cx4 = plt.subplot(gs3[3, 0])
            cx5 = plt.subplot(gs3[4, 0])
            cx6 = plt.subplot(gs3[5, 0])
            cx8 = plt.subplot(gs3[7, 0])
            gs4 = gridspec.GridSpec(2, 1)
            print('after setting up figure')
            print(process.memory_info().rss/1e9)
               
    #        # Plot y-z
    #        tmp = cd.get_locations([ettime], 'KSM')
    #        ax1.scatter(tmp[2], tmp[3], s=40, marker='D', color='r', zorder=5)
    #        # Trajectory
    #        [tmin, tmax] = tlimet_ext
    #        step = 1000
    #        times = [xxx*(tmax-tmin)/step + tmin for xxx in range(step)]
    #        tmp = cd.get_locations(times, 'KSM')
    #        ax1.plot(tmp[2], tmp[3], color='k')
    #        # Plot dots and day markers
    #        tmp = cd.get_locations(ettimemarkers, 'KSM')
    #        ax1.scatter(tmp[2], tmp[3], color='k')
    #        tmp = cd.get_locations(etdaymarkers, 'KSM')
    #        for jjj in range(len(etdaymarkers)):
    #            ax1.text(tmp[1][jjj], tmp[3][jjj], daylabels[jjj], color=fpcolordark)
    #        ax1.set_xlim([-60,60])
    #        ax1.set_ylim([-60,60])
    #        ax1.add_patch(ptch.Wedge((0,0), 1, 0, 360, fill=True, color='0.7'))
    #        ax1.set_aspect('equal')
    #        ax1.set_xlabel('$Y_{KSM}$')
    #        ax1.set_ylabel('$Z_{KSM}$')
    #        ax1.grid()
            
            # Plot x-y
            tmp = cd.get_locations([ettime], 'KSM')
            ax1.scatter(tmp[1], tmp[2], s=40, marker='D', color='r', zorder=5)
            # Trajectory
            [tmin, tmax] = tlimet_ext
            step = 1000
            times = [xxx*(tmax-tmin)/step + tmin for xxx in range(step)]
            tmp = cd.get_locations(times, 'KSM')
            ax1.plot(tmp[1], tmp[2], color='k')
            # Plot dots and day markers
            tmp = cd.get_locations(ettimemarkers, 'KSM')
            ax1.scatter(tmp[1], tmp[2], color='k')
            tmp = cd.get_locations(etdaymarkers, 'KSM')
            for jjj in range(len(etdaymarkers)):
                ax1.text(tmp[1][jjj], tmp[2][jjj], daylabels[jjj], color=fpcolordark)
        
            ax1.set_xlim([-60,60])
            ax1.set_ylim([-60,60])
            [xmp, rmp] = mploc(0.01)
            ax1.plot(xmp,rmp,'k-')
            ax1.plot(xmp,-rmp,'k-')
            [xmp, rmp] = mploc(0.1)
            ax1.plot(xmp,rmp,'k-')
            ax1.plot(xmp,-rmp,'k-')
            ax1.fill_between(xmp,0,rmp, facecolor='blue', alpha=0.1)
            ax1.fill_between(xmp,-rmp,0,facecolor='blue', alpha=0.1)
            ax1.set_aspect('equal')
            ax1.set_xlabel('$X_{KSM}$')
            ax1.set_ylabel('$Y_{KSM}$')
            ax1.grid()
            ax1.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7', zorder=5))
            ax1.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0', zorder=5))
            
            print('after ax1')
            print(process.memory_info().rss/1e9)
    
            # Plot x-z
            tmp = cd.get_locations([ettime], 'KSM')
            ax2.scatter(tmp[1], tmp[3], s=40, marker='D', color='r', zorder=5)
            # Trajectory
            [tmin, tmax] = tlimet_ext
            step = 1000
            times = [xxx*(tmax-tmin)/step + tmin for xxx in range(step)]
            tmp = cd.get_locations(times, 'KSM')
            ax2.plot(tmp[1], tmp[3], color='k')
            # Plot dots and day markers
            tmp = cd.get_locations(ettimemarkers, 'KSM')
            ax2.scatter(tmp[1], tmp[3], color='k')
            tmp = cd.get_locations(etdaymarkers, 'KSM')
            for jjj in range(len(etdaymarkers)):
                ax2.text(tmp[1][jjj], tmp[3][jjj], daylabels[jjj], color=fpcolordark)
        
            ax2.set_xlim([-60,60])
            ax2.set_ylim([-60,60])
            [xmp, rmp] = mploc(0.01)
            ax2.plot(xmp,rmp,'k-')
            ax2.plot(xmp,-rmp,'k-')
            [xmp, rmp] = mploc(0.1)
            ax2.plot(xmp,rmp,'k-')
            ax2.plot(xmp,-rmp,'k-') 
            ax2.set_aspect('equal')
            ax2.set_xlabel('$X_{KSM}$')
            ax2.set_ylabel('$Z_{KSM}$')
            ax2.grid()
            ax2.plot([0,0],[-5,5], c='k', lw=0.5, zorder=4)
            ax2.plot([-60,xmp[np.where(rmp>1)[0][0]-5]],[0,0], c='blue', alpha=0.1, lw=5)
            ax2.add_patch(ptch.Wedge((0,0), 1, -90, 90, fill=True, color='0.7', zorder=5))
            ax2.add_patch(ptch.Wedge((0,0), 1, 90, 270, fill=True, color='0', zorder=5))
            
            print('after ax2')
            print(process.memory_info().rss/1e9)
        
            # Plot B field in KRTP
            if np.any(magdata.ettimes):
                bx1.plot(magdata.ettimes, magdata.data[:,0], c='blue', lw=1, label=r'$B_r$')
            bx1.set_ylabel(r'$B_{R}$ (nT)')
            
            if np.any(magdata.ettimes):
                bx2.plot(magdata.ettimes, magdata.data[:,1], c='green', lw=1, label=r'$B_\theta$')
            bx2.set_ylabel(r'$B_{T}$ (nT)')
            
            if np.any(magdata.ettimes):
                bx3.plot(magdata.ettimes, magdata.data[:,2], c='red', lw=1, label=r'$B_\phi$')
            bx3.set_ylabel(r'$B_{P}$ (nT)')
            
            print('after bx1-bx3')
            print(process.memory_info().rss/1e9)
                
    #        # Plot perpendicular ELS spectrum
    #        if np.any(elsdata.dist_ppa):
    #            data = np.transpose(elsdata.dist_ppa[:,1,:])
    #            data = ma.masked_invalid(data)
    #            ppp = bx4.pcolormesh(elsdata.ettimes, elsdata.energies, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=1e8, vmax=1e11))
    #            ppp.cmap.set_bad('gray')
    #            cbar = plt.colorbar(ppp, cax=cx4)
    #            cbar.set_label(r'e$^-$ energy flux (eV/m$^2$/s/sr/eV)', rotation=270, labelpad=10)            
    #        bx4.set_ylim([0.8,2.5e4])
    #        bx4.set_yscale('log')
    #        bx4.set_ylabel('E (eV)')
            
            # Plot ELS spectrum anode 4
            if np.any(elsdata.dist_ppa):
                data = np.transpose(elsdata.data[:,:,3])
                data = ma.masked_invalid(data)
                ppp = bx4.pcolormesh(elsdata.ettimes, elsdata.energies, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=1e8, vmax=1e11))
                ppp.cmap.set_bad('gray')
                cbar = plt.colorbar(ppp, cax=cx4)
                cbar.set_label(r'e$^-$ E flux' + '\n' + r'(eV/m$^2$/s/sr/eV)', rotation=270, labelpad=25)            
            bx4.set_ylim([0.8,2.5e4])
            bx4.set_yscale('log')
            bx4.set_ylabel('E (eV)')
        
            # Plot ELS pitch angle distribution
            if np.any(elsdata.pa_dist):
                data = np.transpose(elsdata.pa_dist)
                data = ma.masked_invalid(data)
                ppp = bx5.pcolormesh(elsdata.ettimes, elsdata.pa_bins, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=3e12, vmax=5e13))
                ppp.cmap.set_bad('gray')
                cbar = plt.colorbar(ppp, cax=cx5)
                cbar.set_label(r'e$^-$ E flux' + '\n' + r'(eV/m$^2$/s/sr)', rotation=270, labelpad=40)
            bx5.set_ylim([0,180])
            bx5.set_yticks([0,90,180])
            bx5.set_ylabel('PA (deg)')
            
            print('after bx4-bx5')
            print(process.memory_info().rss/1e9)
        
            # Plot RPWS spectrum
            if np.any(rpwsdata.data):
                data = np.transpose(rpwsdata.data)
                data = ma.masked_invalid(data)
                ppp = bx6.pcolormesh(rpwsdata.ettimes, rpwsdata.freq, data, cmap=cmap_SPEC, vmin=0, vmax=35)
                ppp.cmap.set_bad('gray')
                cbar = plt.colorbar(ppp, cax=cx6)
                cbar.set_label(r'dB over BG', rotation=270, labelpad=20)
            bx6.set_ylim([1.1,1.5e7])
            bx6.set_yscale('log')
            bx6.set_ylabel('f (Hz)')  
            
            print('after bx6')
            print(process.memory_info().rss/1e9)
        
            # Plot INCA ion fluxes
            if np.any(incadata.data):
                mycycler = cycler('color', ['red', 'coral', 'orange', 'gold', 'yellowgreen', 'turquoise', 'steelblue', 'blueviolet'])
                bx7.set_prop_cycle(mycycler)
                for iii in range(len(incadata.labels)):
                    tmp = bx7.plot(incadata.ettimes, incadata.data[:,iii], lw=1.5)
                    bx7.text(1.02, 0.1+iii*0.1, incadata.labels[iii], color=tmp[0].get_color(), transform=bx7.transAxes)
            bx7.set_yscale('log')
            bx7.set_ylabel(r'Ion flux' + '\n' + r'(1/cm$^2$/s/sr/keV)')
            
            print('after bx7')
            print(process.memory_info().rss/1e9)
        
            # Hide x-axis labels
            plt.setp(bx1.get_xticklabels(), visible=False)
            plt.setp(bx2.get_xticklabels(), visible=False)
            plt.setp(bx3.get_xticklabels(), visible=False)
            plt.setp(bx4.get_xticklabels(), visible=False)
            plt.setp(bx5.get_xticklabels(), visible=False)
            plt.setp(bx6.get_xticklabels(), visible=False)
            plt.setp(bx7.get_xticklabels(), visible=False)
        
            # Plot LEMMS spectrum
            if np.any(lemmsdata.data):
                data = np.transpose(lemmsdata.data)
                data = ma.masked_invalid(data)
                ppp = bx8.pcolormesh(lemmsdata.ettimes, lemmsdata.energies, data, cmap=cmap_SPEC, norm=colors.LogNorm(vmin=1e-1, vmax=1e3))
                ppp.cmap.set_bad('gray')
                bx8.set_ylim([20,1750])
                cbar = plt.colorbar(ppp, cax=cx8)
                cbar.set_label(r'e$^-$ flux' + '\n' + r'(1/cm$^2$/s/sr/keV)', rotation=270, labelpad=30)
            bx8.set_yscale('log')
            bx8.set_ylabel('E (keV)')       
            
            print('after bx8')
            print(process.memory_info().rss/1e9)
        
            # Put proper x-axis labels
            pytlim = tc.et2datetime(tlimet)
            numticks = 7
            delta = (pytlim[1] - pytlim[0])/(numticks-1)
            ticklist = np.array([pytlim[0]+(iii+0.5)*delta for iii in range(numticks-1)])
            ticklistet = tc.datetime2et(ticklist)
                
            thisLOC = cd.get_locations(ticklistet, 'KRTP')
            ticlab = ['%s\n%.1f\n%.1f\n%.1f' % (datetime.strftime(ticklist[iii],'%j/%H:%M'),
                                                thisLOC[1][iii],
                                                thisLOC[2][iii],
                                                thisLOC[3][iii]) for iii in range(len(ticklistet))]
            bx8.set_xticks(ticklistet)
            bx8.set_xticklabels(ticlab)
        
            # Label description at the right
            ticklab = bx8.xaxis.get_ticklabels()[0]
            trans = ticklab.get_transform()
            bx8.xaxis.set_label_coords(tlimet[0]-timeframe/16*3600, 0, transform=trans)
            bx8.set_xlabel('DOY/HH:MM\n r (RS) \n el (deg)\n LT (h)')
            bx8.set_xlim(tlimet)
            
            print('after x labels')
            print(process.memory_info().rss/1e9)
        
            # Vertical colored areas indicating UVIS (red) and HST (green) images
            tmplist = [uvistimes, hsttimes]
            tmpcolor = ['r', 'g']
            for tmpctr in range(2):
                tmp1 = np.where(np.logical_and(tmplist[tmpctr]['ET_START_STOP'][:,0]>tlimet[0], tmplist[tmpctr]['ET_START_STOP'][:,0]<tlimet[1]))
                tmp2 = np.where(np.logical_and(tmplist[tmpctr]['ET_START_STOP'][:,1]>tlimet[0], tmplist[tmpctr]['ET_START_STOP'][:,1]<tlimet[1]))
                argtmp = np.unique(np.append(tmp1, tmp2))
                for argctr in argtmp:
                    if tmpctr==0 and tmplist[tmpctr]['EXP'][argctr]<400:
                        continue
                    thismin = np.max([tlimet[0], tmplist[tmpctr]['ET_START_STOP'][argctr,0]])
                    thismax = np.min([tlimet[1], tmplist[tmpctr]['ET_START_STOP'][argctr,1]])
                    for axctr in [bx1,bx2,bx3,bx7]:
                        axctr.axvspan(thismin, thismax, alpha=0.3, color=tmpcolor[tmpctr])
                    bx8.axvline(x=thismin, ymin=0, ymax=7.33, c=tmpcolor[tmpctr],
                                   linestyle='--', lw=1, zorder=2, clip_on=False)
                    bx8.axvline(x=thismax, ymin=0, ymax=7.33, c=tmpcolor[tmpctr],
                                   linestyle='--', lw=1, zorder=2, clip_on=False)
            
            print('after overlaying UVIS and HST')
            print(process.memory_info().rss/1e9)
                        
            # Save directory
            fullimgsavedir = '%s/%dhr' % (imgsavedir, timeframe)
            if not os.path.exists(fullimgsavedir):
                os.mkdir(fullimgsavedir)
                
            print('after creating save folder')
            print(process.memory_info().rss/1e9)
                            
            # Save
            plt.savefig('%s/%s_%s.jpg' % (fullimgsavedir, datetime.strftime(pytime, '%Y-%j_%H%M'), int(timeframe)), bbox_inches='tight', dpi=mydpi)
    #        plt.show()
            print('after saving figure')
            print(process.memory_info().rss/1e9)
            plt.close()
            
            print('after closing figure')
            print(process.memory_info().rss/1e9)
            