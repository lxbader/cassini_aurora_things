from __future__ import unicode_literals

import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

from astropy.io import fits
import cubehelix
from datetime import datetime
# For use outside of spyder:
#import matplotlib
#matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import os
from PyQt5 import QtCore, QtWidgets

progname = "UVIS Image Tagger"
progversion = "0.1"

# define some colors
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5,
                         minLight=0.15, gamma=1.5)
hex_red = '#e74c3c'
hex_green = '#16a085' 

# define data paths
fitspath = '%s/Fits/All' % datapath
listpath = '%s/full_set' % imglistpath

# which subsets and tags are available
subsetlist = np.array(['all', 'low_latitude'])
taglist = np.array(['none', 'main', 'poleward', 'equatorward'])

# import info file about image groups
ext_npzfile = np.load('%s/group_info.npz' % listpath)
ext_groupdata = ext_npzfile['groupdata'][()]
ext_labellist = ext_npzfile['labellist'][()]

# create group lists for each subset
grouplists = {}
groupnames = {}
grouptimes = {}
shgroupnames = {}
for iii in subsetlist:
    grouplists[iii] = ext_groupdata['name'][np.where(ext_groupdata[iii])]
    grouptimes[iii] = np.array([datetime.strptime(jjj, '%Y-%jT%H_%M_%S')
                            for jjj in grouplists[iii]])
    shgroupnames[iii] = np.array([datetime.strftime(jjj, '%Y-%m-%dT%H:%M')
                            for jjj in grouptimes[iii]])
    grouplists[iii] = np.array(['%s/%s.txt' % (listpath, jjj) for jjj in grouplists[iii]])

# turns filename of a UVIS file into an absolute path
def makePath(imgfile):
    return '%s/%s' % (fitspath, imgfile)

# manages displayed image and points of the current group
class DataContainer(QtCore.QObject):
    updateSignal = QtCore.pyqtSignal()
    csubset = []
    cgroupfile = []
    cgroup = []
    cimage = []
    csubsetind = 0
    cgroupind = 0
    cimageind = 0
    cview = 'None'
    
    cdatafile = []
    pointlist = {}
    highlight = np.nan
    
    npointcoords = []
    npointactive = False
    
    def __init__(self):
        QtCore.QObject.__init__(self)
        self.updateSubset()
        
    def updateSubset(self):
        self.csubset = grouplists[subsetlist[self.csubsetind]]
        self.updateGroup()
        
    def updateGroup(self):
        self.deactivate()
        if np.any(self.pointlist):
            self.savePoints()
        self.highlight = np.nan
        self.cimageind = 0
        self.cgroupfile = self.csubset[self.cgroupind]
        self.cdatafile = '%s_data.npz' % self.cgroupfile.split('.')[0]
        self.cgroup = np.array([])
        f = open(self.cgroupfile, 'r')
        while True:
            tmp = f.readline()
            if tmp:
                self.cgroup = np.append(self.cgroup, tmp.split('\n')[0])
            else:
                f.close()
                break
        self.loadPoints()
        self.updateImage()
            
    def updateImage(self):
        hdulist = fits.open(makePath(self.cgroup[self.cimageind]))
        hdu = hdulist[0]
        self.cimage = hdu.data
        if np.any(self.cimage[0:60,:]>0.1):
            self.cview = 'South'
        else:
            self.cimage = self.cimage[::-1]
            self.cview = 'North'
        self.updateSignal.emit()
        
    def setGroupInd(self, ind):
        self.cgroupind = ind
        self.updateGroup()
        
    def nextGroup(self):
        maxind = len(self.csubset)-1
        if self.cgroupind == maxind:
            pass
        else:
            self.setGroupInd(self.cgroupind+1)
    
    def prevGroup(self):
        minind = 0
        if self.cgroupind == minind:
            pass
        else:
            self.setGroupInd(self.cgroupind-1)
    
    def setImageInd(self, ind):
        self.cimageind = ind
        self.updateImage()
        
    def nextImage(self):
        maxind = len(self.cgroup)-1
        if self.cimageind == maxind:
            pass
        else:
            self.setImageInd(self.cimageind+1)
    
    def prevImage(self):
        minind = 0
        if self.cimageind == minind:
            pass
        else:
            self.setImageInd(self.cimageind-1)
    
    def savePoints(self):
        np.savez(self.cdatafile, pointlist=self.pointlist)
    
    def loadPoints(self):
        if os.path.isfile(self.cdatafile):
            npzfile = np.load(self.cdatafile)
            self.pointlist = npzfile['pointlist'][()]
        else:
            self.resetPoints()
            
    def resetPoints(self):
        self.pointlist['group'] = self.cgroup
        self.pointlist['exp'] = np.full((len(self.cgroup)), np.nan)
        self.pointlist['hemisphere'] = np.full((len(self.cgroup)), 'X')
        for iii in range(len(self.cgroup)):
            try:
                fff = makePath(self.cgroup[iii])
                hdulist = fits.open(fff)
                hdu = hdulist[0]
                self.pointlist['exp'][iii] = hdu.header['TOTALEXP']
                tmp = hdu.data
                if np.any(tmp[0:60,:]>0.1):
                    self.pointlist['hemisphere'][iii] = 'S'
                else:
                    self.pointlist['hemisphere'][iii] = 'N'
            except:
                pass
        self.pointlist['coords'] = np.full((0, len(self.cgroup), 2), np.nan)
        self.pointlist['labels'] = np.full((0), np.nan)
        self.pointlist['trackable'] = np.full((0), np.nan, dtype=np.bool)
        
    # activating adding of points
    def activate(self):
        self.npointcoords = np.full((len(self.cgroup), 2), np.nan)
        self.npointactive = True
        self.updateSignal.emit()
    
    # ... and deactivating
    def deactivate(self):
        self.npointcoords = np.array([])
        self.npointactive = False
        self.updateSignal.emit()
            
    def addPoint(self, label, trackable):
        if np.any(self.npointcoords):
            self.pointlist['coords'] = np.append(self.pointlist['coords'],
                                                  [self.npointcoords],axis=0)
            self.pointlist['labels'] = np.append(self.pointlist['labels'],
                                                  [label],axis=0)
            self.pointlist['trackable'] = np.append(self.pointlist['trackable'],
                                                  [trackable],axis=0)
            self.updateSignal.emit()
        
    def removePoint(self, ind):
        if ind>0:
            self.pointlist['coords'] = np.delete(self.pointlist['coords'],
                                                  ind,axis=0)
            self.pointlist['labels'] = np.delete(self.pointlist['labels'],
                                                  ind,axis=0)
            self.pointlist['trackable'] = np.delete(self.pointlist['trackable'],
                                                  ind,axis=0)
            self.highlight = np.nan
        elif ind==0:
            self.resetPoints()
            self.highlight = np.nan
        self.updateSignal.emit()
        
dc = DataContainer()
           
# widget displaying the current UVIS image and the marked points
class PlotWidget(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111, projection='polar')
        
        self.plot_figure()

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)
        
    def plot_figure(self):        
        theta = np.array(range(720))*.5
        theta = np.radians(theta)
        r = np.array(range(360))*.5
        levels = np.linspace(0.1, 4, 40)
    
        p = self.axes.contourf(theta, r, dc.cimage, levels,
                               cmap=cmap_UV, extend='both')
        p.cmap.set_under('k')
        p.cmap.set_over('orangered')
        p.set_clim(0, 4)
        
        if dc.npointactive:
            self.axes.scatter(dc.npointcoords[dc.cimageind,0],
                              dc.npointcoords[dc.cimageind,1],
                              color='gold', marker='x', s=25, zorder=5)
        if np.size(dc.pointlist['labels']):
            self.axes.scatter(dc.pointlist['coords'][:,dc.cimageind,0],
                              dc.pointlist['coords'][:,dc.cimageind,1],
                              color='k', marker='x', s=25, zorder=5)
        if np.isfinite(dc.highlight):
            self.axes.scatter(dc.pointlist['coords'][dc.highlight,dc.cimageind,0],
                              dc.pointlist['coords'][dc.highlight,dc.cimageind,1],
                              edgecolor='lime', marker='o', facecolor='None', s=80)        
    
        self.axes.set_theta_zero_location("N")
        self.axes.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
        self.axes.set_xticklabels(['00','06','12','18'])
        self.axes.set_yticks([10,20,30])
        self.axes.set_yticklabels([])
        self.axes.grid('on', color='w', linewidth=2)
        self.axes.set_rmax(30)
        self.axes.set_rmin(0)
        
    def update_figure(self):
        self.axes.cla()
        self.plot_figure()
        self.draw()
        
    def on_click(self, event):
        if dc.npointactive:
            dc.npointcoords[dc.cimageind,:] = [event.xdata, event.ydata]
            self.update_figure()
        
# main window, organizing layout and functionality     
class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("%s, Version %s" % (progname,progversion))
        
        # menu bar on top
        self.file_menu = QtWidgets.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)
                    
        self.help_menu = QtWidgets.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)
        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtWidgets.QWidget(self)
        box = QtWidgets.QGridLayout(self.main_widget)
        
        #------------
        # main plot
        self.pw = PlotWidget(self.main_widget)
        box.addWidget(self.pw,1,1)
        
        #----------------------
        # prev/next buttons
        h1 = QtWidgets.QHBoxLayout(self)        
        self.prev_button = QtWidgets.QPushButton('<Previous', self)
        self.prev_button.clicked.connect(self.on_prev_button_clicked)
        h1.addWidget(self.prev_button)                
        self.next_button = QtWidgets.QPushButton('Next>', self)
        self.next_button.clicked.connect(self.on_next_button_clicked)
        h1.addWidget(self.next_button)
        box.addLayout(h1,2,1)
        
        #----------------------------
        # info and menu
        v = QtWidgets.QVBoxLayout(self)
        
        # subset selection
        tmp = QtWidgets.QLabel('<b>Select subset</b>', self)
        v.addWidget(tmp)
        self.subset_box = QtWidgets.QComboBox(self)
        for iii in subsetlist:
            self.subset_box.addItem(iii)
        self.subset_box.currentIndexChanged.connect(self.on_subset_changed)
        v.addWidget(self.subset_box)        
        
        # some labels
        tmp = QtWidgets.QLabel('<b>Current file</b>', self)
        v.addWidget(tmp)
        self.file_label = QtWidgets.QLabel(dc.cgroup[dc.cimageind], self)
        v.addWidget(self.file_label) 
        
        sbox = QtWidgets.QGridLayout()
        tmp = QtWidgets.QLabel('<b>Hemisphere</b>', self)
        sbox.addWidget(tmp,1,1)
        self.ns_label = QtWidgets.QLabel(dc.cview, self)
        sbox.addWidget(self.ns_label,2,1)
        tmp = QtWidgets.QLabel('<b>File number</b>', self)
        sbox.addWidget(tmp,1,2)
        self.filenumber_label = QtWidgets.QLabel('%d/%d' % (dc.cimageind+1,
                                                            len(dc.cgroup)), self)
        sbox.addWidget(self.filenumber_label,2,2)
        tmp = QtWidgets.QLabel('<b>Select file</b>', self)
        sbox.addWidget(tmp,1,3)
        self.filenumber_box = QtWidgets.QComboBox(self)
        self.fill_filenumberlist()
        self.filenumber_box.currentIndexChanged.connect(self.on_filenumber_changed)
        sbox.addWidget(self.filenumber_box,2,3)
        v.addLayout(sbox)        
        
        tmp = QtWidgets.QLabel('<b>Current image group</b>', self)
        v.addWidget(tmp)
        self.dataset_label = QtWidgets.QLabel(dc.cgroupfile, self)
        v.addWidget(self.dataset_label)
        
        # dataset selection
        tmp = QtWidgets.QLabel('<b>Select new image group</b>', self)
        v.addWidget(tmp)
        
        h1a = QtWidgets.QHBoxLayout(self)
        self.dataset_prev_button = QtWidgets.QPushButton('<', self)
        self.dataset_prev_button.clicked.connect(self.on_dataset_prev_button_clicked)
        h1a.addWidget(self.dataset_prev_button)  
        self.group_select = QtWidgets.QComboBox(self)
        self.fill_grouplist()
        self.group_select.currentIndexChanged.connect(self.on_group_changed)
        h1a.addWidget(self.group_select)
        self.dataset_next_button = QtWidgets.QPushButton('>', self)
        self.dataset_next_button.clicked.connect(self.on_dataset_next_button_clicked)
        h1a.addWidget(self.dataset_next_button)
        v.addLayout(h1a)              
        
        # point editor        
        tmp = QtWidgets.QLabel('<b>Saved points</b>', self)
        v.addWidget(tmp)
        tmp = QtWidgets.QLabel('Num\tLT\tColat     Trackable    Label', self)
        v.addWidget(tmp)
        self.point_list = QtWidgets.QListWidget(self)
        v.addWidget(self.point_list)
        self.point_list.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)        
        self.fill_pointlist()
        
        h2 = QtWidgets.QHBoxLayout(self)
        self.show_button = QtWidgets.QPushButton('Show', self)
        self.show_button.clicked.connect(self.on_show_button_clicked)
        h2.addWidget(self.show_button)        
        self.del_button = QtWidgets.QPushButton('Delete', self)
        self.del_button.clicked.connect(self.on_del_button_clicked)
        h2.addWidget(self.del_button)        
        v.addLayout(h2)
        
        # adding menu
        self.add_button = QtWidgets.QPushButton('Add new point', self)
        self.add_button.clicked.connect(self.on_add_button_clicked)
        v.addWidget(self.add_button)
        
        addbox = QtWidgets.QGridLayout()
        tmp = QtWidgets.QLabel('<b>Status</b>', self)
        addbox.addWidget(tmp,1,1)
        self.active_label = QtWidgets.QLabel('Startup', self)
        addbox.addWidget(self.active_label,1,2)
        
        tmp = QtWidgets.QLabel('Label', self)
        addbox.addWidget(tmp,2,1)
        self.tagbox = QtWidgets.QComboBox(self)
        for tag in taglist:
            self.tagbox.addItem(np.asscalar(tag))
        addbox.addWidget(self.tagbox,2,2)
        
        tmp = QtWidgets.QLabel('Trackable?', self)
        addbox.addWidget(tmp,3,1)
        self.trackable_checkbox = QtWidgets.QCheckBox(self)
        addbox.addWidget(self.trackable_checkbox,3,2)
        
        self.cancel_button = QtWidgets.QPushButton('Cancel', self)
        self.cancel_button.clicked.connect(self.on_cancel_button_clicked)
        addbox.addWidget(self.cancel_button,4,1)
        self.finish_button = QtWidgets.QPushButton('Finish', self)
        self.finish_button.clicked.connect(self.on_finish_button_clicked)
        addbox.addWidget(self.finish_button,4,2)
        v.addLayout(addbox)
        self.add_inactive()
        
        box.addLayout(v,1,2)
        
        # big fat save button
        self.save_button = QtWidgets.QPushButton('Save all points', self)
        self.save_button.clicked.connect(self.on_save_button_clicked)
        self.save_button.setMinimumHeight(2*self.save_button.minimumHeight())
        box.addWidget(self.save_button,2,2)
        
        dc.updateSignal.connect(self.update)
        
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.statusBar().showMessage("Ready", 2000)

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtWidgets.QMessageBox.about(self, "About", """Created by Alexander Bader,
Lancaster University, December 2017.
Contact: a.bader@lancaster.ac.uk""")
        
    def update(self):
        self.pw.update_figure()
        self.file_label.setText(dc.cgroup[dc.cimageind])
        self.ns_label.setText(dc.cview)
        self.filenumber_label.setText('%d/%d' % (dc.cimageind+1, len(dc.cgroup)))
        self.filenumber_box.setCurrentIndex(dc.cimageind)
        self.dataset_label.setText(dc.cgroupfile)
        if dc.npointactive:
            self.add_active()
        else:
            self.add_inactive()
        self.fill_pointlist()
        self.group_select.setCurrentIndex(dc.cgroupind)
                
    def add_active(self):
        self.active_label.setText('<b><font color=%s>Active</font></b>' % hex_red)
        self.finish_button.setEnabled(True)
        self.cancel_button.setEnabled(True)
        self.tagbox.setEnabled(True)
        self.trackable_checkbox.setEnabled(True)
        
    def add_inactive(self):
        self.active_label.setText('<b><font color=%s>Inactive</font></b>' % hex_green)
        self.finish_button.setEnabled(False)
        self.cancel_button.setEnabled(False)
        self.tagbox.setEnabled(False)
        self.trackable_checkbox.setEnabled(False)
        
    def fill_pointlist(self):
        self.point_list.clear()
        tmp = dc.pointlist
        if np.any(tmp):
            labels = tmp['labels']
            trackable = tmp['trackable']
            LT = tmp['coords'][:,dc.cimageind,0]
            LT = LT % (2*np.pi)
            LT = LT*12/np.pi
            colat = tmp['coords'][:,dc.cimageind,1]
            LT[np.where(np.isnan(LT))] = 99.99
            colat[np.where(np.isnan(colat))] = 99.99
            for iii in range(len(tmp['labels'])):
                self.point_list.addItem('%02d        %05.2f         %05.2f       %s        %s' % (iii, LT[iii],
                                        colat[iii], trackable[iii], labels[iii]))
            if np.isfinite(dc.highlight):
                self.point_list.setCurrentRow(dc.highlight)

    def fill_filenumberlist(self):
        self.filenumber_box.clear()
        for iii in range(len(dc.cgroup)):
            self.filenumber_box.addItem('%s' % (iii+1))
            
    def fill_grouplist(self):
        self.group_select.clear()
        for sfn in shgroupnames[subsetlist[dc.csubsetind]]:
            self.group_select.addItem(np.asscalar(sfn))
                
    def on_prev_button_clicked(self):
        dc.prevImage()
        
    def on_next_button_clicked(self):
        dc.nextImage()
        
    def on_subset_changed(self):
        tmp = self.subset_box.currentIndex()
        dc.csubsetind = tmp
        dc.updateSubset()
        self.fill_grouplist()
        
    def on_filenumber_changed(self):
        tmp = self.filenumber_box.currentIndex()
        dc.setImageInd(tmp)
        
    def on_group_changed(self):
        tmp = self.group_select.currentIndex()
        dc.setGroupInd(tmp)
        self.fill_filenumberlist()
    
    def on_dataset_prev_button_clicked(self):
        dc.prevGroup()
        
    def on_dataset_next_button_clicked(self):
        dc.nextGroup()
        
    def on_show_button_clicked(self):
        tmp = self.point_list.currentRow()
        dc.highlight = tmp
        self.update()
        
    def on_add_button_clicked(self):
        dc.activate()
        
    def on_del_button_clicked(self):
        tmp = self.point_list.currentRow()
        dc.removePoint(tmp)
    
    def on_finish_button_clicked(self):
        tmp = self.tagbox.currentIndex()
        trackable = self.trackable_checkbox.isChecked()
        dc.addPoint(taglist[tmp], trackable)
        dc.deactivate()
    
    def on_cancel_button_clicked(self):
        dc.deactivate()
        
    def on_save_button_clicked(self):
        dc.savePoints()
        self.statusBar().showMessage("Points saved.", 2000)

qApp = QtWidgets.QApplication(sys.argv)

aw = ApplicationWindow()
aw.setWindowTitle("%s, Version %s" % (progname,progversion))
aw.show()
sys.exit(qApp.exec_())
#qApp.exec_()
