import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
imglistpath = pr.imglistpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

from datetime import datetime, timedelta
import glob
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as sopt

def rotfit(dt, a):
    return a * 2*np.pi*dt/SATURN_T

# rotation period of Saturn [s]
SATURN_T = 10.66*3600
SIGMA = 30/180*np.pi

# define data paths
listpath = '%s/full_set' % imglistpath

filelist = glob.glob('%s/*_data.npz' % listpath)

regions = ['equatorward', 'main', 'poleward']
names = ['equatorward', 'main oval', 'poleward']

for rrr in range(len(regions)):

    dt = np.array([])
    dphi = np.array([])
    
    for filectr in range(len(filelist)):
        fff = filelist[filectr]
        
        npzfile = np.load(fff)
        cpointlist = npzfile['pointlist'][()]    
        cimgtimes = [datetime.strptime(cpointlist['group'][iii], '%Y-%jT%H_%M_%S.fits')
                                            for iii in range(len(cpointlist['group']))]    
        cimgtimes = [cimgtimes[iii]+timedelta(seconds=cpointlist['exp'][iii]/2)
                                            for iii in range(len(cpointlist['exp']))]    
        ccoords = cpointlist['coords']
        clabels = cpointlist['labels']
        ctrack = cpointlist['trackable']   
        
        for pctr in range(len(clabels)):
            if clabels[pctr]==regions[rrr] and ctrack[pctr]:
                argvalid = np.where(np.isfinite(ccoords[pctr,:,0]))[0]
                
                # calculate all possible deltas
                for iii in range(len(argvalid)):
#                for iii in range(0,1):
                    for jjj in range(iii+1, len(argvalid)):
#                    for jjj in range(len(argvalid)-1, len(argvalid)):
                        
                        dt_tmp = cimgtimes[argvalid[jjj]]-cimgtimes[argvalid[iii]]
                        dt_tmp = dt_tmp.total_seconds()
                        
                        maxangle_tmp = ccoords[pctr,argvalid[jjj],0]
                        minangle_tmp = ccoords[pctr,argvalid[iii],0]
                        if not maxangle_tmp >= minangle_tmp:
                            maxangle_tmp += 2*np.pi
                        dphi_tmp = maxangle_tmp-minangle_tmp
                        
                        if dphi_tmp >= np.pi:
                            continue
                        
                        dt = np.append(dt, [dt_tmp])
                        dphi = np.append(dphi, [dphi_tmp])
    
    opt = sopt.curve_fit(rotfit, dt, dphi, sigma=np.full((len(dt)),SIGMA), absolute_sigma=True)
    lin_dt = np.arange(np.max(dt))
    
    plt.figure()
    ax = plt.subplot(111)
    ax.scatter(dt,dphi*180/np.pi, marker='+', color='0')
    ax.plot(lin_dt, rotfit(lin_dt,1)*180/np.pi, label='Rigid corotation 10hr 40min',
            ls='--', lw=1, color='k')
    ax.plot(lin_dt, rotfit(lin_dt,0.5)*180/np.pi, label='50% rigid corotation',
            ls='-.', lw=1, color='k')
    ax.plot(lin_dt, rotfit(lin_dt,opt[0])*180/np.pi, label='Fit = %.1f%% corotation'
            % (np.asscalar(opt[0]*100)), ls='-', lw=1, color='k')
    ax.legend()
    ax.set_xlabel('dt [s]')
    ax.set_ylabel('dphi [deg]')
    ax.set_title('Corotation speed of %s auroral features' % names[rrr])
    ax.set_xlim(left=0)
    ax.set_ylim(bottom=0)
    plt.savefig('%s/%s_corotation.jpg' % (listpath,regions[rrr]), bbox_inches='tight', dpi=500)
    plt.show()
    plt.close()