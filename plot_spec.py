import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import plot_HST_UVIS

from astropy.io import fits
import numpy as np
import os

savepath = '{}/Plots/spec'.format(boxpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)

hdul = fits.open('E:/data/UVIS/PDS_UVIS/PROJECTIONS/COUVIS_0047/2014_099T08_37_02_spec.fits')
wave = hdul[1].data['FREQ']
wdiff = np.mean(np.diff(wave))

img = hdul[0].data
img = img[:,:int(len(img[0,:,0])/6),:]

for iii in range(len(wave)):
    plot_HST_UVIS.simpleplot(img[:,:,iii]*wdiff*100, '{}/{}'.format(savepath, iii), '{} A'.format(wave[iii]), style='LT', KR_MIN=0.3, KR_MAX=20, scale='log')