# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import cassinipy
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import numpy as np
import time_conversions as tc

cd = cassinipy.CassiniData(datapath)

#%%

tstart = datetime(2007,1,13,0)
tstop = datetime(2007,1,27,0)

times = np.linspace(tc.datetime2et(tstart), tc.datetime2et(tstop), num=10000)


#_, r, lat, lt = cd.get_locations(times, refframe='KRTP')
_, x, y, z = cd.get_locations(times, refframe='KSM')


fig = plt.figure()
fig.set_size_inches(10,10)
ax = plt.subplot(projection='3d')
ax.plot(x, y, z, lw=0.5)
ax.set_xlabel('x [RS]')
ax.set_ylabel('y [RS]')
ax.set_zlabel('z [RS]')
ax.set_xlim([-50,50])
ax.set_ylim([-50,50])
ax.set_zlim([-50,50])
ax.set_proj_type('ortho')






